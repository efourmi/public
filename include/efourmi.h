/*!
    \file
    \brief Файл экспортируемых величин EFourmi устройств


*/
/*!
    \defgroup configuring Конфигурирование EFourmi устройства
    \details Конфигурирование EFourmi-устройства производится путем модификации
    структуры, описанной в struct efourmi_uheader_lite, расположенной по смещению __EFHeader
    в файле efourmi_lite_startup*.s проекта.
    Размер областей памяти, доступных по смещению EFOURMI_ROREG_USER и EFOURMI_RWREG_USER производится
    либо через указание RODATA_SZ=XX RWDATA_SZ=XX в командной строке ассемблера, либо через прямое указание
    в директивах ROData_size и RWData_size в файле efourmi_lite_startup*.s проекта.
*/
/*!
    \defgroup system Доступ к системным ресурсам EFourmi устройства.
    \details Доступ к системным ресурсам (вводам/выводам) возможен: для пользовательской программы - через системную функцию,
    через интерфейс USB, и через RS485-Modbus для устройств, сконфигурированных в качестве ведомых.
*/
#ifndef __EFOURMI_H
#define __EFOURMI_H

#ifdef __cplusplus
    extern "C"{
#endif

#include <stdint.h>

/*!
    \ingroup configuring
    \brief Регистры конфигурации интерфейсов задаются массивом efourmi_uheader_lite.config
*/
enum efourmi_configuration {
    /// Режим работы MODBUS. 1 - ведущий, 0 - ведомый.
    EFOURMICFG_MASTER_MODE,
    /// Скорость MODBUS интерфейса, бод/100. Например скорость 9600 бод задается значением 96.
    EFOURMICFG_MODBUS_GBPS,
    /// Контроль четности MODBUS интерфейса 0 - отсутствует, 1 - нечетный, 2 - четный
    EFOURMICFG_MODBUS_PARITY,
    /// Адресс устройства на шине MODBUS в диапазоне 1..247
    EFOURMICFG_MODBUS_ADDRESS,
    /// Время ожидания первого байта ответа ведомого MODBUS устройства, бод. Например, значению 88 соответствует 11(длина MODBUS символа) * 8 символов ожидания.
    EFOURMICFG_MODBUS_ANSWAIT,
    /// ШИМ делитель для частоты 48MHz, см. документацию на TIMx->PWM для STM32F072.
    EFOURMICFG_PWM_PSC,
    /// ШИМ период, см. документацию на TIMx->ARR для STM32F072.
    EFOURMICFG_PWM_ARR,
    /// Время захвата уровня сигнала АЦП 0..7, см. документацию на ADC1->SMPR для STM32F072.
    EFOURMICFG_ADC_SMPR,
    /// Таймаут удержания 1-Wire сессии, мс
    EFOURMICFG_1WIRE_STOUT,
    /// Идентификатор производителя USB интерфейса
    EFOURMICFG_USB_VID,
    /// Идентификатор устройства USB интерфейса
    EFOURMICFG_USB_PID,
    /// Число регистров
    EFOURMICFG_LAST
};

/*!
    \defgroup configuring_iomodes Режимы работы вводов/выводов EFourmi-устройства
    \ingroup configuring
    \brief Режим работы вводов/выводов задается массивом efourmi_uheader_lite.ioconfig
    \details Например:

        Cтроке "II00PPOA" соответсвует конфигурация:
            дискретные входы с индексами [0], [1]
            два незадействованных входа/выхода
            ШИМ-выходы с индексами [EFOURMI_RWREG_PWM + 4], [EFOURMI_RWREG_PWM + 5]
            дискретный выход с индексом 6
            аналоговый вход с индексом [EFOURMI_ROREG_PWM + 7]
    \{
*/
/// Режим дискретного входа
#define EFOURMI_IOMODE_DI                                  'I'
/// Режим дискретного выхода
#define EFOURMI_IOMODE_DO                                  'O'
/// Ввод/вывод в режиме аналогового входа
#define EFOURMI_IOMODE_ADC                                 'A'
/// Ввод/вывод в режиме ШИМ выхода
#define EFOURMI_IOMODE_PWM                                 'P'
/// Ввод/вывод в режиме, определяемом пользовательской программой, активирует тактирование выбранного порта
#define EFOURMI_IOMODE_USER                                'U'
/// Ввод/вывод не используется
#define EFOURMI_IOMODE_NUSED                               '0'
/*!
  \}
*/
/*!
    \ingroup configuring
    \brief Структура описания EFourmi устройства.
    \details Данная структура в ассемблерном представлении находится в файле efourmi_lite_startup*.s проекта
*/
struct efourmi_uheader_lite{
    uint32_t magic[2];///< EFOURMI_MAGIC0 и EFOURMI_MAGIC1. [Автоматически]
    uint32_t sp;///< Указатель на вершину стека пользовательской программы. [Автоматически]
    void (*main)(void);///< Точка входа в пользовательскую программу. [Автоматически]
    const void* rodata;///< Указатель на пользовательские данные, доступные для чтения через Modbus по смещению EFOURMI_ROREG_USER. Данные задаются пользовательской программой. [Автоматически]
    uint32_t rodata_sz;///< Размер пользовательских данных, доступные через Modbus по смещению EFOURMI_ROREG_USER. [Автоматически]
    void* rwdata;///< Указатель на пользовательские данные, доступные для чтения и записи через Modbus по смещению EFOURMI_RWREG_USER. [Автоматически]
    uint32_t rwdata_sz;///< Размер пользовательских данных, доступные через Modbus по смещению EFOURMI_RWREG_USER. [Автоматически]
    uint8_t ioconfig[8];///< Строка конфигурации входов/выходов Efourmi устройства
    uint16_t config[EFOURMICFG_LAST];///< Регистры конфигурации интерфейсов EFourmi устройства
};
/*!
    \ingroup configuring
    \brief Значение struct efourmi_uheader_lite.magic[0]
*/
#define EFOURMI_MAGIC0                                     (0x756F6665)
/*!
    \ingroup configuring
    \brief Значение struct efourmi_uheader_lite.magic[1]
*/
#define EFOURMI_MAGIC1                                     (0x00696D72)


/*!
  \defgroup system_cmd Доступ к системным интерфейсам из пользовательской программы
  \ingroup system
  \brief Использование системной функции из пользовательской программы
  \details
  \{
*/
/// Прототип функции обработчика прерывания
typedef void irq_handler(void);
/*!
    \brief Прототип системной функции
    \details Для доступа к системным интерфейсам пользовательское приложение вызывает cmd_handler с
    соответствующим кодом запроса.
*/
typedef int cmd_handler(int cmd, ...);
/*!
  \}
*/

/*!
  \defgroup errcodes Описание возвращаемых cmd_handler() ошибок
  \ingroup system_cmd
  \{
*/
/// \brief Ошибка в параметрах функции
#define EFOURMI_EINVAL                                     (-1)
/// \brief Неправильно задан индекс
#define EFOURMI_EININD                                     (-2)
/// \brief Интерфейс занят
#define EFOURMI_EBUSY                                      (-3)
/// \brief Данные не готовы
#define EFOURMI_ENAK                                       (-4)
/// \brief Функция/Параметры не поддерживается
#define EFOURMI_ENOTSUP                                    (-5)
/// \brief Ответ удаленного устройства превышает допустимый размер
#define EFOURMI_EOVRRUN                                    (-6)
/// \brief Ответ удаленного устройства поврежден
#define EFOURMI_ECHAR                                      (-7)
/// \brief Ошибка выполнения функции в данном контексте
#define EFOURMI_EFAULT                                     (-8)
/// \brief Превышено время выполнения функции
#define EFOURMI_ETIMEOUT                                   (-9)
/// \brief Ошибка ввода-вывода
#define EFOURMI_EIO                                        (-10)
/// \brief Отсутствует отклик удаленного устройства
#define EFOURMI_ENORESP                                    (-11)
/*!
  \}
*/

/*!
    \defgroup system_cmd_cmd
    \ingroup system_cmd
    \brief Коды запроса системной функции
    \details Перечень кодов, доступных к использованию в качестве параметра cmd функции cmd_handler()
  \{
*/
#define EFOURMI_CMD_REGISTERIRQ                            (0)
/*
    int cmd(EFOURMI_CMD_REGISTERIRQ, int IRQn, void(*IRQHandler)(void))
*/
#define EFOURMI_CMD_ENVGET                                 (1)
/*
    int cmd(EFOURMI_CMD_ENVGET, int envn)
*/
#define EFOURMI_CMD_ENVSET                                 (2)
/*
    int cmd(EFOURMI_CMD_ENVSET, int envn, int value)
*/
#define EFOURMI_CMD_REGGET                                 (3)
/*
    int cmd(EFOURMI_CMD_REGGET, int regn)
*/
#define EFOURMI_CMD_REGSET                                 (4)
/*
    int cmd(EFOURMI_CMD_REGSET, int regn, int value)
*/
#define EFOURMI_CMD_MODBUS                                 (5)
/*
    int cmd(EFOURMI_CMD_MODBUS, uint8_t* data, int datalen)
*/
/*!
  \}
*/




/*!
  \}
*/

/*!
  \defgroup owire Интерфейс 1Wire
  \{
*/
/// Максимальный размер единичной 1Wire транзакции
#define EFOURMI_1WIRE_BUFFERLEN                            (248)
/// Время выполнения обмена битом данных, в 1/1024 мс
#define EFOURMI_1WIRE_TIMING_BIT                           ((10 * 1024 * 1000) / (48000000 / 0x01A1))
/// Время выполнения сигнала сброса, в 1/1024 мс
#define EFOURMI_1WIRE_TIMING_RESET                         ((10 * 1024 * 1000) / (48000000 / 0x1388))
/*!
  \defgroup owire_session_cmd Команды работы с 1Wire сессиями и транзакциями
  \{
*/
/// \brief Создать новую сессию и выполнить транзакцию
#define EFOURMI_1WIRE_CMD_NEWTRANS                         (0x00)
/// \brief Получить результат предудущей транзакции
#define EFOURMI_1WIRE_CMD_GETRES                           (0x01)
/// \brief Продолжить выполнение транзакций в рамках текущей сессии
#define EFOURMI_1WIRE_CMD_CONT                             (0x02)
/// \brief Завершить сессию
#define EFOURMI_1WIRE_CMD_FIN                              (0x03)
/*!
  \}
*/
/*
  Транзакция состоит из массива операций, следующих друг за другом без выравнивания. Результат выполнения транзакции
  может отличаться по размеру с исходным описанием, при этом при компоновке транзакции должнен учитываться тот факт,
  что результат транзакции должен уместиться в EFOURMI_1WIRE_BUFFERLEN, иначе функция вернет EFOURMI_EINVAL, без фактического
  выполения транзакции.

  Байт описания операции [*OP*] состоит из типа операции в маске EFOURMI_1WIRE_OP_CMD_MASK,
  результат выполнения записывается в данный байт в маске EFOURMI_1WIRE_OP_RESULT_MASK.

  Байт размера данных [*SZ*] описывает длину массива данных. Установленный бит EFOURMI_1WIRE_SZ_INBYTES указывает на то, что поле
  EFOURMI_1WIRE_SZ_VALUE содержит длину данных в байтах, которые находятся сразу за байтом [*SZ*].
  Сброшенный бит EFOURMI_1WIRE_SZ_INBYTES указывает на длину данных в битах, данные находятся в массиве байт минимально
  необходимого размера для данного количества бит, "лишние" биты имеют значение ноль.

  Байт времени задержки описывает промежуток времени с разрешением 10ms в интервале 10..2560ms.

  Операции EFOURMI_1WIRE_OP_CMD_EXCH, EFOURMI_1WIRE_OP_CMD_REXCH - обмен данными. Операция EFOURMI_1WIRE_OP_CMD_REXCH предваряет обмен
  данными сигналом сброса.
  Формат операции [EFOURMI_1WIRE_OP_CMD_REXCH][*SZ*][DATA0..N].
  В случае успеха результат идентичен описанию. EFOURMI_1WIRE_OP_RESULT_MASK устанавливается в EFOURMI_1WIRE_OP_RESULT_SUCCESS,
  [DATA0..N] корректируются в соответствии с полученными данными.
  В случае неудачи возвращается один байт (EFOURMI_1WIRE_OP_CMD_*EXCH) or (EFOURMI_1WIRE_OP_RESULT_*).

  Операции EFOURMI_1WIRE_OP_CMD_FSEARCH, EFOURMI_1WIRE_OP_CMD_SEARCH - поиск устройств. Операция EFOURMI_1WIRE_OP_CMD_FSEARCH сбрасывает
  поиск, устанавливает текущую команду поиска (Search ROM, Search Alarm) и возвращает идентификатор первого устройства.
  EFOURMI_1WIRE_OP_CMD_SEARCH осуществляет поиск следующего устройства через заданную команду поиска.
  Формат операций: [EFOURMI_1WIRE_OP_CMD_FSEARCH][Команда поиска]
                   [EFOURMI_1WIRE_OP_CMD_SEARCH]
  В случае успеха результат содержит один байт (EFOURMI_1WIRE_OP_CMD_*SEARCH) or (EFOURMI_1WIRE_OP_RESULT_SUCCESS*) + 8 байт идентификатора
  найденного устройства.
  В случае неудачи возвращается один байт (EFOURMI_1WIRE_OP_CMD_*) or (EFOURMI_1WIRE_OP_RESULT_*).

  Операция EFOURMI_1WIRE_OP_CMD_DELAY - задержка. Операция EFOURMI_1WIRE_OP_CMD_DELAY осуществляет задержку по времени
  перед выполнением следующей операции
  Формат операции: [EFOURMI_1WIRE_OP_CMD_DELAY] [Байт времени задержки]
  Возвращает один байт [EFOURMI_1WIRE_OP_CMD_DELAY].
*/
/*!
  \defgroup owire_op 1Wire операции
  \{
*/
/*!
  \defgroup owire_op_cmd Байт описания/результата операции
  \{
*/
/// \brief Маска кода операции
#define EFOURMI_1WIRE_OP_CMD_MASK                          (0x0F)
/// \brief Обмен данными с предварительным сбросом
#define EFOURMI_1WIRE_OP_CMD_REXCH                         (0x01)
/// \brief Обмен данными */
#define EFOURMI_1WIRE_OP_CMD_EXCH                          (0x02)
/// \brief Поиск первого устройства
#define EFOURMI_1WIRE_OP_CMD_FSEARCH                       (0x03)
/// \brief Поиск следующего устройства
#define EFOURMI_1WIRE_OP_CMD_SEARCH                        (0x04)
/// \brief Задержка
#define EFOURMI_1WIRE_OP_CMD_DELAY                         (0x05)
/// \brief Маска кода результата операции
#define EFOURMI_1WIRE_OP_RESULT_MASK                       (0xF0)
/// \brief Операция выполнена успешно
#define EFOURMI_1WIRE_OP_RESULT_SUCCESS                    (0x10)
/// \brief Операция поиска выполенна успешно, найденное устройство - последнее
#define EFOURMI_1WIRE_OP_RESULT_SUCCESS_LAST               (0x20)
/// \brief Нет ответа от устройств на шине
#define EFOURMI_1WIRE_OP_RESULT_NOANSWER                   (0x30)
/// \brief Попытка поиска устройства после получения EFOURMI_1WIRE_OP_RESULT_SUCCESS_LAST
#define EFOURMI_1WIRE_OP_RESULT_ALAST                      (0x40)
/// \brief Ошибка в описании команды
#define EFOURMI_1WIRE_OP_RESULT_CMDERR                     (0x50)
/*!
  \}
*/
/*!
  \defgroup owire_op_sz Байт размера операции
  \{
*/
/// Бит размерности: 1 - в байтах, 0 - в битах
#define EFOURMI_1WIRE_SZ_INBYTES                           (0x80)
/// Маска размера
#define EFOURMI_1WIRE_SZ_VALUE                             (0x7F)
/*!
  \}
*/
/*!
  \}
*/
/*!
  \}
*/

/*!
  \defgroup roreg Группы регистров для чтения
  Каждая группа содержит 256 регистров [XX00]-[XXFF]
  \{
*/
/// Цифровые входы устройства, по 16 входов в 1 регистре
#define EFOURMI_ROREG_DI                                   (0x0000)
/// Аналоговые входы устройсва
#define EFOURMI_ROREG_ADC                                  (0x0100)
/// Регистры для чтения пользовательской программы
#define EFOURMI_ROREG_USER                                 (0x0200)
/*!
  \}
*/

/*!
  \defgroup rwreg Группы регистров для чтения/записи
  Каждая группа содержит 256 регистров [XX00]-[XXFF]
  \{
*/
/// Цифровые выходы устройства, по 16 выходов в 1 регистре
#define EFOURMI_RWREG_DO                                   (0x0000)
/// ШИМ выходы устройства
#define EFOURMI_RWREG_PWM                                  (0x0100)
/// Аналоговые выходы устройства
#define EFOURMI_RWREG_DAC                                  (0x0200)
/// Регистры полного доступа пользовательской программы
#define EFOURMI_RWREG_USER                                 (0x0300)
/// Регистры, хранящиеся во флеш-памяти
#define EFOURMI_RWREG_FLASH                                (0x0400)
/*!
  \}
*/


/*!
  \defgroup uirq Рекомендуемые приоритеты прерываний пользовательской программы
  \{
*/
/// Высокий приоритет
#define EFOURMI_USERIRQ_HIGH                               (3 << 6)
/// Низкий приоритет
#define EFOURMI_USERIRQ_LO                                 (4 << 6)
/*!
  \}
*/




/*!
  \defgroup env Переменные среды
  \{
*/
/// Modbus - Общее число сообщений
#define EFOURMI_ENV_MODBUS_MSGCOUNT                        (0)
/// Modbus - Число сообщений недостаточной длины или с неправильной контрольной суммой
#define EFOURMI_ENV_MODBUS_ECOMCOUNT                       (1)
/// Modbus - Число сообщений возвращенных с ошибкой
#define EFOURMI_ENV_MODBUS_EEXCCOUNT                       (2)
/// Modbus - Число широковещательных или адресованных устройству сообщений
#define EFOURMI_ENV_MODBUS_SMSGCOUNT                       (3)
/// Modbus - Число широковещательных сообщений
#define EFOURMI_ENV_MODBUS_BRSTCOUNT                       (4)
/// Modbus - Число сообщений вызвавших ответ NAK
#define EFOURMI_ENV_MODBUS_NAKCOUNT                        (5)
/// Modbus - Число сообщений вызвавших BUSY
#define EFOURMI_ENV_MODBUS_BUSYCOUNT                       (6)
/// Modbus - Число сообщений с длиной превышающей допустимую
#define EFOURMI_ENV_MODBUS_CHORCOUNT                       (7)
/// Значение тактовой частоты SYSCLK
#define EFOURMI_ENV_SYSFREQ                                (8)
/*!
  \}
*/














/*!
  \defgroup system_usb Доступ к системным интерфейсам через интерфейс USB
  \ingroup system
  \brief Настройки USB интерфейса и доступные управляющие команды.
  \details
  \{
*/
/// Размер уникального идентификатора устройства
#define EFOURMI_SERIAL_SIZE                                (24)
/// Идентификатор производителя по умолчанию
#define EFOURMI_USB_VID_DEF                                (0x6666)
/// Идентификатор устройства по умолчанию
#define EFOURMI_USB_PID_DEF                                (0x0001)
/// Класс EFOURMI USB интерфейса
#define EFOURMI_USB_INTERFACE_CLASS                        (0xFF)
/// Подкласс EFOURMI USB интерфейса
#define EFOURMI_USB_INTERFACE_SUBCLASS                     (0x01)
/// Запрос к интерфейсу на сброс текущей транзакции
#define EFOURMI_USB_REQ_SYNC                               (0x00)
/// Запрос на перезагрузку устройства
#define EFOURMI_USB_REQ_RESET                              (0x01)
/// Запрос на получение массива активных настроек
#define EFOURMI_USB_REQ_RTCONFIG                           (0x02)
/// Запрос на получение конфигурации линий ввода-вывода
#define EFOURMI_USB_REQ_IOCONFIG                           (0x03)
/// Запров на получение/установку величины таймаута RS485
#define EFOURMI_USB_REQ_RS485TOUT                          (0x04)
/*!
  \}
*/
#ifdef __cplusplus
}
#endif

#endif
