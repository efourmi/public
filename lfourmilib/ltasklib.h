#ifndef LTASKLIB_H
#define LTASKLIB_H

#include <stdint.h>
#include <time.h>
#include <poll.h>
#include "lua.h"


typedef int64_t msec; /* milliseconds time type */

struct task;

struct task_yeild_result{
    struct pollfd* polls;
    size_t pollcount;
    int ms;
};

typedef int (*task_KFunction)(lua_State* L, int status, const struct task_yeild_result* result);

LUAI_FUNC int tasklib_istask(lua_State* L);

LUAI_FUNC msec tasklib_msec(void); /* return time in milliseconds from system start */

LUAI_FUNC struct pollfd* task_newpoll(lua_State* L, size_t pollcount);

LUAI_FUNC void task_yeild(lua_State* L, task_KFunction kf, int ms);

LUAI_FUNC int luaopen_task_timer(lua_State* L);

LUAI_FUNC int luaopen_task_modbus(lua_State* L);


#endif // LTASKLIB_H
