#include "ltasklib.h"
#include "lauxlib.h"
#include <stdlib.h>
#include <assert.h>
#include <sys/queue.h>


#define STATE(L) (((struct tasklib_state**)lua_getextraspace(L))[0])

msec tasklib_msec(void){
    struct timespec now;
#ifdef CLOCK_MONOTONIC_RAW
    clock_gettime(CLOCK_MONOTONIC_RAW, &now);
#else
#warning "CLOCK_MONOTONIC_RAW not support on this platform, use CLOCK_MONOTONIC"
    clock_gettime(CLOCK_MONOTONIC, &now);
#endif
    return (now.tv_sec * 1000) + (now.tv_nsec/1000000);
}

struct task{
    LIST_ENTRY(task) tasklist_entry;
    lua_State* tL;
    size_t bank_start; // index of current task first struct pollfd* in current taskpool.bank
    size_t bank_count; // count of current task struct pollfd* in current taskpool.bank
    msec sleepto; // timeout of current functions, if sleepto < 0 - timeout infinity
    int registryindex;
    int luastateregistryindex;
    int stackcorrection;
};

struct tasklib_state{
    LIST_HEAD(, task) tasklist;
    struct task* ctask;
    int registryindex;
    struct{
        struct pollfd* polls;
        size_t size;  // allocated size of bank
        size_t count; // active poll count
    }pollbank[2];
    int timeout;  // timeout for the next poll call
    int readbank; // index of bank with result of previous poll call
    struct task_yeild_result taskresult;
};

int tasklib_istask(lua_State* L){
    return STATE(L)->ctask != NULL;
}

static int tasklibstate_gc(lua_State* L){
    struct tasklib_state* state = STATE(L);
    struct task* ctask;
    (void)L;
    while(ctask = LIST_FIRST(&state->tasklist), ctask){
        luaL_unref(L, LUA_REGISTRYINDEX, ctask->luastateregistryindex);
        luaL_unref(L, LUA_REGISTRYINDEX, ctask->registryindex);
        LIST_REMOVE(ctask, tasklist_entry);
    }
    free(state->pollbank[0].polls);
    free(state->pollbank[1].polls);
    return 0;
}

static void tasklibstate_init(lua_State* L){
    static const struct luaL_Reg tasklibstate_meta[]={
        {"__gc", tasklibstate_gc},
        {NULL, NULL}
    };
    struct tasklib_state* state = lua_newuserdata(L, sizeof(struct tasklib_state));

    luaL_newmetatable(L, "TASKLIBSTATE_META");
    lua_pushvalue(L, -1);
    lua_setfield(L, -2, "__index");
    luaL_setfuncs(L, tasklibstate_meta, 0);
    lua_pop(L, 1);
    luaL_setmetatable(L, "TASKLIBSTATE_META");

    LIST_INIT(&state->tasklist);
    state->ctask = NULL;
    state->registryindex = luaL_ref(L, LUA_REGISTRYINDEX);
    state->pollbank[0].count = 0;
    state->pollbank[0].size = 0;
    state->pollbank[0].polls = NULL;
    state->pollbank[1].count = 0;
    state->pollbank[1].size = 0;
    state->pollbank[1].polls = NULL;
    state->readbank = 0;
    state->timeout = -1;

    STATE(L) = state;
}

static int tasklib_newtask(lua_State* L){
    struct task* ntask;
    struct tasklib_state* state;
    luaL_checktype(L, 1, LUA_TFUNCTION);

    state = STATE(L);
    ntask = lua_newuserdata(L, sizeof(struct task));
    LIST_INSERT_HEAD(&state->tasklist, ntask, tasklist_entry);
    ntask->bank_start = 0;
    ntask->bank_count = 0;
    ntask->sleepto = -1;
    ntask->tL = lua_newthread(L);
    ntask->registryindex = luaL_ref(L, LUA_REGISTRYINDEX);
    ntask->luastateregistryindex = luaL_ref(L, LUA_REGISTRYINDEX);
    ntask->stackcorrection = -1;

    lua_xmove(L, ntask->tL, lua_gettop(L));
    return 0;
}

struct pollfd* task_newpoll(lua_State* L, size_t pollcount){
    struct tasklib_state* state = STATE(L);
    int writebank = state->readbank ^ 1;
    state->ctask->bank_count = pollcount;
    if(pollcount){
        state->ctask->bank_start = state->pollbank[writebank].count;
        size_t reqsize = state->ctask->bank_start + state->ctask->bank_count;
        if(reqsize > state->pollbank[writebank].size){
            struct pollfd* pb = realloc(state->pollbank[writebank].polls, sizeof(struct pollfd) * reqsize);
            if(!pb){
                luaL_error(L, "lmodbusdlib.c/task_startwrite(): not enought memory");
            }
            state->pollbank[writebank].polls = pb;
            state->pollbank[writebank].size = reqsize;
        }
        state->pollbank[writebank].count += pollcount;
        return state->pollbank[writebank].polls + state->ctask->bank_start;
    }else{
        return NULL;
    }
}

void task_yeild(lua_State* L, task_KFunction kf, int ms){
    struct tasklib_state* state = STATE(L);
    if(ms < 0){
        state->ctask->sleepto = -1;
    }else{
        state->ctask->sleepto = tasklib_msec() + ms;
        if((unsigned int)ms < ((unsigned int)state->timeout)){
            state->timeout = ms;
        }
    }
    lua_yieldk(L, lua_gettop(L), (lua_KContext)&state->taskresult, (lua_KFunction)kf);
}

static int tasklib_shedule(lua_State* L){
    struct tasklib_state* state = STATE(L);
    struct task* ctask,* ntask;
    if(state->ctask){
        luaL_error(L, "call shedule() from task impossible");
    }
    while(ctask = LIST_FIRST(&state->tasklist), ctask){
        do{
            int ret;
            ntask = LIST_NEXT(ctask, tasklist_entry);
            state->ctask = ctask;
            state->taskresult.pollcount = ctask->bank_count;
            state->taskresult.polls = state->pollbank[state->readbank].polls + ctask->bank_start;
            if(ctask->sleepto < 0){
                state->taskresult.ms = -1;
            }else{
                msec diff = ctask->sleepto - tasklib_msec();
                state->taskresult.ms = diff < 0? 0: diff;
            }
            ctask->bank_count = 0;
            ctask->bank_start = 0;
            ctask->sleepto = -1;
            ret = lua_resume(ctask->tL, L, lua_gettop(ctask->tL) + ctask->stackcorrection);
            state->ctask = NULL;
            ctask->stackcorrection = 0;
            switch(ret){
            case LUA_OK: // task compleate
                luaL_unref(L, LUA_REGISTRYINDEX, ctask->luastateregistryindex);
                luaL_unref(L, LUA_REGISTRYINDEX, ctask->registryindex);
                LIST_REMOVE(ctask, tasklist_entry);
                break;
            case LUA_YIELD: // task wait
                break;
            default:{// task error
                const char* err = lua_tostring(ctask->tL, -1);
                printf("%s\r\n", err);
                luaL_unref(L, LUA_REGISTRYINDEX, ctask->luastateregistryindex);
                luaL_unref(L, LUA_REGISTRYINDEX, ctask->registryindex);
                LIST_REMOVE(ctask, tasklist_entry);
                break;
            }
            }
            ctask = ntask;
        }while(ctask);
        state->readbank ^= 1;
        if((state->pollbank[state->readbank].count == 0) && (state->timeout < 0)){
            break;
        }
        poll(state->pollbank[state->readbank].polls, state->pollbank[state->readbank].count, state->timeout);
        state->pollbank[state->readbank ^ 1].count = 0;
        state->timeout = -1;
    }
    return 0;
}

static int tasklib_udelayK(lua_State* L, int status, const struct task_yeild_result* result){
    (void)status;
    if(result->ms > 0){
        task_yeild(L, tasklib_udelayK, result->ms);
    }
    return 0;
}

static int tasklib_udelay(lua_State* L){
    struct task* ctask = STATE(L)->ctask;
    int ms = luaL_checkinteger(L, 1);
    ms = ms < 0? 0: ms;
    if(ctask){
        task_yeild(L, tasklib_udelayK, ms);
    }else if(ms){
        poll(NULL, 0, ms);
    }
    return 0;
}

static const luaL_Reg tasksublibs[] = {
  {"timer", luaopen_task_timer},
  {"modbus", luaopen_task_modbus},
  {NULL, NULL}
};

LUAMOD_API int luaopen_task(lua_State* L){
    size_t l;
    assert(LUA_EXTRASPACE >= sizeof(void*));
    luaL_checkversion(L);
    tasklibstate_init(L);
    lua_pushcfunction(L, tasklib_newtask);
    lua_setglobal(L, "newtask");
    lua_pushcfunction(L, tasklib_shedule);
    lua_setglobal(L, "shedule");
    lua_pushcfunction(L, tasklib_udelay);
    lua_setglobal(L, "udelay");
    lua_newtable(L);
    for(l = 0; tasksublibs[l].name; l++){
        tasksublibs[l].func(L);
        lua_setfield(L, -2, tasksublibs[l].name);
    }
    return 1;
}
