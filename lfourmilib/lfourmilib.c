#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include <string.h>
#include <stdint.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <poll.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/queue.h>

#define MAKEMBDWORD(B,I) ((uint16_t)(((B)[(I)] << 8) | ((B)[(I) + 1])))

#if LUA_VERSION_NUM == 502
    #define KFUNCTION(NAME)                                NAME(lua_State* L)
    #define KHEADER
    #define __lua_getglobal(LSTATE, NAME)                  (lua_getglobal((LSTATE), (NAME)), lua_type(L, -1))
    #define __lua_getfield(LSTATE, INDEX,  NAME)           (lua_getfield((LSTATE), (INDEX), (NAME)), lua_type(L, -1))
#elif LUA_VERSION_NUM == 503
    #define KFUNCTION(NAME)                                NAME(lua_State* L, int status, lua_KContext ctx)
    #define KHEADER                                        (void)status;(void)ctx
    #define __lua_getglobal                                lua_getglobal
    #define __lua_getfield                                 lua_getfield
#endif

#define EXC_ILLFUNCTION                  (0x1)
#define EXC_ILLDATAADDR                  (0x2)
#define EXC_ILLDATAVALUE                 (0x3)
#define EXC_SERVERFAILURE                (0x4)
#define EXC_ACKNOLEDGE                   (0x5)
#define EXC_SERVERBUSY                   (0x6)
#define EXC_GATEWAYPATH                  (0xA)
#define EXC_GATEWAYNORESP                (0xB)

#define ENV_OPEN_TESTEFOURMI_COUNT        "open_attemp"
#define ENV_OPEN_TESTEFOURMI_COUNT_DEF    (2)
#define ENV_OPEN_TESTEFOURMI_TIMEOUT      "timeout"
#define ENV_OPEN_TESTEFOURMI_TIMEOUT_DEF  (1000)

typedef int64_t msec;

struct task{
    lua_State* L;
    int task_sindex; // index of task thread in run() stack
    int func_sindex; // index of task main function in run() stack
    size_t bank_start; // index of current task first struct pollfd* in current taskpool.bank
    size_t bank_count; // count of current task struct pollfd* in current taskpool.bank
    msec sleepto; // timeout of current functions, if sleepto < 0 - timeout infinity
};

struct taskpool{
    struct{
        struct pollfd* polls;
        size_t size; // allocated size of bank
        size_t count; // active poll count
    }bank[2];
    int timeout; // timeout for the next poll call
    unsigned int readbank; // index of bank with result of previous poll call
    struct task* tasklist; // struct task* list
    struct task* acttask; // current active task
    int task_count; // task count on run() start
    int activetask_count;// active task count
};

#define TASKPOOL_REF                             "FOURMILIB_TASKPOOL"
#define TASKPOOL_REF_META                        "FOURMILIB_TASKPOOL_META"

#define REGLIST_META                             "FOURIMLIB_REGLIST"

#define ONDTIMER_REF_META                        "FOURMILIB_ODTIMER_META"
#define OFFDTIMER_REF_META                       "FOURMILIB_OFFDTIMER_META"

#define MBUSNODE_REF_META                        "FOURMILIB_MBUSNODE_META"

static msec os_ticks(void){
    struct timespec now;
#ifdef CLOCK_MONOTONIC_RAW
    clock_gettime(CLOCK_MONOTONIC_RAW, &now);
#else
#warning "CLOCK_MONOTONIC_RAW not support on this platform, use CLOCK_MONOTONIC"
    clock_gettime(CLOCK_MONOTONIC, &now);
#endif
    return (now.tv_sec * 1000) + (now.tv_nsec/1000000);
}

static int fourmi_env(lua_State* L, const char* envname, int def){
    int t = __lua_getglobal(L, "fourmi");
    int ret;
    if(t == LUA_TTABLE){
        int t2 = __lua_getfield(L, -1, envname);
        if(t2 == LUA_TNUMBER){
            ret = lua_tointeger(L, -1);
        }else{
            ret = def;
        }
        lua_pop(L, 2);
        return ret;
    }else{
        lua_pop(L, 1);
        return def;
    }
}

static int taskpool_gc(lua_State* L){
    struct taskpool* tpool = (struct taskpool*)luaL_testudata(L, -1, TASKPOOL_REF_META);
    if(tpool){
        free(tpool->bank[0].polls);
        free(tpool->bank[1].polls);
        free(tpool->tasklist);
    }
    return 0;
}

static struct taskpool* taskpool_get(lua_State* L){
    int m = lua_pushthread(L);
    lua_pop(L, 1);
    if(m){// main thread
        return NULL;
    }
    lua_getfield(L, LUA_REGISTRYINDEX, TASKPOOL_REF);
    void* ts = luaL_testudata(L, -1, TASKPOOL_REF_META);
    lua_pop(L, 1);
    return ts;
}

static struct pollfd* taskpoll_fin(lua_State* L, struct taskpool* tpool, size_t* pollcount, int* ms){
    (void)L; // possibly would be need for error handling
    if(ms){
        if(tpool->acttask->sleepto < 0){
            *ms = -1;
        }else{
            msec diff = tpool->acttask->sleepto - os_ticks();
            *ms = diff < 0? 0: diff;
        }
    }
    if(pollcount){
        *pollcount = tpool->acttask->bank_count;
    }
    return tpool->bank[tpool->readbank].polls + tpool->acttask->bank_start;
}

static struct pollfd* taskpoll_start(lua_State* L, struct taskpool* tpool, size_t pollcount, int ms){
    int writebank = tpool->readbank ^ 1;
    if(ms < 0){
        tpool->acttask->sleepto = -1;
    }else{
        tpool->acttask->sleepto = os_ticks() + ms;
        if((unsigned int)ms < ((unsigned int)tpool->timeout)){
            tpool->timeout = ms;
        }
    }
    tpool->acttask->bank_count = pollcount;
    if(pollcount){
        tpool->acttask->bank_start = tpool->bank[writebank].count;
        size_t reqsize = tpool->acttask->bank_start + tpool->acttask->bank_count;
        if(reqsize > tpool->bank[writebank].size){
            struct pollfd* pb = realloc(tpool->bank[writebank].polls, sizeof(struct pollfd) * reqsize);
            if(!pb){
                luaL_error(L, "lmodbusdlib.c/task_startwrite(): not enought memory");
            }
            tpool->bank[writebank].polls = pb;
            tpool->bank[writebank].size = reqsize;
        }
        tpool->bank[writebank].count += pollcount;
        return tpool->bank[writebank].polls + tpool->acttask->bank_start;
    }else{
        return NULL;
    }
}

static void init_taskpool_meta(lua_State* L){
    static const luaL_Reg taskpool_meta[] = {
        {"__gc", taskpool_gc},
        {NULL, NULL}
    };
    luaL_newmetatable(L, TASKPOOL_REF_META);
    lua_pushvalue(L, -1);
    lua_setfield(L, -2, "__index");
    luaL_setfuncs(L, taskpool_meta, 0);
    lua_pop(L, 1);
}

struct reg{
    int address;
    lua_Integer value;
};

struct reglist{
    size_t sz;
    struct reg regs[];
};

static struct reg* reglist_addrtoreg(struct reglist* rl, lua_Integer addr){
    size_t sz = rl->sz;
    while(sz){
        if(rl->regs[--sz].address == addr){
            return rl->regs + sz;
        }
    }
    return NULL;
}

static int reglist_Lindex(lua_State* L){
    struct reglist* rl = (struct reglist*)luaL_checkudata(L, 1, REGLIST_META);
    lua_Integer address = luaL_checkinteger(L, 2);
    if(address < 0){
        goto __fault;
    }
    struct reg* r = reglist_addrtoreg(rl, address);
    if(!r){
        goto __fault;
    }
    lua_pushinteger(L, r->value);
    return 1;
__fault:
    luaL_argerror(L, 2, "index is out of set");
    return 0;
}

static int reglist_Lnewindex(lua_State* L){
    struct reglist* rl = (struct reglist*)luaL_checkudata(L, 1, REGLIST_META);
    lua_Integer address = luaL_checkinteger(L, 2);
    if(address < 0){
        goto __addrfault;
    }
    int t3 = lua_type(L, 3);
    lua_Integer value;
    if(t3 == LUA_TNUMBER){
        value = lua_tointeger(L, 3);
    }else if(t3 == LUA_TBOOLEAN){
        value = lua_toboolean(L, 3);
    }else{
        goto __valuefault;
    }
    struct reg* r = reglist_addrtoreg(rl, address);
    if(!r){
        goto __addrfault;
    }
    r->value = value;
    return 1;
__addrfault:
    return luaL_argerror(L, 2, "index is out of set");
__valuefault:
    return luaL_argerror(L, 3 , "expected number or boolean");
}

static int reglist_Llen(lua_State* L){
    struct reglist* rl = (struct reglist*)luaL_checkudata(L, 1, REGLIST_META);
    lua_pushinteger(L, rl->sz);
    return 1;
}

static int regaddr_cmp(const void* r1, const void* r2){
    const struct reg* reg1 = (const struct reg*)r1;
    const struct reg* reg2 = (const struct reg*)r2;
    return reg1->address - reg2->address;
}
static int reglist_Lnew(lua_State* L){
    int sz = lua_gettop(L);
    int i;
    if(!sz){
        luaL_error(L, "at least 1 register must be set");
    }
    struct reglist* rl = (struct reglist*)lua_newuserdata(L, sizeof(struct reglist) + sizeof(struct reg) * sz);
    rl->sz = sz;
    for(i = 0; i < sz; i++){
        lua_Integer addr = luaL_checkinteger(L, i + 1);
        if((addr < 0) || (addr > 65535)){
            luaL_argerror(L, i + 1, "address must be in range 0..65535");
        }
        int i2;
        for(i2 = 0; i2 < i; i2++){
            if(rl->regs[i2].address == addr){
                luaL_argerror(L, i, "duplicate address in set");
            }
        }
        rl->regs[i].address = addr;
    }
    if(sz > 1){
        qsort(&rl->regs, sz, sizeof(struct reg), regaddr_cmp);
    }
    luaL_setmetatable(L, REGLIST_META);
    return 1;
}

static int reglist_Ltostring(lua_State* L){
    struct reglist* rl = (struct reglist*)luaL_checkudata(L, 1, REGLIST_META);
    luaL_Buffer buf;
    luaL_buffinit(L, &buf);
    luaL_addchar(&buf, '{');
    lua_pushinteger(L, rl->regs[0].address);
    luaL_addvalue(&buf);
    luaL_addchar(&buf, '=');
    lua_pushinteger(L, rl->regs[0].value);
    luaL_addvalue(&buf);
    size_t i;
    for(i = 1; i < rl->sz; i++){
        luaL_addchar(&buf, ',');
        lua_pushinteger(L, rl->regs[i].address);
        luaL_addvalue(&buf);
        luaL_addchar(&buf, '=');
        lua_pushinteger(L, rl->regs[i].value);
        luaL_addvalue(&buf);
    }
    luaL_addchar(&buf, '}');
    luaL_pushresult(&buf);
    return 1;
}

static int reglist_Lcall(lua_State* L){
    struct reglist* rl1 = (struct reglist*)luaL_checkudata(L, 1, REGLIST_META);
    lua_Integer val = luaL_checkinteger(L, 2);
    size_t i;
    for(i = 0; i < rl1->sz; i++){
        rl1->regs[i].value = val;
    }
    return 0;
}

static void init_reglist_meta(lua_State* L){
    static const luaL_Reg reglist_meta[] = {
        {"__index", reglist_Lindex},
        {"__newindex", reglist_Lnewindex},
        {"__len", reglist_Llen},
        {"__tostring", reglist_Ltostring},
        {"__call", reglist_Lcall},
        {NULL, NULL}
    };
    luaL_newmetatable(L, REGLIST_META);
    luaL_setfuncs(L, reglist_meta, 0);
    lua_pop(L, 1);
}

struct ondtimer{
    int64_t to;
    int interval;
    int prev_value;
};

static int ondtimer_Lcall(lua_State* L){
    struct ondtimer* odt = (struct ondtimer*)luaL_checkudata(L, 1, ONDTIMER_REF_META);
    int value = lua_toboolean(L, 2);
    int ms = luaL_optinteger(L, 3, 0);
    if(value){
        if(!odt->prev_value){
            odt->to = os_ticks() + odt->interval;
            lua_pushboolean(L, 0);
            lua_pushinteger(L, odt->interval < ms? odt->interval: ms);
        }else if(odt->to < 0){
            lua_pushboolean(L, 1);
            lua_pushinteger(L, ms);
        }else{
            msec diff = odt->to - os_ticks();
            if(diff <= 0){
                odt->to = -1;
                lua_pushboolean(L, 1);
                lua_pushinteger(L, ms);
            }else{
                lua_pushboolean(L, 0);
                lua_pushinteger(L, diff < ms? diff: ms);
            }
        }
    }else{
        lua_pushboolean(L, 0);
        lua_pushinteger(L, ms);
    }
    odt->prev_value = value;
    return 2;
}

static int ondtimer_Lnew(lua_State* L){
    int interval = luaL_checkinteger(L, 1);
    if(interval < 1){
        luaL_argerror(L, 1, "interval must be greater then zero");
    }
    struct ondtimer* odt = (struct ondtimer*)lua_newuserdata(L, sizeof(struct ondtimer));
    luaL_setmetatable(L, ONDTIMER_REF_META);
    odt->prev_value = 0;
    odt->interval = interval;
    return 1;
}

static void init_ondtimer_meta(lua_State* L){
    static const luaL_Reg ondtimer_meta[] = {
        {"__call", ondtimer_Lcall},
        {NULL, NULL}
    };
    luaL_newmetatable(L, ONDTIMER_REF_META);
    lua_pushvalue(L, -1);
    lua_setfield(L, -2, "__index");
    luaL_setfuncs(L, ondtimer_meta, 0);
    lua_pop(L, 1);
}

struct offdtimer{
    int64_t to;
    int interval;
    int prev_value;
};

static int offdtimer_Lcall(lua_State* L){
    struct offdtimer* odt = (struct offdtimer*)luaL_checkudata(L, 1, OFFDTIMER_REF_META);
    int value = lua_toboolean(L, 2);
    int ms = luaL_optinteger(L, 3, 0);
    if(value){
        lua_pushboolean(L, 1);
        lua_pushinteger(L, ms);
    }else{
        if(!odt->prev_value){
            odt->to = os_ticks() + odt->interval;
            lua_pushboolean(L, 1);
            lua_pushinteger(L, odt->interval < ms? odt->interval: ms);
        }else if(odt->to < 0){
            lua_pushboolean(L, 0);
            lua_pushinteger(L, ms);
        }else{
            msec diff = odt->to - os_ticks();
            if(diff <= 0){
                odt->to = -1;
                lua_pushboolean(L, 0);
                lua_pushinteger(L, ms);
            }else{
                lua_pushboolean(L, 1);
                lua_pushinteger(L, diff < ms? diff: ms);
            }
        }
    }
    odt->prev_value = value;
    return 2;
}

static int offdtimer_Lnew(lua_State* L){
    int interval = luaL_checkinteger(L, 1);
    if(interval < 1){
        luaL_argerror(L, 1, "interval must be greater then zero");
    }
    struct offdtimer* odt = (struct offdtimer*)lua_newuserdata(L, sizeof(struct offdtimer));
    luaL_setmetatable(L, OFFDTIMER_REF_META);
    odt->prev_value = 0;
    odt->interval = interval;
    odt->to = -1;
    return 1;
}

static void init_offdtimer_meta(lua_State* L){
    static const luaL_Reg offdtimer_meta[] = {
        {"__call", offdtimer_Lcall},
        {NULL, NULL}
    };
    luaL_newmetatable(L, OFFDTIMER_REF_META);
    lua_pushvalue(L, -1);
    lua_setfield(L, -2, "__index");
    luaL_setfuncs(L, offdtimer_meta, 0);
    lua_pop(L, 1);
}

struct mbusnode;

// ret == 0 successfull, < 0 process error, > 0 - modbus error
typedef int(*node_finish_callback)(lua_State* L, struct mbusnode* mnode, int ret);

struct mbusnode{
    int sock; // socket
    int maddr; // remote modbus address
    int isfourmi; // remote device is efourmidevice
    int isfourmittest; // fourmi test request counter
    uint16_t be_port; // remote port in network byte format
    int lock;
    uint8_t data[7 + 7 + 253]; // buffer for send/recv
    uint8_t* pdata; // in-buffer i/o position
    uint16_t unique; // current modbus request id
    uint16_t datalen; // in-buffer data len
    enum{
        stage_transmit, // transmit request in process
        stage_receivehead, // receive answer header
        stage_receivemessage // receive answer body
    } stage;
    int timeout; // request timeout
    node_finish_callback callback; // operation finish callback
    void* tag;
    char addr[]; // modbus remote address as IPv4 format or hostname
};

#define mbusnode_sheader(N)  ((N)->data)
#define mbusnode_smessage(N) ((N)->data + 7)
#define mbusnode_rheader(N)  ((N)->data + 7)
#define mbusnode_rmessage(N) ((N)->data + 7 + 7)

static int mbusnode_checkanswer(lua_State* L, struct mbusnode* mnode){
    int ret;
    if(memcmp(mbusnode_sheader(mnode), mbusnode_rheader(mnode), 5) != 0){
        ret = -EIO;
        goto __return;
    }
    if(mbusnode_sheader(mnode)[6] != mbusnode_rheader(mnode)[6]){
        ret = -EIO;
        goto __return;
    }
    if(mbusnode_rmessage(mnode)[0] & 0x80){
        if(mnode->datalen != 2){
            ret = -EIO;
            goto __return;
        }
        ret = mbusnode_rmessage(mnode)[1];
        goto __return;
    }
    ret = 0;
__return:
    return mnode->callback(L, mnode, ret);
}

static int KFUNCTION(mbusnode_request_nblock_K){
    KHEADER;
    // in stack(-1) struct mbusnode* mnode
    struct taskpool* ts = taskpool_get(L);
    if(!ts){
        luaL_error(L, "TASKPOOL_REF not found");
    }
    int ms;
    int ret;
    struct pollfd* opfd;
    struct pollfd* pfd = taskpoll_fin(L, ts, NULL, &ms); // count forever 1
    struct mbusnode* mnode = lua_touserdata(L, -1);
    switch(mnode->stage){
    case stage_transmit:
        if(ms == 0){
            ret = -ETIMEDOUT;
            goto __return;
        }
        if(pfd->revents & POLLOUT){
            ret = send(mnode->sock, mnode->pdata, mnode->datalen, 0);
            if(ret < 0){
                ret = -errno;
            }
            if(ret == mnode->datalen){
                mnode->stage = stage_receivehead;
                mnode->pdata = mbusnode_rheader(mnode);
                mnode->datalen = 7;
                struct pollfd* opfd = taskpoll_start(L, ts, 1, ms);
                opfd->fd = mnode->sock;
                opfd->events = POLLIN;
                lua_yieldk(L, 1, 0, mbusnode_request_nblock_K);
            }
            if(ret >= 0){
                mnode->pdata += ret;
                mnode->datalen -= ret;
                ret = -EAGAIN;
            }
            if(ret != -EAGAIN){
                goto __return;
            }
        }else if(pfd->revents & POLLERR){
            ret = -EIO;
            goto __return;
        }else if(pfd->revents & POLLHUP){
            ret = -ECONNRESET;
            goto __return;
        }
        opfd = taskpoll_start(L, ts, 1, ms);
        opfd->fd = mnode->sock;
        opfd->events = POLLOUT;
        lua_yieldk(L, 1, 0, mbusnode_request_nblock_K);
    case stage_receivehead:
        if(ms == 0){
            ret = -ETIMEDOUT;
            goto __return;
        }
        if(pfd->revents & POLLIN){
            ret = recv(mnode->sock, mnode->pdata, mnode->datalen, 0);
            if(ret < 0){
                ret = -errno;
            }
            if(ret == mnode->datalen){
                int datalen = (mbusnode_rheader(mnode)[4] << 8) + mbusnode_rheader(mnode)[5];
                if((datalen < 2) || (datalen > 254)){
                    ret = -EIO;
                    goto __return;
                }
                mnode->stage = stage_receivemessage;
                mnode->pdata = mbusnode_rmessage(mnode);
                mnode->datalen = datalen - 1;
                goto __stage_receivemessage;
            }else if(ret == 0){
                ret = -ECONNRESET;
                goto __return;
            }else if(ret > 0){
                mnode->pdata += ret;
                mnode->datalen -= ret;
                ret = -EAGAIN;
            }
            if(ret != -EAGAIN){
                goto __return;
            }
        }else if(pfd->revents & POLLERR){
            ret = -EIO;
            goto __return;
        }else if(pfd->revents & POLLHUP){
            ret = -ECONNRESET;
            goto __return;
        }
        opfd = taskpoll_start(L, ts, 1, ms);
        opfd->fd = mnode->sock;
        opfd->events = POLLIN;
        lua_yieldk(L, 1, 0, mbusnode_request_nblock_K);
__stage_receivemessage:
    case stage_receivemessage:
        if(ms == 0){
            ret = -ETIMEDOUT;
            goto __return;
        }
        if(pfd->revents & POLLIN){
            ret = recv(mnode->sock, mnode->pdata, mnode->datalen, 0);
            if(ret < 0){
                ret = -errno;
            }
            if(ret == mnode->datalen){
                mnode->datalen = mbusnode_rheader(mnode)[5] - 1;
                break;
            }else if(ret == 0){
                ret = -ECONNRESET;
                goto __return;
            }else if(ret > 0){
                mnode->pdata += ret;
                mnode->datalen -= ret;
                ret = -EAGAIN;
            }
            if(ret != -EAGAIN){
                goto __return;
            }
        }else if(pfd->revents & POLLERR){
            ret = -EIO;
            goto __return;
        }else if(pfd->revents & POLLHUP){
            ret = -ECONNRESET;
            goto __return;
        }
        opfd = taskpoll_start(L, ts, 1, ms);
        opfd->fd = mnode->sock;
        opfd->events = POLLIN;
        lua_yieldk(L, 1, 0, mbusnode_request_nblock_K);
    }
    lua_pop(L, 1);// pop struct node*
    return mbusnode_checkanswer(L, mnode);
__return:
    lua_pop(L, 1);// pop struct node*
    return mnode->callback(L, mnode, ret);
}

static int mbusnode_request_nblock(lua_State* L, struct mbusnode* mnode, struct taskpool* tpool){
    int ret = send(mnode->sock, mnode->pdata, mnode->datalen, 0);
    if(ret < 0){
        ret = -errno;
    }
    if(ret == mnode->datalen){
        struct pollfd* pfd = taskpoll_start(L, tpool, 1, mnode->timeout);
        pfd->fd = mnode->sock;
        pfd->events = POLLIN;
        mnode->stage = stage_receivehead;
        mnode->datalen = 7;
        mnode->pdata = mbusnode_rheader(mnode);
        lua_pushlightuserdata(L, mnode);
        lua_yieldk(L, 1, 0, mbusnode_request_nblock_K);
        return 0;
    }else if((ret < 0) && (ret != -EAGAIN)){
        return mnode->callback(L, mnode, ret);
    }else{
        if(mnode->timeout == 0){
            return mnode->callback(L, mnode, -ETIMEDOUT);
        }
        if(ret > 0){
            mnode->pdata += ret;
            mnode->datalen -= ret;
        }
        struct pollfd* pfd = taskpoll_start(L, tpool, 1, mnode->timeout);
        pfd->fd = mnode->sock;
        pfd->events = POLLOUT;
        lua_pushlightuserdata(L, mnode);
        lua_yieldk(L, 1, 0, mbusnode_request_nblock_K);
        return 0;
    }
}

static int mbusnode_request_block(lua_State* L, struct mbusnode* mnode){
    msec to = mnode->timeout < 0? -1: os_ticks() + mnode->timeout;
    msec opfinish;
    int ret;
    // send request
    while(1){
        ret = send(mnode->sock, mnode->pdata, mnode->datalen, 0);
        if(ret < 0){
            ret = -errno;
        }
        if(ret == mnode->datalen){
            mnode->pdata = mbusnode_rheader(mnode);
            mnode->datalen = 7;
            break;
        }
        if(to < 0){
            opfinish = 0;
        }else{
            opfinish = os_ticks();
            if(opfinish >= to){
                ret = -ETIMEDOUT;
                goto __return;
            }
        }
        if(ret > 0) {
            mnode->pdata += ret;
            mnode->datalen -= ret;
            ret = -EAGAIN;
        }
        if(ret == -EAGAIN){
            struct pollfd pfd;
            pfd.fd = mnode->sock;
            pfd.events = POLLOUT;
            ret = poll(&pfd, 1, to - opfinish);
            if(ret == 0){
                ret = -ETIMEDOUT;
                goto __return;
            }
            if(pfd.revents & POLLOUT){
                continue;
            }else if(pfd.revents & POLLERR){
                ret = -EIO;
                goto __return;
            }else if(pfd.revents & POLLHUP){
                ret = -ECONNRESET;
                goto __return;
            }
        }else if(ret < 0){
            ret = -errno;
            goto __return;
        }
    }
    // receive answer header
    while(1){
        ret = recv(mnode->sock, mnode->pdata, mnode->datalen, 0);
        if(ret < 0){
            ret = -errno;
        }
        if(ret == mnode->datalen){
            int datalen = (mbusnode_rheader(mnode)[4] << 8) + mbusnode_rheader(mnode)[5];
            if((datalen < 2) || (datalen > 254)){
                ret = -EIO;
                goto __return;
            }
            mnode->datalen = datalen - 1;
            mnode->pdata = mbusnode_rmessage(mnode);
            break;
        }
        if(to < 0){
            opfinish = 0;
        }else{
            opfinish = os_ticks();
            if(opfinish >= to){
                ret = -ETIMEDOUT;
                goto __return;
            }
        }
        if(ret > 0) {
            mnode->pdata += ret;
            mnode->datalen -= ret;
            ret = -EAGAIN;
        }
        if(ret == -EAGAIN){
            struct pollfd pfd;
            pfd.fd = mnode->sock;
            pfd.events = POLLIN;
            ret = poll(&pfd, 1, to - opfinish);
            if(ret == 0){
                ret = -ETIMEDOUT;
                goto __return;
            }
            if(pfd.revents & POLLIN){
                continue;
            }else if(pfd.revents & POLLERR){
                ret = -EIO;
                goto __return;
            }else if(pfd.revents & POLLHUP){
                ret = -ECONNRESET;
                goto __return;
            }
        }else if(ret < 0){
            ret = -errno;
            goto __return;
        }
    }
    // receive answer message
    while(1){
        ret = recv(mnode->sock, mnode->pdata, mnode->datalen, 0);
        if(ret < 0){
            ret = -errno;
        }
        if(ret == mnode->datalen){
            mnode->datalen = mbusnode_rheader(mnode)[5] - 1;
            break;
        }
        if(to < 0){
            opfinish = 0;
        }else{
            opfinish = os_ticks();
            if(opfinish >= to){
                ret = -ETIMEDOUT;
                goto __return;
            }
        }
        if(ret > 0) {
            mnode->pdata += ret;
            mnode->datalen -= ret;
            ret = -EAGAIN;
        }
        if(ret == -EAGAIN){
            struct pollfd pfd;
            pfd.fd = mnode->sock;
            pfd.events = POLLIN;
            ret = poll(&pfd, 1, to - opfinish);
            if(ret == 0){
                ret = -ETIMEDOUT;
                goto __return;
            }
            if(pfd.revents & POLLIN){
                continue;
            }else if(pfd.revents & POLLERR){
                ret = -EIO;
                goto __return;
            }else if(pfd.revents & POLLHUP){
                ret = -ECONNRESET;
                goto __return;
            }
        }else if(ret < 0){
            goto __return;
        }
    }
    return mbusnode_checkanswer(L, mnode);
__return:
    return mnode->callback(L, mnode, ret);
}

static int mbusnode_request(lua_State* L, struct mbusnode* mnode, const uint8_t* data, int datalen, int timeout, node_finish_callback callback){
    if((datalen < 1) || (datalen > 253)){
        return callback(L, mnode, -EINVAL);
    }
    mnode->stage = stage_transmit;
    mnode->pdata = mnode->data;
    mnode->data[0] = ++mnode->unique >> 8;
    mnode->data[1] = mnode->unique & 0xFF;
    mnode->data[2] = 0;
    mnode->data[3] = 0;
    mnode->data[4] = 0;
    mnode->data[5] = datalen + 1;
    mnode->data[6] = mnode->maddr;
    mnode->datalen = 7 + datalen;
    if(data){
        memcpy(mbusnode_smessage(mnode), data, datalen);
    }
    mnode->callback = callback;
    mnode->timeout = timeout;
    struct taskpool* ts = taskpool_get(L);
    if(!ts){
        return mbusnode_request_block(L, mnode);
    }else{
        return mbusnode_request_nblock(L, mnode, ts);
    }
}

static void mbusnode_close(struct mbusnode* mnode);
static int mbusnode_open_callback(lua_State* L, struct mbusnode* mnode, int ret){
    const uint8_t req17 = 17;
    mnode->isfourmittest--;
    if(ret < 0){
        mbusnode_close(mnode);
        lua_pushinteger(L, ret);
        return 1;
    }else if(ret == EXC_GATEWAYPATH){
        mbusnode_close(mnode);
        lua_pushinteger(L, -ECONNABORTED);
        return 1;
    }else if(ret == 0){
        if((mnode->datalen > (2 + 7 + 1)) && (mbusnode_rmessage(mnode)[1] > (7 + 1)) && !memcmp(mbusnode_rmessage(mnode) + 2, "efourmi",7)){
            mnode->isfourmi = 1;
        }
    }else if(mnode->isfourmittest > 0){
        return mbusnode_request(L, mnode, &req17, 1, fourmi_env(L, ENV_OPEN_TESTEFOURMI_TIMEOUT, ENV_OPEN_TESTEFOURMI_TIMEOUT_DEF), mbusnode_open_callback);
    }
    lua_pushinteger(L, 0);
    return 1;
}

static int mbusnode_open(lua_State* L, struct mbusnode* mnode){
    int ret;
    struct hostent* hent;
    struct sockaddr_in saddr;
    const uint8_t req17 = 17;
    if(mnode->sock >= 0){
        lua_pushinteger(L, 0);
        return 1;
    }
    mnode->sock = socket(AF_INET, SOCK_STREAM, 0);
    if(mnode->sock < 0){
        lua_pushinteger(L, -errno);
        return 1;
    }
    hent = gethostbyname(mnode->addr);
    if(hent == NULL){
        lua_pushinteger(L, -100000 - h_errno);
        return 1;
    }
    saddr.sin_family = AF_INET;
    saddr.sin_port = mnode->be_port;
    saddr.sin_addr = *(struct in_addr *) hent->h_addr_list[0];
    ret = connect(mnode->sock, (struct sockaddr*)&saddr, sizeof(struct sockaddr_in));
    if(ret != 0){
        ret = -errno;
        close(mnode->sock);
        mnode->sock = -1;
        lua_pushinteger(L, ret);
        return 1;
    }
    int opt = 1;
    ret = ioctl(mnode->sock, FIONBIO, &opt);
    if(ret != 0){
        ret = -errno;
        close(mnode->sock);
        mnode->sock = -1;
        lua_pushinteger(L, ret);
        return 1;
    }
    opt = 900;
    setsockopt(mnode->sock, SOL_SOCKET, SO_RCVBUF, &opt, sizeof(opt));
    opt = 900;
    setsockopt(mnode->sock, SOL_SOCKET, SO_SNDBUF, &opt, sizeof(opt));
    opt = 1;
    setsockopt(mnode->sock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
    opt = 1;
    setsockopt(mnode->sock, IPPROTO_TCP, TCP_NODELAY, &opt, sizeof(opt));
    opt = 1;
    setsockopt(mnode->sock, SOL_SOCKET, SO_KEEPALIVE, &opt, sizeof(opt));
    mnode->isfourmittest = fourmi_env(L, ENV_OPEN_TESTEFOURMI_COUNT, ENV_OPEN_TESTEFOURMI_COUNT_DEF);
    if(mnode->isfourmittest <= 0){
        lua_pushinteger(L, 0);
        return 1;
    }
    return mbusnode_request(L, mnode,&req17, 1, fourmi_env(L, ENV_OPEN_TESTEFOURMI_TIMEOUT, ENV_OPEN_TESTEFOURMI_TIMEOUT_DEF), mbusnode_open_callback);
}

static int mbusnode_Lopen(lua_State* L){
    return mbusnode_open(L, (struct mbusnode*)luaL_checkudata(L, 1, MBUSNODE_REF_META));
}

static void mbusnode_close(struct mbusnode* mnode){
    if(mnode->sock >= 0){
        close(mnode->sock);
        mnode->sock = -1;
        mnode->isfourmi = 0;
    }
}

static int mbusnode_Lclose(lua_State* L){
    struct mbusnode* mnode = (struct mbusnode*)luaL_checkudata(L, 1, MBUSNODE_REF_META);
    mbusnode_close(mnode);
    return 0;
}

static int mbusnode_Lgc(lua_State* L){
    struct mbusnode* mnode = (struct mbusnode*)luaL_testudata(L, 1, MBUSNODE_REF_META);
    if(mnode){
        free(mnode->tag);
        mbusnode_close(mnode);
    }
    return 0;
}

static int mbusnode_status(struct mbusnode* mnode){
    return mnode->sock >= 0;
}

static int mbusnode_Lstatus(lua_State* L){
    struct mbusnode* mnode = (struct mbusnode*)luaL_checkudata(L, 1, MBUSNODE_REF_META);
    lua_pushboolean(L, mbusnode_status(mnode));
    return 1;
}

static int mbusnode_addr(struct mbusnode* mnode){
    return mnode->maddr;
}

static int mbusnode_Laddr(lua_State* L){
    struct mbusnode* mnode = (struct mbusnode*)luaL_checkudata(L, 1, MBUSNODE_REF_META);
    lua_pushinteger(L, mbusnode_addr(mnode));
    return 1;
}

static int mbusnode_isefourmi(struct mbusnode* mnode){
    return mnode->isfourmi;
}

static int mbusnode_Lisefourmi(lua_State* L){
    struct mbusnode* mnode = (struct mbusnode*)luaL_checkudata(L, 1, MBUSNODE_REF_META);
    lua_pushinteger(L, mbusnode_isefourmi(mnode));
    return 1;
}

static int mbusnode_req_callback(lua_State* L, struct mbusnode* mnode, int ret){
    lua_pushinteger(L, ret);
    if(ret == 0){
        lua_pushlstring(L, (const char*)mbusnode_rmessage(mnode), mnode->datalen);
    }else{
        lua_pushnil(L);
    }
    return 2;
}

static int mbusnode_Lreq(lua_State* L){
    struct mbusnode* mnode = (struct mbusnode*)luaL_checkudata(L, 1, MBUSNODE_REF_META);
    size_t req_sz;
    const char* req = luaL_checklstring(L, 2, &req_sz);
    int timeout = luaL_optinteger(L, 3, -1);
    return mbusnode_request(L, mnode, (const uint8_t*)req, (int)req_sz, timeout, mbusnode_req_callback);
}

struct io_ctx{
    struct reglist* rl;
    size_t indfrom;
    size_t count;
    int usedef;
    int mbusfunc;
    lua_Integer def;
};

static int mbusnode_get1_next(lua_State* L, struct mbusnode* mnode, struct io_ctx* ctx);
static int mbusnode_get1_next_callback(lua_State* L, struct mbusnode* mnode, int ret){
    struct io_ctx* ctx = mnode->tag;
    if(ret == 0){
        if((mnode->datalen == (mbusnode_rmessage(mnode)[1] + 2)) && (mbusnode_rmessage(mnode)[1] == ((ctx->count + 7) / 8))){
            size_t i;
            for(i = 0; i < ctx->count; i++){
                ctx->rl->regs[ctx->indfrom + i].value = (mbusnode_rmessage(mnode)[2 + i / 8] >> (i % 8)) & 1;
            }
            if((ctx->indfrom + ctx->count) != ctx->rl->sz){
                return mbusnode_get1_next(L, mnode, ctx);
            }
        }else{
            ret = -EIO;
        }
    }
    if((ret != 0) && (ctx->usedef)){
        size_t i;
        for(i = ctx->indfrom; i < ctx->rl->sz; i++){
            ctx->rl->regs[i].value = ctx->def;
        }
    }
    free(mnode->tag);
    mnode->tag = NULL;
    mnode->lock = 0;
    lua_pushinteger(L, ret);
    return 1;
}

static int mbusnode_get1_next(lua_State* L, struct mbusnode* mnode, struct io_ctx* ctx){
    size_t indnext = ctx->indfrom + ctx->count;
    size_t count = 1;
    ctx->indfrom = indnext;
    mbusnode_smessage(mnode)[0] = ctx->mbusfunc;
    mbusnode_smessage(mnode)[1] = ctx->rl->regs[indnext].address >> 8;
    mbusnode_smessage(mnode)[2] = ctx->rl->regs[indnext].address & 0xFF;
    indnext++;
    while((indnext < ctx->rl->sz) && (ctx->rl->regs[indnext].address == (ctx->rl->regs[indnext - 1].address + 1) && (count <= 0x7D0))){
        indnext++;
        count++;
    }
    mbusnode_smessage(mnode)[3] = count >> 8;
    mbusnode_smessage(mnode)[4] = count & 0xFF;
    ctx->count = count;
    return mbusnode_request(L, mnode, NULL, 5, fourmi_env(L, ENV_OPEN_TESTEFOURMI_TIMEOUT, ENV_OPEN_TESTEFOURMI_TIMEOUT_DEF), mbusnode_get1_next_callback);
}

static int mbusnode_set1_next(lua_State* L, struct mbusnode* mnode, struct io_ctx* ctx);
static int mbusnode_set1_next_callback(lua_State* L, struct mbusnode* mnode, int ret){
    struct io_ctx* ctx = mnode->tag;
    if(ret == 0){
        if((mnode->datalen == 5)
                && (MAKEMBDWORD(mbusnode_rmessage(mnode), 1) == ctx->rl->regs[ctx->indfrom].address)
                && (MAKEMBDWORD(mbusnode_rmessage(mnode), 3) == ctx->count)){
            if((ctx->indfrom + ctx->count) != ctx->rl->sz){
                return mbusnode_set1_next(L, mnode, ctx);
            }
        }else{
            ret = -EIO;
        }
    }
    if((ret != 0) && (ctx->usedef)){
        size_t i;
        for(i = ctx->indfrom; i < ctx->rl->sz; i++){
            ctx->rl->regs[i].value = ctx->def;
        }
    }
    free(mnode->tag);
    mnode->tag = NULL;
    mnode->lock = 0;
    lua_pushinteger(L, ret);
    return 1;
}

static int mbusnode_set1_next(lua_State* L, struct mbusnode* mnode, struct io_ctx* ctx){
    size_t indnext = ctx->indfrom + ctx->count;
    size_t count = 1;
    size_t i;
    ctx->indfrom = indnext;
    mbusnode_smessage(mnode)[0] = ctx->mbusfunc;
    mbusnode_smessage(mnode)[1] = ctx->rl->regs[indnext].address >> 8;
    mbusnode_smessage(mnode)[2] = ctx->rl->regs[indnext].address & 0xFF;
    indnext++;
    while((indnext < ctx->rl->sz) && (ctx->rl->regs[indnext].address == (ctx->rl->regs[indnext - 1].address + 1) && (count <= 0x7B0))){
        indnext++;
        count++;
    }
    mbusnode_smessage(mnode)[3] = count >> 8;
    mbusnode_smessage(mnode)[4] = count & 0xFF;
    mbusnode_smessage(mnode)[5] = (count + 7) / 8;
    uint8_t* currentout = mbusnode_smessage(mnode) + 5;
    for(i = 0; i < count; i++){
        int mod = i % 8;
        if(mod == 0){
            *(++currentout) = ctx->rl->regs[ctx->indfrom + i].value? 1: 0;
        }else{
            *currentout |= ctx->rl->regs[ctx->indfrom + i].value? 1 << mod: 0;
        }
    }
    ctx->count = count;
    return mbusnode_request(L, mnode, NULL, 6 + mbusnode_smessage(mnode)[5], fourmi_env(L, ENV_OPEN_TESTEFOURMI_TIMEOUT, ENV_OPEN_TESTEFOURMI_TIMEOUT_DEF), mbusnode_set1_next_callback);
}

static int mbusnode_get16_next(lua_State* L, struct mbusnode* mnode, struct io_ctx* ctx);
static int mbusnode_get16_next_callback(lua_State* L, struct mbusnode* mnode, int ret){
    struct io_ctx* ctx = mnode->tag;
    if(ret == 0){
        if((mnode->datalen == (mbusnode_rmessage(mnode)[1] + 2)) && (mbusnode_rmessage(mnode)[1] == (ctx->count * 2))){
            size_t i;
            for(i = 0; i < ctx->count; i++){
                ctx->rl->regs[ctx->indfrom + i].value = (mbusnode_rmessage(mnode)[2 + 2 * i] << 8) | (mbusnode_rmessage(mnode)[2 + 2 * i + 1]);
            }
            if((ctx->indfrom + ctx->count) != ctx->rl->sz){
                return mbusnode_get16_next(L, mnode, ctx);
            }
        }else{
            ret = -EIO;
        }
    }
    if((ret != 0) && (ctx->usedef)){
        size_t i;
        for(i = ctx->indfrom; i < ctx->rl->sz; i++){
            ctx->rl->regs[i].value = ctx->def;
        }
    }
    free(mnode->tag);
    mnode->tag = NULL;
    mnode->lock = 0;
    lua_pushinteger(L, ret);
    return 1;
}

static int mbusnode_get16_next(lua_State* L, struct mbusnode* mnode, struct io_ctx* ctx){
    size_t indnext = ctx->indfrom + ctx->count;
    size_t count = 1;
    ctx->indfrom = indnext;
    mbusnode_smessage(mnode)[0] = ctx->mbusfunc;
    mbusnode_smessage(mnode)[1] = ctx->rl->regs[indnext].address >> 8;
    mbusnode_smessage(mnode)[2] = ctx->rl->regs[indnext].address & 0xFF;
    indnext++;
    while((indnext < ctx->rl->sz) && (ctx->rl->regs[indnext].address == (ctx->rl->regs[indnext - 1].address + 1) && (count <= 0x7D))){
        indnext++;
        count++;
    }
    mbusnode_smessage(mnode)[3] = count >> 8;
    mbusnode_smessage(mnode)[4] = count & 0xFF;
    ctx->count = count;
    return mbusnode_request(L, mnode, NULL, 5, fourmi_env(L, ENV_OPEN_TESTEFOURMI_TIMEOUT, ENV_OPEN_TESTEFOURMI_TIMEOUT_DEF), mbusnode_get16_next_callback);
}

static int mbusnode_set16_next(lua_State* L, struct mbusnode* mnode, struct io_ctx* ctx);
static int mbusnode_set16_next_callback(lua_State* L, struct mbusnode* mnode, int ret){
    struct io_ctx* ctx = mnode->tag;
    if(ret == 0){
        if((mnode->datalen == 5)
                && (MAKEMBDWORD(mbusnode_rmessage(mnode), 1) == ctx->rl->regs[ctx->indfrom].address)
                && (MAKEMBDWORD(mbusnode_rmessage(mnode), 3) == ctx->count)){
            if((ctx->indfrom + ctx->count) != ctx->rl->sz){
                return mbusnode_set16_next(L, mnode, ctx);
            }
        }else{
            ret = -EIO;
        }
    }
    if((ret != 0) && (ctx->usedef)){
        size_t i;
        for(i = ctx->indfrom; i < ctx->rl->sz; i++){
            ctx->rl->regs[i].value = ctx->def;
        }
    }
    free(mnode->tag);
    mnode->tag = NULL;
    mnode->lock = 0;
    lua_pushinteger(L, ret);
    return 1;
}

static int mbusnode_set16_next(lua_State* L, struct mbusnode* mnode, struct io_ctx* ctx){
    size_t indnext = ctx->indfrom + ctx->count;
    size_t count = 1;
    size_t i;
    ctx->indfrom = indnext;
    mbusnode_smessage(mnode)[0] = ctx->mbusfunc;
    mbusnode_smessage(mnode)[1] = ctx->rl->regs[indnext].address >> 8;
    mbusnode_smessage(mnode)[2] = ctx->rl->regs[indnext].address & 0xFF;
    indnext++;
    while((indnext < ctx->rl->sz) && (ctx->rl->regs[indnext].address == (ctx->rl->regs[indnext - 1].address + 1) && (count <= 0x7B))){
        indnext++;
        count++;
    }
    mbusnode_smessage(mnode)[3] = count >> 8;
    mbusnode_smessage(mnode)[4] = count & 0xFF;
    mbusnode_smessage(mnode)[5] = count * 2;
    uint8_t* currentout = mbusnode_smessage(mnode) + 6;
    for(i = 0; i < count; i++){
        currentout[0] = ctx->rl->regs[ctx->indfrom + i].value >> 8;
        currentout[1] = ctx->rl->regs[ctx->indfrom + i].value & 0xFF;
        currentout += 2;
    }
    ctx->count = count;
    return mbusnode_request(L, mnode, NULL, 6 + mbusnode_smessage(mnode)[5], fourmi_env(L, ENV_OPEN_TESTEFOURMI_TIMEOUT, ENV_OPEN_TESTEFOURMI_TIMEOUT_DEF), mbusnode_set16_next_callback);
}

static int mbusnode_io(lua_State* L, int mbusfunc, int(*nextfunc)(lua_State*, struct mbusnode*, struct io_ctx*), int dirout){
    struct io_ctx* ctx;
    struct mbusnode* mnode = (struct mbusnode*)luaL_checkudata(L, 1, MBUSNODE_REF_META);
    struct reglist* rl = (struct reglist*)luaL_checkudata(L, 2, REGLIST_META);
    int usedef;
    size_t i;
    lua_Integer def = lua_tointegerx(L, 3, &usedef);
    if(dirout){
        for(i = 0; i < rl->sz; i++){
            if((rl->regs[i].value < 0) || (rl->regs[i].value > 65535)){
                return luaL_argerror(L, 2, "registry value is out of bound 0..65535");
            }
        }
    }
    if(!mbusnode_status(mnode)){
        lua_pushinteger(L, -ENOTCONN);
        if(usedef){
            goto __filldef;
        }
        return 1;
    }
    if(mnode->lock){
        lua_pushinteger(L, -EBUSY);
        if(usedef){
            goto __filldef;
        }
        return 1;
    }
    ctx = malloc(sizeof(struct io_ctx));
    if(!ctx){
        luaL_error(L, "not enought memory");
    }
    mnode->lock = 1;
    mnode->tag = ctx;
    ctx->indfrom = 0;
    ctx->count = 0;
    ctx->rl = rl;
    ctx->usedef = usedef;
    ctx->def = def;
    ctx->mbusfunc = mbusfunc;
    //++ add efourmi optimized call
    return nextfunc(L, mnode, ctx);
__filldef:
    for(i = 0; i < rl->sz; i++){
        rl->regs[i].value = def;
    }
    return 1;
}

static int mbusnode_Lgetdi(lua_State* L){
    return mbusnode_io(L, 2, mbusnode_get1_next, 0);
}

static int mbusnode_Lgetdo(lua_State* L){
    return mbusnode_io(L, 1, mbusnode_get1_next, 0);
}

static int mbusnode_Lgetro(lua_State* L){
    return mbusnode_io(L, 4, mbusnode_get16_next, 0);
}

static int mbusnode_Lgetrw(lua_State* L){
    return mbusnode_io(L, 3, mbusnode_get16_next, 0);
}

static int mbusnode_Lsetdo(lua_State* L){
    return mbusnode_io(L, 15, mbusnode_set1_next, 1);
}

static int mbusnode_Lsetrw(lua_State* L){
    return mbusnode_io(L, 16, mbusnode_set16_next, 1);
}

static void init_mbusnode_meta(lua_State * L){
    static const luaL_Reg mbusnode_meta[] = {
        {"open", mbusnode_Lopen},
        {"close", mbusnode_Lclose},
        {"status", mbusnode_Lstatus},
        {"addr", mbusnode_Laddr},
        {"efourmi", mbusnode_Lisefourmi},
        {"req", mbusnode_Lreq},
        {"getdi", mbusnode_Lgetdi},
        {"getdo", mbusnode_Lgetdo},
        {"getro", mbusnode_Lgetro},
        {"getrw", mbusnode_Lgetrw},
        {"setdo", mbusnode_Lsetdo},
        {"setrw", mbusnode_Lsetrw},
        {"__gc", mbusnode_Lgc},
        {NULL, NULL}
    };
    luaL_newmetatable(L, MBUSNODE_REF_META);
    lua_pushvalue(L, -1);
    lua_setfield(L, -2, "__index");
    luaL_setfuncs(L, mbusnode_meta, 0);
    lua_pop(L, 1);
}

static int fourmi_mbusnode(lua_State* L){
    const char* addr = luaL_checkstring(L, 1);
    int port = luaL_checkinteger(L, 2);
    int mbusaddr = luaL_checkinteger(L, 3);
    size_t addr_len = strlen(addr);
    if(addr_len == 0){
        luaL_argerror(L, 1, "network address is too small");
    }
    if((port < 1) || (port > 65535)){
        luaL_argerror(L, 2, "port must be in range 1..65535");
    }
    if((mbusaddr < 0) || (mbusaddr > 255)){
        luaL_argerror(L, 3, "node address must be in range 0..255");
    }
    struct mbusnode* mnode = (struct mbusnode*)lua_newuserdata(L, sizeof(struct mbusnode) + strlen(addr) + 1);
    mnode->be_port = htons(port);
    mnode->sock = -1;
    mnode->lock = 0;
    mnode->isfourmi = 0;
    strcpy(mnode->addr, addr);
    mnode->maddr = mbusaddr;
    mnode->tag = NULL;
    luaL_setmetatable(L, MBUSNODE_REF_META);
    return 1;
}

static int fourmi_id(lua_State* L){
    struct taskpool* ts = taskpool_get(L);
    lua_pushinteger(L, ts? ts->acttask->func_sindex: 0);
    return 1;
}

static int KFUNCTION(fourmi_usleep_K){
    KHEADER;
    struct taskpool* tpool = taskpool_get(L);
    int ms;
    if(!tpool){
        luaL_error(L, "TASKPOOL_REF not found");
    }
    taskpoll_fin(L, tpool, NULL, &ms);
    if(ms > 0){
        taskpoll_start(L, tpool, 0, ms);
        lua_yieldk(L, 0, 0, fourmi_usleep_K);
    }
    return 0;
}

static int fourmi_usleep(lua_State* L){
    int ms = luaL_checkinteger(L, -1);
    struct taskpool* ts = taskpool_get(L);
    if(!ts){
        poll(NULL, 0, ms);
    }else{
        taskpoll_start(L, ts, 0, ms);
        lua_yieldk(L, 0, 0, fourmi_usleep_K);
    }
    return 0;
}

static int fourmi_run(lua_State* L){
    int taskcount = lua_gettop(L);
    int i;
    if(!taskcount){
        luaL_error(L, "at least one function require");
    }
    for(i = 1; i <= taskcount; i++){
        if(!lua_isfunction(L, i)){
            luaL_error(L, "parameter %d not a function", i);
        }
    }
    lua_getfield(L, LUA_REGISTRYINDEX, TASKPOOL_REF);
    int prevtpool = lua_gettop(L);
    struct taskpool* tpool = (struct taskpool*)lua_newuserdata(L, sizeof(struct taskpool));
    luaL_setmetatable(L, TASKPOOL_REF_META);
    lua_setfield(L, LUA_REGISTRYINDEX, TASKPOOL_REF);
    tpool->bank[0].count = 0;
    tpool->bank[0].size = 0;
    tpool->bank[0].polls = NULL;
    tpool->bank[1].count = 0;
    tpool->bank[1].size = 0;
    tpool->bank[1].polls = NULL;
    tpool->readbank = 0;
    tpool->timeout = -1;
    tpool->task_count = taskcount;
    tpool->activetask_count = taskcount;
    tpool->tasklist = (struct task*)malloc(sizeof(struct task) * tpool->task_count);
    if(!tpool->tasklist){
        luaL_error(L, "not enought memory");
    }
    for(i = 1; i <= taskcount; i++){
        tpool->tasklist[i-1].L = lua_newthread(L);
        tpool->tasklist[i-1].task_sindex = lua_gettop(L);
        tpool->tasklist[i-1].func_sindex = i;
        tpool->tasklist[i-1].bank_count = 0;
        tpool->tasklist[i-1].sleepto = -1;
    }
    for(i = 0; i < tpool->task_count; i++){
       tpool->acttask = &tpool->tasklist[i];
       lua_pushvalue(L, tpool->tasklist[i].func_sindex);
       lua_xmove(L, tpool->tasklist[i].L, 1);
       int r = lua_resume(tpool->tasklist[i].L, L, 0);
       if(r == LUA_OK){
           printf("task \"%i\" successfully finished\r\n", tpool->tasklist[i].func_sindex);
           tpool->tasklist[i].L = NULL;
           tpool->activetask_count--;
       }else if(r != LUA_YIELD){
           printf("task \"%i\" return: %i[%s]\r\n", tpool->tasklist[i].func_sindex, r, lua_tostring(tpool->tasklist[i].L, -1));
           lua_pop(L, 1);
           tpool->tasklist[i].L = NULL;
           tpool->activetask_count--;
       }
    }
    while(tpool->activetask_count){
        tpool->readbank ^= 1;
        poll(tpool->bank[tpool->readbank].polls, tpool->bank[tpool->readbank].count, tpool->timeout);
        tpool->bank[tpool->readbank ^ 1].count = 0;
        tpool->timeout = -1;
        //++++ add error handling
        for(i = 0; i < tpool->task_count; i++){
           if(tpool->tasklist[i].L){
               tpool->acttask = &tpool->tasklist[i];
               int r = lua_resume(tpool->tasklist[i].L, L, lua_gettop(tpool->tasklist[i].L));
               if(r == LUA_OK){
                   printf("task \"%i\" successfully finished\r\n", tpool->tasklist[i].func_sindex);
                   tpool->tasklist[i].L = NULL;
                   tpool->activetask_count--;
               }else if(r != LUA_YIELD){
                   printf("task \"%i\" return: %i[%s]\r\n", tpool->tasklist[i].func_sindex, r, lua_tostring(tpool->tasklist[i].L, -1));
                   tpool->tasklist[i].L = NULL;
                   tpool->activetask_count--;
                   lua_pop(L, 1);
               }
           }
        }
    }
    lua_pushvalue(L, prevtpool);
    lua_setfield(L, LUA_REGISTRYINDEX, TASKPOOL_REF);
    return 0;
}

LUAMOD_API int luaopen_fourmi(lua_State *L) {
    static const luaL_Reg fourmilib[] = {
        {"run", fourmi_run},
        {"usleep", fourmi_usleep},
        {"id", fourmi_id},
        {"mbusnode", fourmi_mbusnode},
        {"reglist", reglist_Lnew},
        {"ondtimer", ondtimer_Lnew},
        {"offdtimer", offdtimer_Lnew},
        {NULL, NULL}
    };
    luaL_newlib(L, fourmilib);
    init_taskpool_meta(L);
    init_mbusnode_meta(L);
    init_ondtimer_meta(L);
    init_offdtimer_meta(L);
    init_reglist_meta(L);
    return 1;
}
