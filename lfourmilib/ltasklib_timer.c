#include <poll.h>
#include "ltasklib.h"
#include "lauxlib.h"

#define TTIMER_PULSE_META                        "TTIMER_PULSE_META"
#define TTIMER_PULSEB_META                       "TTIMER_PULSEB_META"
#define TTIMER_ONDELAY_META                      "TTIMER_ONDELAY_META"
#define TTIMER_OFFDELAY_META                     "TTIMER_OFFDELAY_META"

static int check_period(lua_State* L, int arg){
    int period = luaL_checkinteger(L, arg);
    if(period < 1){
        luaL_argerror(L, arg, "timer period must be greater then zero");
    }
    return period;
}

struct pulse_timer{
    msec ttime;
    int period;
    enum{
        pulsetimer_off,
        pulsetimer_on
    }state;
};

static int timer_pulse(lua_State* L){
    int period = check_period(L, 1);
    struct pulse_timer* timer = lua_newuserdata(L, sizeof(struct pulse_timer));
    timer->period = period;
    timer->state = pulsetimer_off;
    luaL_setmetatable(L, TTIMER_PULSE_META);
    return 1;
}

static int timer_pulse_call(lua_State* L){
    struct pulse_timer* timer = luaL_checkudata(L, 1, TTIMER_PULSE_META);
    int b = lua_toboolean(L, 2);
    if(!b){
        timer->state = pulsetimer_off;
    }else{
        msec ticks = tasklib_msec();
        switch(timer->state){
            case pulsetimer_off:
                timer->state = pulsetimer_on;
                timer->ttime = ticks + timer->period;
            case pulsetimer_on:
                if(ticks >= timer->ttime){
                    timer->ttime = ticks + timer->period;
                    lua_pushboolean(L, 1);
                    return 1;
                }
        }
    }
    lua_pushboolean(L, 0);
    return 1;
}

static void timer_pulse_meta(lua_State* L){
    static const luaL_Reg pulse_meta[] = {
        {"__call", timer_pulse_call},
        {NULL, NULL}
    };
    luaL_newmetatable(L, TTIMER_PULSE_META);
    luaL_setfuncs(L, pulse_meta, 0);
    lua_pop(L, 1);
}

static int timer_pulseb(lua_State* L){
    int period = check_period(L, 1);
    struct pulse_timer* timer = lua_newuserdata(L, sizeof(struct pulse_timer));
    timer->period = period;
    timer->state = pulsetimer_off;
    luaL_setmetatable(L, TTIMER_PULSEB_META);
    return 1;
}

static int timer_pulseb_callK(lua_State* L, int status, const struct task_yeild_result* result){
    (void)status;
    if(result->ms > 0){
        task_yeild(L, timer_pulseb_callK, result->ms);
    }
    lua_pushboolean(L, 1);
    return 1;
}

static int timer_pulseb_call(lua_State* L){
    struct pulse_timer* timer = luaL_checkudata(L, 1, TTIMER_PULSEB_META);
    int b = lua_toboolean(L, 2);
    if(b){
        int sleeptime;
        msec ticks = tasklib_msec();
        switch(timer->state){
            case pulsetimer_off:
                timer->state = pulsetimer_on;
                sleeptime = timer->period;
                break;
            case pulsetimer_on:
                sleeptime = timer->ttime - ticks + timer->period;
                break;
        }
        timer->ttime = ticks + sleeptime;
        if(sleeptime <= 0){
            lua_pushboolean(L, 1);
            return 1;
        }
        if(tasklib_istask(L)){
            task_yeild(L, timer_pulseb_callK, sleeptime);
        }else{
            poll(NULL, 0, sleeptime);
        }
    }
    timer->state = pulsetimer_off;
    lua_pushboolean(L, 0);
    return 1;
}

static void timer_pulseb_meta(lua_State* L){
    static const luaL_Reg pulseb_meta[] = {
        {"__call", timer_pulseb_call},
        {NULL, NULL}
    };
    luaL_newmetatable(L, TTIMER_PULSEB_META);
    luaL_setfuncs(L, pulseb_meta, 0);
    lua_pop(L, 1);
}

struct ondelay_timer{
    msec ttime;
    enum {
        ondelaytimer_off,
        ondelaytimer_ondelay,
        ondelaytimer_on
    }state;
    int period;
};

static int timer_ondelay(lua_State* L){
    int period = check_period(L, 1);
    struct ondelay_timer* timer = lua_newuserdata(L, sizeof(struct ondelay_timer));
    timer->period = period;
    timer->state = ondelaytimer_off;
    luaL_setmetatable(L, TTIMER_ONDELAY_META);
    return 1;
}

static int timer_ondelay_call(lua_State* L){
    struct ondelay_timer* timer = luaL_checkudata(L, 1, TTIMER_ONDELAY_META);
    int input = lua_toboolean(L, 2);
    msec ticks;
    if(!input){
        timer->state = ondelaytimer_off;
        lua_pushboolean(L, 0);
        return 1;
    }
    ticks = tasklib_msec();
    switch(timer->state){
        case ondelaytimer_off:
            timer->state = ondelaytimer_ondelay;
            timer->ttime = ticks + timer->period;
        case ondelaytimer_ondelay:
            if(ticks < timer->ttime){
                lua_pushboolean(L, 0);
                return 1;
            }
            timer->state = ondelaytimer_on;
        case ondelaytimer_on:
            break;
    }
    lua_pushboolean(L, 1);
    return 1;
}

static void timer_ondelay_meta(lua_State* L){
    static const luaL_Reg ondelay_meta[] = {
        {"__call", timer_ondelay_call},
        {NULL, NULL}
    };
    luaL_newmetatable(L, TTIMER_ONDELAY_META);
    luaL_setfuncs(L, ondelay_meta, 0);
    lua_pop(L, 1);
}

struct offdelay_timer{
    msec ttime;
    enum {
        offdelaytimer_on,
        offdelaytimer_offdelay,
        offdelaytimer_off
    }state;
    int period;
};

static int timer_offdelay(lua_State* L){
    int period = check_period(L, 1);
    struct offdelay_timer* timer = lua_newuserdata(L, sizeof(struct offdelay_timer));
    timer->period = period;
    timer->state = offdelaytimer_off;
    luaL_setmetatable(L, TTIMER_OFFDELAY_META);
    return 1;
}

static int timer_offdelay_call(lua_State* L){
    struct offdelay_timer* timer = luaL_checkudata(L, 1, TTIMER_OFFDELAY_META);
    int input = lua_toboolean(L, 2);
    msec ticks;
    if(input){
        timer->state = offdelaytimer_on;
        lua_pushboolean(L, 1);
        return 1;
    }
    ticks = tasklib_msec();
    switch(timer->state){
        case offdelaytimer_on:
            timer->state = offdelaytimer_offdelay;
            timer->ttime = ticks + timer->period;
        case offdelaytimer_offdelay:
            if(ticks < timer->ttime){
                lua_pushboolean(L, 1);
                return 1;
            }
            timer->state = offdelaytimer_off;
        case offdelaytimer_off:
            break;
    }
    lua_pushboolean(L, 0);
    return 1;
}

static void timer_offdelay_meta(lua_State* L){
    static const luaL_Reg offdelay_meta[] = {
        {"__call", timer_offdelay_call},
        {NULL, NULL}
    };
    luaL_newmetatable(L, TTIMER_OFFDELAY_META);
    luaL_setfuncs(L, offdelay_meta, 0);
    lua_pop(L, 1);
}

int luaopen_task_timer(lua_State* L){
    static const luaL_Reg timerlib[] = {
      {"pulse", timer_pulse},
      {"pulseb", timer_pulseb},
      {"ondelay", timer_ondelay},
      {"offdelay", timer_offdelay},
      {NULL, NULL}
    };
    timer_pulse_meta(L);
    timer_pulseb_meta(L);
    timer_ondelay_meta(L);
    timer_offdelay_meta(L);
    luaL_newlib(L, timerlib);
    return 1;
}
