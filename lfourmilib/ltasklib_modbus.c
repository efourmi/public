#include "ltasklib.h"
#include "lauxlib.h"
#include "efourmi.h"
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <poll.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <netdb.h>

#define MAKEMBDWORD(B,I) ((uint16_t)(((B)[(I)] << 8) | ((B)[(I) + 1])))

#define MBUSNODE_REF_META                        "TMODBUS_MBUSNODE_META"
#define MOWIREREQ_REF_META                       "TMODBUS_OWIREREQ_META"

#define EXC_ILLFUNCTION                  (0x1)
#define EXC_ILLDATAADDR                  (0x2)
#define EXC_ILLDATAVALUE                 (0x3)
#define EXC_SERVERFAILURE                (0x4)
#define EXC_ACKNOLEDGE                   (0x5)
#define EXC_SERVERBUSY                   (0x6)
#define EXC_GATEWAYPATH                  (0xA)
#define EXC_GATEWAYNORESP                (0xB)

static int luaL_checkuint16(lua_State* L, int argn){
    int b;
    lua_Integer i = lua_tointegerx(L, argn, &b);
    if(!b || (i < 0) || (i > 65535)){
        luaL_argerror(L, argn, "argument must be an integer in range 0..65535");
    }
    return i;
}

static int luaL_checkuint1(lua_State* L, int argn){
    lua_Integer i;
    switch(lua_type(L, argn)){
    case LUA_TBOOLEAN:
        return lua_toboolean(L, argn);
    case LUA_TNUMBER:
        i = lua_tointeger(L, argn);
        if(!(i >> 1)){
            return i;
        }
        break;
    default:
        break;
    }
    luaL_argerror(L, argn, "argument must be a boolean or an integer in range 0..1");
    return 0;
}

struct mbusnode;

// ret == 0 successfull, < 0 process error, > 0 - modbus error
typedef int(*node_finish_callback)(lua_State* L, struct mbusnode* mnode, int ret);

struct mbusnode{
    int sock; // socket
    int address; // remote modbus address
    int isfourmi; // remote device is efourmidevice
    int timeout; // request timeout
    enum{
        stage_free,
        stage_transmit, // transmit request in process
        stage_receivehead, // receive answer header
        stage_receivemessage // receive answer body
    } stage;
    uint8_t* pdata; // in-buffer i/o position
    node_finish_callback callback; // operation finish callback
    void* tag; // pointer to associated data, autofree on mbusnode.__gc
    uint16_t be_port; // remote port in network byte format
    uint16_t unique; // current modbus request id
    uint16_t datalen; // in-buffer data len
    uint8_t data[7 + 7 + 253]; // buffer for send/recv
    char host[]; // modbus remote address as IPv4 format or hostname
};

#define mbusnode_sheader(N)  ((N)->data)
#define mbusnode_smessage(N) ((N)->data + 7)
#define mbusnode_rheader(N)  ((N)->data + 7)
#define mbusnode_rmessage(N) ((N)->data + 7 + 7)

static int mbusnode_request_finish(lua_State* L, struct mbusnode* mnode, int ret){
    mnode->stage = stage_free;
    return mnode->callback(L, mnode, ret);
}

static int mbusnode_request_finish_success(lua_State* L, struct mbusnode* mnode){
    int ret;
    do{
        // first 5 byte or sended/received message must be equel
        if(memcmp(mbusnode_sheader(mnode), mbusnode_rheader(mnode), 5) != 0){
            ret = -EIO;
            break;
        }
        // node address in sended/received message must be equel
        if(mbusnode_sheader(mnode)[6] != mbusnode_rheader(mnode)[6]){
            ret = -EIO;
            break;
        }
        // if receive a modbus error
        if(mbusnode_rmessage(mnode)[0] & 0x80){
            // and message length is corrected
            if(mnode->datalen != 2){
                ret = -EIO;
                break;
            }
            // return it
            ret = mbusnode_rmessage(mnode)[1];
            break;
        }
        ret = 0;
    }while(0);
    // finish
    return mbusnode_request_finish(L, mnode, ret);
}

static int mbusnode_request_nblock_K(lua_State* L, int status, const struct task_yeild_result* result){
    int ms = result->ms; // elapsed time
    int ret;
    struct pollfd* opfd; // polls for next yeild
    struct pollfd* pfd = result->polls; // ready polls
    struct mbusnode* mnode = lua_touserdata(L, -1); // in stack(-1) struct mbusnode* mnode
    (void)status; // not used, disable warnings
    switch(mnode->stage){
    case stage_free: // disable warnings
        break;
    case stage_transmit:
        if(pfd->revents & POLLOUT){ // we can transmit
            do{
                ret = send(mnode->sock, mnode->pdata, mnode->datalen, 0);
                if(ret < 0){
                    ret = -errno;
                }
            }while(ret == -EINTR);
            if(ret == mnode->datalen){ // transmit all data, begin to receive header
                mnode->stage = stage_receivehead;
                mnode->pdata = mbusnode_rheader(mnode);
                mnode->datalen = 7;
                opfd = task_newpoll(L, 1);
                opfd->fd = mnode->sock;
                opfd->events = POLLIN;
                task_yeild(L, mbusnode_request_nblock_K, ms);
            }else if(ret >= 0){ // transmition not compleate
                mnode->pdata += ret;
                mnode->datalen -= ret;
                ret = -EAGAIN;
            }
            if(ret != -EAGAIN){
                goto __return;
            }
        }else if(pfd->revents & POLLERR){ // some crazy happens, halt
            ret = -EIO;
            goto __return;
        }else if(pfd->revents & POLLHUP){ // connection reset, halt
            ret = -ECONNRESET;
            goto __return;
        }else if(ms == 0){
            ret = -ETIMEDOUT;
            goto __return;
        }
        // wait for transmition ability
        opfd = task_newpoll(L, 1);
        opfd->fd = mnode->sock;
        opfd->events = POLLOUT;
        task_yeild(L, mbusnode_request_nblock_K, ms);//yeild
    case stage_receivehead:
        if(pfd->revents & POLLIN){ // have IN flag
            do{
                ret = recv(mnode->sock, mnode->pdata, mnode->datalen, 0);
                if(ret < 0){
                    ret = -errno;
                }
            }while(ret == -EINTR);
            if(ret == mnode->datalen){ // receive whole header, receive body
                int datalen = (mbusnode_rheader(mnode)[4] << 8) + mbusnode_rheader(mnode)[5];
                if((datalen < 2) || (datalen > 254)){ // check body length
                    ret = -EIO;
                    goto __return;
                }
                mnode->stage = stage_receivemessage;
                mnode->pdata = mbusnode_rmessage(mnode);
                mnode->datalen = datalen - 1;
                goto __stage_receivemessage;
            }else if(ret == 0){ // connection closed, halt
                ret = -ECONNRESET;
                goto __return;
            }else if(ret > 0){ // receive is not compeate, yeild
                mnode->pdata += ret;
                mnode->datalen -= ret;
                ret = -EAGAIN;
            }
            if(ret != -EAGAIN){
                goto __return;
            }
        }else if(pfd->revents & POLLERR){ // some crazy happens, halt
            ret = -EIO;
            goto __return;
        }else if(pfd->revents & POLLHUP){ // connection reset, halt
            ret = -ECONNRESET;
            goto __return;
        }else if(ms == 0){
            ret = -ETIMEDOUT;
            goto __return;
        }
        // wait for new data
        opfd = task_newpoll(L, 1);
        opfd->fd = mnode->sock;
        opfd->events = POLLIN;
        task_yeild(L, mbusnode_request_nblock_K, ms);//yeild
__stage_receivemessage:
    case stage_receivemessage:
        if(pfd->revents & POLLIN){ // have IN flag
            do{
                ret = recv(mnode->sock, mnode->pdata, mnode->datalen, 0);
                if(ret < 0){
                    ret = -errno;
                }
            }while(ret == -EINTR);
            if(ret == mnode->datalen){ // receive whole body
                mnode->datalen = mbusnode_rheader(mnode)[5] - 1;
                break;
            }else if(ret == 0){ // connection closed, halt
                ret = -ECONNRESET;
                goto __return;
            }else if(ret > 0){ // receive is not compeate, yeild
                mnode->pdata += ret;
                mnode->datalen -= ret;
                ret = -EAGAIN;
            }
            if(ret != -EAGAIN){
                goto __return;
            }
        }else if(pfd->revents & POLLERR){ // some crazy happens, halt
            ret = -EIO;
            goto __return;
        }else if(pfd->revents & POLLHUP){ // connection reset, halt
            ret = -ECONNRESET;
            goto __return;
        }else if(ms == 0){
            ret = -ETIMEDOUT;
            goto __return;
        }
        // wait for new data
        opfd = task_newpoll(L, 1);
        opfd->fd = mnode->sock;
        opfd->events = POLLIN;
        task_yeild(L, mbusnode_request_nblock_K, ms);
    }
    // successfull exit, check answer
    lua_pop(L, 1);// pop struct node*
    return mbusnode_request_finish_success(L, mnode);
__return:
    // do error callback
    lua_pop(L, 1);// pop struct node*
    return mbusnode_request_finish(L, mnode, ret);
}

static int mbusnode_request_nblock(lua_State* L, struct mbusnode* mnode){
    int ret;
    if(mnode->stage != stage_free){
        return mbusnode_request_finish(L, mnode, -EBUSY);
    }
    // try send all the message
    do{
        ret = send(mnode->sock, mnode->pdata, mnode->datalen, 0);
        if(ret < 0){
            // send failed
            ret = -errno;
        }
    }while(ret == -EINTR);
    if(ret == mnode->datalen){ // send all the message successfull, prepare read header
        struct pollfd* pfd = task_newpoll(L, 1);
        pfd->fd = mnode->sock;
        pfd->events = POLLIN;
        mnode->stage = stage_receivehead;
        mnode->datalen = 7;
        mnode->pdata = mbusnode_rheader(mnode);
        lua_pushlightuserdata(L, mnode); // push mnode onto the stack top
        task_yeild(L, mbusnode_request_nblock_K, mnode->timeout); // yeild
    }else if((ret < 0) && (ret != -EAGAIN)){ // send return error
        return mbusnode_request_finish(L, mnode, ret); // do callback
    }else{ // send return EAGAIN, or not all data sended, try send in continuosly mode
        struct pollfd* pfd = task_newpoll(L, 1);
        pfd->fd = mnode->sock;
        pfd->events = POLLOUT;
        mnode->stage = stage_transmit;
        if(ret > 0){ // correct pdata, datalen
            mnode->pdata += ret;
            mnode->datalen -= ret;
        }
        lua_pushlightuserdata(L, mnode);
        task_yeild(L, mbusnode_request_nblock_K, mnode->timeout);
    }
    return 0; // disable warning
}

static int mbusnode_request_block(lua_State* L, struct mbusnode* mnode){
    msec to;
    msec opfinish;
    int ret;
    to = tasklib_msec() + mnode->timeout;
    // send request
    while(1){
        do{
            ret = send(mnode->sock, mnode->pdata, mnode->datalen, 0);
            if(ret < 0){
                ret = -errno;
            }
        }while(ret == -EINTR);
        if(ret == mnode->datalen){
            mnode->pdata = mbusnode_rheader(mnode);
            mnode->datalen = 7;
            break;
        }
        opfinish = tasklib_msec();
        if(opfinish >= to){
            ret = -ETIMEDOUT;
            goto __return;
        }
        if(ret > 0) {
            mnode->pdata += ret;
            mnode->datalen -= ret;
            ret = -EAGAIN;
        }
        if(ret == -EAGAIN){
            struct pollfd pfd;
            pfd.fd = mnode->sock;
            pfd.events = POLLOUT;
            ret = poll(&pfd, 1, to - opfinish);
            if(ret == 0){
                ret = -ETIMEDOUT;
                goto __return;
            }
            if(pfd.revents & POLLOUT){
                continue;
            }else if(pfd.revents & POLLERR){
                ret = -EIO;
                goto __return;
            }else if(pfd.revents & POLLHUP){
                ret = -ECONNRESET;
                goto __return;
            }
        }else if(ret < 0){
            ret = -errno;
            goto __return;
        }
    }
    // receive answer header
    while(1){
        do{
            ret = recv(mnode->sock, mnode->pdata, mnode->datalen, 0);
            if(ret < 0){
                ret = -errno;
            }
        }while(ret == -EINTR);
        if(ret == mnode->datalen){
            int datalen = (mbusnode_rheader(mnode)[4] << 8) + mbusnode_rheader(mnode)[5];
            if((datalen < 2) || (datalen > 254)){
                ret = -EIO;
                goto __return;
            }
            mnode->datalen = datalen - 1;
            mnode->pdata = mbusnode_rmessage(mnode);
            break;
        }
        opfinish = tasklib_msec();
        if(opfinish >= to){
            ret = -ETIMEDOUT;
            goto __return;
        }
        if(ret > 0) {
            mnode->pdata += ret;
            mnode->datalen -= ret;
            ret = -EAGAIN;
        }
        if(ret == -EAGAIN){
            struct pollfd pfd;
            pfd.fd = mnode->sock;
            pfd.events = POLLIN;
            ret = poll(&pfd, 1, to - opfinish);
            if(ret == 0){
                ret = -ETIMEDOUT;
                goto __return;
            }
            if(pfd.revents & POLLIN){
                continue;
            }else if(pfd.revents & POLLERR){
                ret = -EIO;
                goto __return;
            }else if(pfd.revents & POLLHUP){
                ret = -ECONNRESET;
                goto __return;
            }
        }else if(ret < 0){
            ret = -errno;
            goto __return;
        }
    }
    // receive answer message
    while(1){
        do{
            ret = recv(mnode->sock, mnode->pdata, mnode->datalen, 0);
            if(ret < 0){
                ret = -errno;
            }
        }while(ret == -EINTR);
        if(ret == mnode->datalen){
            mnode->datalen = mbusnode_rheader(mnode)[5] - 1;
            break;
        }
        opfinish = tasklib_msec();
        if(opfinish >= to){
            ret = -ETIMEDOUT;
            goto __return;
        }
        if(ret > 0) {
            mnode->pdata += ret;
            mnode->datalen -= ret;
            ret = -EAGAIN;
        }
        if(ret == -EAGAIN){
            struct pollfd pfd;
            pfd.fd = mnode->sock;
            pfd.events = POLLIN;
            ret = poll(&pfd, 1, to - opfinish);
            if(ret == 0){
                ret = -ETIMEDOUT;
                goto __return;
            }
            if(pfd.revents & POLLIN){
                continue;
            }else if(pfd.revents & POLLERR){
                ret = -EIO;
                goto __return;
            }else if(pfd.revents & POLLHUP){
                ret = -ECONNRESET;
                goto __return;
            }
        }else if(ret < 0){
            goto __return;
        }
    }
    // answer is received, check it for modbus errors
    return mbusnode_request_finish_success(L, mnode);
__return:
    // some error, do callback
    return mbusnode_request_finish(L, mnode, ret);
}

// NOT USE MNODE->TAG
static int mbusnode_request(lua_State* L, struct mbusnode* mnode, const uint8_t* data, int datalen, node_finish_callback callback){
    // test datalen in allowed range
    if((datalen < 1) || (datalen > 253)){
        return callback(L, mnode, -EINVAL);
    }
    // set variables
    mbusnode_sheader(mnode)[0] = ++mnode->unique >> 8;
    mbusnode_sheader(mnode)[1] = mnode->unique & 0xFF;
    mbusnode_sheader(mnode)[2] = 0;
    mbusnode_sheader(mnode)[3] = 0;
    mbusnode_sheader(mnode)[4] = 0;
    mbusnode_sheader(mnode)[5] = datalen + 1;
    mbusnode_sheader(mnode)[6] = mnode->address;
    mnode->pdata = mbusnode_sheader(mnode);
    mnode->datalen = 7 + datalen;
    if(data){
        memcpy(mbusnode_smessage(mnode), data, datalen);
    }
    mnode->callback = callback;
    // test execution mode
    if(!tasklib_istask(L)){
        // main thread
        return mbusnode_request_block(L, mnode);
    }else{
        // task thread
        return mbusnode_request_nblock(L, mnode);
    }
}

static void mbusnode_close(struct mbusnode* mnode){
    // if connection is opened
    if(mnode->sock >= 0){
        // close it
        close(mnode->sock);
        mnode->sock = -1;
    }
}

static int mbusnode_Lclose(lua_State* L){
    mbusnode_close((struct mbusnode*)luaL_checkudata(L, 1, MBUSNODE_REF_META));
    return 0;
}

static int mbusnode_open(lua_State* L, struct mbusnode* mnode){
    int ret, opt;
    struct hostent* hent;
    struct sockaddr_in saddr;
    // test if connection is allready opened
    if(mnode->sock >= 0){
        // return -EISCONN;
        lua_pushinteger(L, -EISCONN);
        return 1;
    }
    // try to open socket
    mnode->sock = socket(AF_INET, SOCK_STREAM, 0);
    if(mnode->sock < 0){
        lua_pushinteger(L, -errno);
        return 1;
    }
    // convert hostname to sockaddr
    hent = gethostbyname(mnode->host);
    if(hent == NULL){
        lua_pushinteger(L, -100000 - h_errno);
        return 1;
    }
    saddr.sin_family = AF_INET;
    saddr.sin_port = mnode->be_port;
    saddr.sin_addr = *(struct in_addr *) hent->h_addr_list[0];
    // try to connect
    ret = connect(mnode->sock, (struct sockaddr*)&saddr, sizeof(struct sockaddr_in));
    if(ret != 0){
        ret = -errno;
        close(mnode->sock);
        mnode->sock = -1;
        lua_pushinteger(L, ret);
        return 1;
    }
    // set to non blocked mode
    opt = 1;
    ret = ioctl(mnode->sock, FIONBIO, &opt);
    if(ret != 0){
        ret = -errno;
        close(mnode->sock);
        mnode->sock = -1;
        lua_pushinteger(L, ret);
        return 1;
    }
    // set other modbus needed parameters
    opt = 900;
    setsockopt(mnode->sock, SOL_SOCKET, SO_RCVBUF, &opt, sizeof(opt));
    opt = 900;
    setsockopt(mnode->sock, SOL_SOCKET, SO_SNDBUF, &opt, sizeof(opt));
    opt = 1;
    setsockopt(mnode->sock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
    opt = 1;
    setsockopt(mnode->sock, IPPROTO_TCP, TCP_NODELAY, &opt, sizeof(opt));
    opt = 1;
    setsockopt(mnode->sock, SOL_SOCKET, SO_KEEPALIVE, &opt, sizeof(opt));
    // reset stage
    mnode->stage = stage_free;
    // return success
    lua_pushinteger(L, 0);
    return 1;
}

static int mbusnode_Lopen(lua_State* L){
    return mbusnode_open(L, (struct mbusnode*)luaL_checkudata(L, 1, MBUSNODE_REF_META));
}

static int mbusnode_Lgc(lua_State* L){
    struct mbusnode* mnode = (struct mbusnode*)luaL_testudata(L, 1, MBUSNODE_REF_META);
    if(mnode){
        free(mnode->tag); // destroy associated pointer
        mbusnode_close(mnode); // close connection if opened
    }
    return 0;
}

static int mbusnode_status(struct mbusnode* mnode){
    int ret;
    if(mnode->sock < 0){
        return 0;
    }
    do{
        ret = send(mnode->sock, NULL, 0, 0);
    }while((ret == -1) && (errno == EINTR));
    if(ret < 0){
        mbusnode_close(mnode);
        return 0;
    }
    return 1;
}

static int mbusnode_Lstatus(lua_State* L){
    lua_pushboolean(L, mbusnode_status((struct mbusnode*)luaL_checkudata(L, 1, MBUSNODE_REF_META)));
    return 1;
}

static int mbusnode_address(struct mbusnode* mnode){
    return mnode->address;
}

static int mbusnode_Laddress(lua_State* L){
    lua_pushinteger(L, mbusnode_address((struct mbusnode*)luaL_checkudata(L, 1, MBUSNODE_REF_META)));
    return 1;
}

static int mbusnode_Ltimeout(lua_State* L){
    struct mbusnode* mnode = (struct mbusnode*)luaL_checkudata(L, 1, MBUSNODE_REF_META);
    if(lua_isinteger(L, 2)){
        lua_Integer i = lua_tointeger(L, 2);
        if((i <= 0) || (i > INT_MAX)){
            luaL_argerror(L, 2, "not in range");
        }
        mnode->timeout = i;
    }
    lua_pushinteger(L, mnode->timeout);
    return 1;
}

static int mbusnode_req_callback(lua_State* L, struct mbusnode* mnode, int ret){
    // push result code
    lua_pushinteger(L, ret);
    if(ret == 0){
        // push answer
        lua_pushlstring(L, (const char*)mbusnode_rmessage(mnode), mnode->datalen);
    }else{
        // no answer
        lua_pushnil(L);
    }
    return 2;
}

static int mbusnode_Lreq(lua_State* L){
    struct mbusnode* mnode = (struct mbusnode*)luaL_checkudata(L, 1, MBUSNODE_REF_META);
    size_t req_sz;
    const char* req = luaL_checklstring(L, 2, &req_sz);
    // check if mnode is used in other task
    if(mnode->stage != stage_free){
        // yes, it used, return -EBUSY
        lua_pushinteger(L, -EBUSY);
        return 1;
    }
    return mbusnode_request(L, mnode, (const uint8_t*)req, (int)req_sz, mbusnode_req_callback);
}

static int mbusnode_return_iarray(lua_State* L, int count, int value){
    int i = count;
    while(i--){
        lua_pushinteger(L, value);
    }
    return count;
}

static int mbusnode_get1_next_callback(lua_State* L, struct mbusnode* mnode, int ret){
    int count = lua_tointeger(L, 3);
    int bcount = (count + 7)  / 8;
    int i;
    if(ret > 0){
        ret = -10000 - ret;
    }else if(ret == 0){
         if((mnode->datalen == (2 + bcount)) && (mbusnode_rmessage(mnode)[1] == bcount)){
            for(i = 0; i < count; i++){
                lua_pushboolean(L, (mbusnode_rmessage(mnode)[2 + i / 8] >> (i % 8)) & 1);
            }
            return count;
         }else{
             ret = -EIO;
         }
    }
    return mbusnode_return_iarray(L, count, ret);
}

static int mbusnode_get1(lua_State* L, int mfunc){
    struct mbusnode* mnode = (struct mbusnode*)luaL_checkudata(L, 1, MBUSNODE_REF_META);
    int from = luaL_checkuint16(L, 2);
    int count = luaL_checkuint16(L, 3);
    if((from + count) > 65535){
        return mbusnode_return_iarray(L, count, -10000 - EXC_ILLDATAADDR);
    }else if(count > 0x7D0){
        return mbusnode_return_iarray(L, count, -10000 - EXC_ILLDATAVALUE);
    }else if(count == 0){
        return 0;
    }
    mbusnode_smessage(mnode)[0] = mfunc;
    mbusnode_smessage(mnode)[1] = from >> 8;
    mbusnode_smessage(mnode)[2] = from & 0xFF;
    mbusnode_smessage(mnode)[3] = count >> 8;
    mbusnode_smessage(mnode)[4] = count & 0xFF;
    return mbusnode_request(L, mnode, NULL, 5, mbusnode_get1_next_callback);
}

static int mbusnode_get16_next_callback(lua_State* L, struct mbusnode* mnode, int ret){
    int count = lua_tointeger(L, 3);
    int bcount = count * 2;
    int i;
    if(ret > 0){
        ret = -10000 - ret;
    }else if(ret == 0){
         if((mnode->datalen == (2 + bcount)) && (mbusnode_rmessage(mnode)[1] == bcount)){
            for(i = 0; i < count; i++){
                lua_pushinteger(L, (mbusnode_rmessage(mnode)[2 + 2 * i] << 8) | (mbusnode_rmessage(mnode)[2 + 2 * i + 1]));
            }
            return count;
         }else{
             ret = -EIO;
         }
    }
    return mbusnode_return_iarray(L, count, ret);
}

static int mbusnode_get16(lua_State* L, int mfunc){
    struct mbusnode* mnode = (struct mbusnode*)luaL_checkudata(L, 1, MBUSNODE_REF_META);
    int from = luaL_checkuint16(L, 2);
    int count = luaL_checkuint16(L, 3);
    if((from + count) > 65535){
        return mbusnode_return_iarray(L, count, -10000 - EXC_ILLDATAADDR);
    }else if(count > 0x7D){
        return mbusnode_return_iarray(L, count,-10000 - EXC_ILLDATAVALUE);
    }else if(count == 0){
        return 0;
    }
    mbusnode_smessage(mnode)[0] = mfunc;
    mbusnode_smessage(mnode)[1] = from >> 8;
    mbusnode_smessage(mnode)[2] = from & 0xFF;
    mbusnode_smessage(mnode)[3] = count >> 8;
    mbusnode_smessage(mnode)[4] = count & 0xFF;
    return mbusnode_request(L, mnode, NULL, 5, mbusnode_get16_next_callback);
}

static int mbusnode_set_next_callback(lua_State* L, struct mbusnode* mnode, int ret){
    int from = lua_tointeger(L, 2);
    int count = lua_gettop(L) - 2;
    if(ret > 0){
        ret = -10000 - ret;
    }else if(ret == 0){
        if((mnode->datalen != 5)
                || (MAKEMBDWORD(mbusnode_rmessage(mnode), 1) != from)
                || (MAKEMBDWORD(mbusnode_rmessage(mnode), 3) != count)){
            ret = -EIO;
        }else{
            return count;
        }
    }
    lua_pop(L, count);
    return mbusnode_return_iarray(L, count, ret);
}

static int mbusnode_setm_next_callback(lua_State* L, struct mbusnode* mnode, int ret){
    int from = lua_tointeger(L, 2);
    int value = lua_tointeger(L, 3);
    int mask = lua_tointeger(L, 4);
    if(ret > 0){
        ret = -10000 - ret;
    }else if(ret == 0){
        if((mnode->datalen != 7)
                || (MAKEMBDWORD(mbusnode_rmessage(mnode), 1) != from)
                || (MAKEMBDWORD(mbusnode_rmessage(mnode), 3) != mask)
                || (MAKEMBDWORD(mbusnode_rmessage(mnode), 5) != value)){
            ret = -EIO;
        }else{
            lua_pop(L, 1);
            return 1;
        }
    }
    lua_pushinteger(L, ret);
    return 1;
}

/* di[from]... = mnode::getdi(from, count) */
static int mbusnode_Lgetdi(lua_State* L){
    return mbusnode_get1(L, 2);
}
/* do[from]... = mnode::getdo(from, count) */
static int mbusnode_Lgetdo(lua_State* L){
    return mbusnode_get1(L, 1);
}
/* ro[from]... = mnode::getro(from, count) */
static int mbusnode_Lgetro(lua_State* L){
    return mbusnode_get16(L, 4);
}
/* rw[from]... = mnode::getrw(from, count) */
static int mbusnode_Lgetrw(lua_State* L){
    return mbusnode_get16(L, 3);
}
/* do[from] = mnode::setdo(from, value0...) */
static int mbusnode_Lsetdo(lua_State* L){
    struct mbusnode* mnode = (struct mbusnode*)luaL_checkudata(L, 1, MBUSNODE_REF_META);
    int from = luaL_checkuint16(L, 2);
    int count = lua_gettop(L) - 2;
    int i;
    uint8_t* currentout;
    if((from + count) > 65535){
        return mbusnode_return_iarray(L, count, -10000 - EXC_ILLDATAADDR);
    }else if(count > 0x7B0){
        return mbusnode_return_iarray(L, count, -10000 - EXC_ILLDATAVALUE);
    }else if(count == 0){
        return 0;
    }
    mbusnode_smessage(mnode)[0] = 15;
    mbusnode_smessage(mnode)[1] = from >> 8;
    mbusnode_smessage(mnode)[2] = from & 0xFF;
    mbusnode_smessage(mnode)[3] = count >> 8;
    mbusnode_smessage(mnode)[4] = count & 0xFF;
    mbusnode_smessage(mnode)[5] = (count + 7) / 8;
    currentout = mbusnode_smessage(mnode) + 5;
    for(i = 0; i < count; i++){
        int mod = i % 8;
        int value = luaL_checkuint1(L, 3 + i);
        if(mod == 0){
            *(++currentout) = value;
        }else{
            *currentout |= value << mod;
        }
    }
    return mbusnode_request(L, mnode, NULL, 6 + mbusnode_smessage(mnode)[5], mbusnode_set_next_callback);
}
/* rw[from] = mnode::setrw(from, value0...) */
static int mbusnode_Lsetrw(lua_State* L){
    struct mbusnode* mnode = (struct mbusnode*)luaL_checkudata(L, 1, MBUSNODE_REF_META);
    int from = luaL_checkuint16(L, 2);
    int count = lua_gettop(L) - 2;
    int i;
    uint8_t* currentout;
    if((from + count) > 65535){
        return mbusnode_return_iarray(L, count, -10000 - EXC_ILLDATAADDR);
    }else if(count > 0x7B){
        return mbusnode_return_iarray(L, count, -10000 - EXC_ILLDATAVALUE);
    }else if(count == 0){
        return 0;
    }
    mbusnode_smessage(mnode)[0] = 16;
    mbusnode_smessage(mnode)[1] = from >> 8;
    mbusnode_smessage(mnode)[2] = from & 0xFF;
    mbusnode_smessage(mnode)[3] = count >> 8;
    mbusnode_smessage(mnode)[4] = count & 0xFF;
    mbusnode_smessage(mnode)[5] = count * 2;
    currentout = mbusnode_smessage(mnode) + 6;
    for(i = 0; i < count; i++){
        int value = luaL_checkuint16(L, 3 + i);
        currentout[0] = value >> 8;
        currentout[1] = value & 0xFF;
        currentout += 2;
    }
    return mbusnode_request(L, mnode, NULL, 6 + mbusnode_smessage(mnode)[5], mbusnode_set_next_callback);
}
/* rw[from] = mnode::setrwm(from, value, mask) */
static int mbusnode_Lsetrwm(lua_State* L){
    struct mbusnode* mnode = (struct mbusnode*)luaL_checkudata(L, 1, MBUSNODE_REF_META);
    int from = luaL_checkuint16(L, 2);
    int value = luaL_checkuint16(L, 3);
    int mask = luaL_checkuint16(L, 4);
    if(mask == 0xFFFF){
        lua_pop(L, 1); // pop mask
        return 1;
    }
    mbusnode_smessage(mnode)[0] = 22;
    mbusnode_smessage(mnode)[1] = from >> 8;
    mbusnode_smessage(mnode)[2] = from & 0xFF;
    mbusnode_smessage(mnode)[3] = mask >> 8;
    mbusnode_smessage(mnode)[4] = mask & 0xFF;
    mbusnode_smessage(mnode)[5] = value >> 8;
    mbusnode_smessage(mnode)[6] = value & 0xFF;
    return mbusnode_request(L, mnode, NULL, 7, mbusnode_setm_next_callback);
}

struct owire_request{
    int (*toLuaResult)(lua_State* L, struct owire_request* req);
    const uint8_t* req;
    const size_t req_sz;
    uint8_t* ans;
    size_t ans_sz;
    int err;
    enum{
        STATUS_EMPTY = 0,
        STATUS_SUCCESS = EFOURMI_1WIRE_OP_RESULT_SUCCESS >> 4,
        STATUS_SUCCESSL = EFOURMI_1WIRE_OP_RESULT_SUCCESS_LAST >> 4,
        STATUS_NOANSWER = EFOURMI_1WIRE_OP_RESULT_NOANSWER >> 4,
        STATUS_ALAST = EFOURMI_1WIRE_OP_RESULT_ALAST >> 4,
        STATUS_CMDERR = EFOURMI_1WIRE_OP_RESULT_CMDERR >> 4,
        STATUS_UNDEF = 0x10,
        STATUS_CRCFAULT = 0x11
    } status;
    uint8_t data[];
};

static void init_mbusnode_meta(lua_State * L){
    static const luaL_Reg mbusnode_meta[] = {
        {"open", mbusnode_Lopen},
        {"close", mbusnode_Lclose},
        {"status", mbusnode_Lstatus},
        {"address", mbusnode_Laddress},
        {"timeout", mbusnode_Ltimeout},
        {"req", mbusnode_Lreq},
        {"getdi", mbusnode_Lgetdi},
        {"getdo", mbusnode_Lgetdo},
        {"getro", mbusnode_Lgetro},
        {"getrw", mbusnode_Lgetrw},
        {"setdo", mbusnode_Lsetdo},
        {"setrw", mbusnode_Lsetrw},
        {"setrwm", mbusnode_Lsetrwm},
        {"__gc", mbusnode_Lgc},
        {NULL, NULL}
    };
    luaL_newmetatable(L, MBUSNODE_REF_META);
    lua_pushvalue(L, -1);
    lua_setfield(L, -2, "__index");
    luaL_setfuncs(L, mbusnode_meta, 0);
    lua_pop(L, 1);
}

static int owr_Lerr(lua_State* L){
    struct owire_request* owr = (struct owire_request*)luaL_checkudata(L, 1, MOWIREREQ_REF_META);
    lua_pushinteger(L, owr->err);
    return 1;
}

static int owr_Lstatus(lua_State* L){
    struct owire_request* owr = (struct owire_request*)luaL_checkudata(L, 1, MOWIREREQ_REF_META);
    lua_pushinteger(L, owr->status);
    return 1;
}

static int owr_Lresult(lua_State* L){
    struct owire_request* owr = (struct owire_request*)luaL_checkudata(L, 1, MOWIREREQ_REF_META);
    return owr->toLuaResult(L, owr);
}

static void init_owr_meta(lua_State * L){
    static const luaL_Reg owr_meta[] = {
        {"err", owr_Lerr},
        {"status", owr_Lstatus},
        {"result", owr_Lresult},
        {NULL, NULL}
    };
    luaL_newmetatable(L, MOWIREREQ_REF_META);
    lua_pushvalue(L, -1);
    lua_setfield(L, -2, "__index");
    luaL_setfuncs(L, owr_meta, 0);
    lua_pop(L, 1);
}

static int table_boolfield(lua_State* L, int arg, const char* name, int def){
    int r;
    if(lua_getfield(L, arg, name) == LUA_TBOOLEAN){
        r = lua_toboolean(L, -1);
    }else{
        r = def;
    }
    lua_pop(L, 1);
    return r;
}

static int table_intfield(lua_State* L, int arg, const char* name, int def){
    int r;
    if(lua_getfield(L, arg, name) == LUA_TNUMBER){
        r = lua_tointeger(L, -1);
    }else{
        r = def;
    }
    lua_pop(L, 1);
    return r;
}

static const char* table_strfield(lua_State* L, int arg, const char* name, const char* def){
    const char* r;
    if(lua_getfield(L, arg, name) == LUA_TSTRING){
        r = lua_tostring(L, -1);
    }else{
        r = def;
    }
    lua_pop(L, 1);
    return r;
}

static int mbusnode(lua_State* L){
    luaL_checktype(L, 1, LUA_TTABLE);
    const char* host = table_strfield(L, 1, "host", NULL);
    int port = table_intfield(L, 1, "port", -1);
    int address = table_intfield(L, 1, "address", -1);
    int efourmi = table_boolfield(L, 1, "efourmi", 0);
    if((host == NULL) || (strlen(host) == 0)){
        luaL_argerror(L, 1, "'host' attribute invalid");
    }
    if((port < 1) || (port > 65535)){
        luaL_argerror(L, 2, "'port' attribute must be in range 1..65535");
    }
    if((address < 0) || (address > 255)){
        luaL_argerror(L, 3, "'address' attribute must be in range 0..255");
    }
    struct mbusnode* mnode = (struct mbusnode*)lua_newuserdata(L, sizeof(struct mbusnode) + strlen(host) + 1);
    mnode->be_port = htons(port);
    mnode->sock = -1;
    mnode->isfourmi = efourmi;
    strcpy(mnode->host, host);
    mnode->address = address;
    mnode->tag = NULL;
    mnode->timeout = 1000;
    luaL_setmetatable(L, MBUSNODE_REF_META);
    return 1;
}

int luaopen_task_modbus(lua_State* L){
    static const luaL_Reg modbuslib[] = {
        {"remote", mbusnode},
        {NULL, NULL}
    };
    init_mbusnode_meta(L);
    init_owr_meta(L);
    luaL_newlib(L, modbuslib);
    return 1;
}
