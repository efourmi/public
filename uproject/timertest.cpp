#include "efourmi.hpp"

using namespace efourmi;

static volatile uint32_t ticks1ms = 0;

static void sleep(int ms){
    uint32_t to = ticks1ms + ms;
    while(((int32_t)to - (int32_t)ticks1ms) > 0){
        __WFI();
    }
}

IRQ_HANDLER(SysTick_Handler){
    ticks1ms++;
}

static ModbusBuffer mbuffer;

static ModbusNode node100(100, mbuffer, sleep); // io configuration IIIIIIII

static ModbusNode self(0, mbuffer, sleep); // io configuration OOOOOOOO

static int node100in0;
static bool node100in0_6;
static int self_out0;

static PulseTimer scan_timer(ticks1ms, 50);
static OnDelayTimer self_o6_ondt(ticks1ms, 1000);
static OffDelayTimer self_o7_offdt(ticks1ms, 2000);
static IOSession self_session(self);
static IOSession node100_session(node100);

int main(void){
    cmd_regirq(SysTick_IRQn, SysTick_Handler);
    SysTick_Config(cmd_envget(EFOURMI_ENV_SYSFREQ) / 1000);
    self_session.addRWWriter(EFOURMI_RWREG_DO + 0, ~(3 << 6), self_out0);
    node100_session.addROReader(EFOURMI_ROREG_DI + 0, node100in0);
    while(1){
        if(scan_timer.Do(true)){
            node100_session.Do();
            if(node100in0 < 0){
                node100in0_6 = false;
            }else{
                node100in0_6 = (node100in0 >> 6) & 1;
            }
            self_out0 = 0;
            if(self_o6_ondt.Do(node100in0_6)){
                self_out0 |= 1 << 6;
            }
            if(self_o7_offdt.Do(node100in0_6)){
                self_out0 |= 1 << 7;
            }
            self_session.Do();
        }
    }
}
