
; Stack definition
Stack_Size      EQU     0x00000400

                AREA    STACK, NOINIT, READWRITE, ALIGN=3
Stack_Mem       SPACE   Stack_Size
__initial_sp

; Heap definition
Heap_Size       EQU     0x00000800

                AREA    HEAP, NOINIT, READWRITE, ALIGN=3
__heap_base
Heap_Mem        SPACE   Heap_Size
__heap_limit

; Shared read-only memory declaration
				IF :DEF:RODATA_SZ
ROData_size     EQU     RODATA_SZ
				ELSE
ROData_size     EQU     0; set ROData size in words
				ENDIF
					
; Shared read-write memory declaration
				IF :DEF:RWDATA_SZ
RWData_size     EQU     RWDATA_SZ
				ELSE
RWData_size     EQU     0; set RWData size in words
				ENDIF

; Shared read-only memory definition				
                IF ROData_size > 0

                AREA    ||.bss||, NOINIT, READWRITE, ALIGN=1
                EXPORT  ROData
ROData
__ROData
                SPACE   ROData_size + ROData_size
__ROData_size   EQU     ROData_size

                ELSE

__ROData        EQU     0
__ROData_size   EQU     0

                ENDIF
					
; Shared read-write memory definition
                IF RWData_size > 0

                AREA    ||.bss||, NOINIT, READWRITE, ALIGN=1
                EXPORT  RWData
RWData
__RWData
                SPACE   RWData_size + RWData_size
__RWData_size   EQU     RWData_size

                ELSE

__RWData        EQU     0
__RWData_size   EQU     0

                ENDIF

; Vector table definition
                PRESERVE8
                THUMB

                AREA    RESET, DATA, READONLY
                EXPORT  __Vectors
                EXPORT  __Vectors_End
                EXPORT  __Vectors_Size

__Vectors       
__EFHeader      DCD     0x756F6665, 0x00696D72         ; Magics, do not change
                DCD     __initial_sp                   ; Stack initial
                DCD     Startup_Handler                ; Startup handler
                DCD     __ROData                       ; RO data pointer
                DCD     __ROData_size                  ; RO data size
                DCD     __RWData                       ; RW data pointer
                DCD     __RWData_size                  ; RW data size
__EFHeader_End
__EFIOConf  	DCB     "OOOOOOOO"                     ; IO Configuration
__EFIOConf_End
__EFConfig		DCW     1                              ; Master mode active
				DCW     1152                           ; Modbus speed = 115,2kbps
				DCW     0                              ; Modbus parity = none
				DCW     1                              ; Modbus address = 1
				DCW     88                             ; Modbus answer wait time = 11bit * 8 symbols
				DCW     47                             ; TIM PSC = 47
				DCW     999                            ; TIM ARR = 999
				DCW     7                              ; ADC SMPR = 7
				DCW     1000                           ; OWire session lock timeout 1000ms
				DCW     0x6666                         ; USB VID
				DCW     0x0001                         ; USB PID
__EFConfig_End
__Vectors_End

__Vectors_Size  EQU  __Vectors_End - __Vectors
				ASSERT (__EFHeader_End - __EFHeader) == 32
				ASSERT (__EFIOConf_End - __EFIOConf) == 8
				ASSERT (__EFConfig_End - __EFConfig) == 22

                AREA    |.text|, CODE, READONLY

Startup_Handler PROC
                EXPORT  Startup_Handler                [WEAK]
                IMPORT  __main
                LDR     R1, =0x20002000
                STR     R0, [R1]
                LDR     R1, =__main
                BX      R1
                ENDP

                ALIGN

                IF      :DEF:__MICROLIB

                EXPORT  __initial_sp
                EXPORT  __heap_base
                EXPORT  __heap_limit

                ELSE

                IMPORT  __use_two_region_memory
                EXPORT  __user_initial_stackheap
                 
__user_initial_stackheap

                 LDR     R0, =  Heap_Mem
                 LDR     R1, =(Stack_Mem + Stack_Size)
                 LDR     R2, = (Heap_Mem +  Heap_Size)
                 LDR     R3, = Stack_Mem
                 BX      LR

                 ALIGN

                 ENDIF

                 END
