; Shared read-only memory declaration
				IF :DEF:RODATA_SZ
ROData_size     EQU     RODATA_SZ
				ELSE
ROData_size     EQU     0; set ROData size in words
				ENDIF
					
; Shared read-write memory declaration
				IF :DEF:RWDATA_SZ
RWData_size     EQU     RWDATA_SZ
				ELSE
RWData_size     EQU     0; set RWData size in words
				ENDIF

; Shared read-only memory definition				
                IF ROData_size > 0

                AREA    ||.bss||, NOINIT, READWRITE, ALIGN=1
                EXPORT  ROData
ROData
__ROData
                SPACE   ROData_size + ROData_size
__ROData_size   EQU     ROData_size

                ELSE

__ROData        EQU     0
__ROData_size   EQU     0

                ENDIF
					
; Shared read-write memory definition
                IF RWData_size > 0

                AREA    ||.bss||, NOINIT, READWRITE, ALIGN=1
                EXPORT  RWData
RWData
__RWData
                SPACE   RWData_size + RWData_size
__RWData_size   EQU     RWData_size

                ELSE

__RWData        EQU     0
__RWData_size   EQU     0

                ENDIF

; Vector table definition
                PRESERVE8
                THUMB

                AREA    RESET, DATA, READONLY
                EXPORT  __Vectors
                EXPORT  __Vectors_End
                EXPORT  __Vectors_Size

__Vectors       
__EFHeader      DCD     0x756F6665, 0x00696D72         ; Magics, do not change
                DCD     0x00000000                     ; Stack initial
                DCD     0x00000000                     ; Startup handler
                DCD     __ROData                       ; RO data pointer
                DCD     __ROData_size                  ; RO data size
                DCD     __RWData                       ; RW data pointer
                DCD     __RWData_size                  ; RW data size
__EFHeader_End
__EFIOConf  	DCB     "IIIIIIII"                     ; IO Configuration
__EFIOConf_End
__EFConfig		DCW     0                              ; Master mode active
				DCW     1152                           ; Modbus speed = 115,2kbps
				DCW     0                              ; Modbus parity = none
				DCW     100                            ; Modbus address = 100
				DCW     88                             ; Modbus answer wait time = 11bit * 8 symbols
				DCW     47                             ; TIM PSC = 47
				DCW     999                            ; TIM ARR = 999
				DCW     7                              ; ADC SMPR = 7
				DCW     1000                           ; OWire session lock timeout 1000ms
				DCW     0x6666                         ; USB VID
				DCW     0x0001                         ; USB PID
__EFConfig_End
__Vectors_End

__Vectors_Size  EQU  __Vectors_End - __Vectors
				ASSERT (__EFHeader_End - __EFHeader) == 32
				ASSERT (__EFIOConf_End - __EFIOConf) == 8
				ASSERT (__EFConfig_End - __EFConfig) == 22

                END
