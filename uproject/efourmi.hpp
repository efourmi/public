#ifndef __EFOURMI_HPP
#define __EFOURMI_HPP

#include <cstdint>
#include <cstddef>
#include "efourmi.h"
#ifdef EFOURMI_LITE
#include "stm32f0xx.h"
#endif

extern "C" {
    extern volatile uint16_t ROData[];
    extern volatile uint16_t RWData[];

    typedef void irq_handler(void);
    typedef int cmd_handler(int cmd, ...);

}

#define p_cmd_handler ((cmd_handler**)(0x20002000))

#define cmd_regirq(IRQN, HANDLER) ((**p_cmd_handler)(EFOURMI_CMD_REGISTERIRQ, (IRQN), (HANDLER)))

#define cmd_envget(ENVN) ((**p_cmd_handler)(EFOURMI_CMD_ENVGET, (ENVN)))

#define cmd_envset(ENVN, VALUE) ((**p_cmd_handler)(EFOURMI_CMD_ENVSET, (ENVN), (VALUE)))

#define cmd_regget(REGN) ((**p_cmd_handler)(EFOURMI_CMD_REGGET, (REGN)))

#define cmd_roregget(REGN) (cmd_regget(0x10000 | (REGN)))

#define cmd_regset(REGN, VALUE) ((**p_cmd_handler)(EFOURMI_CMD_REGSET, (REGN), (VALUE)))

#define cmd_modbus(DATA, DATALEN) ((**p_cmd_handler)(EFOURMI_CMD_MODBUS, (DATA), (DATALEN)))


#define IRQ_HANDLER(handler)                     extern "C" void handler(void)


#define MODBUSD_EXC_ILLFUNCTION                  (0x1)
#define MODBUSD_EXC_ILLDATAADDR                  (0x2)
#define MODBUSD_EXC_ILLDATAVALUE                 (0x3)
#define MODBUSD_EXC_SERVERFAILURE                (0x4)
#define MODBUSD_EXC_ACKNOLEDGE                   (0x5)
#define MODBUSD_EXC_SERVERBUSY                   (0x6)
#define MODBUSD_EXC_GATEWAYPATH                  (0xA)
#define MODBUSD_EXC_GATEWAYNORESP                (0xB)

namespace efourmi {

enum OWireResult {
    OWR_EMPTY = 0,
    OWR_SUCCESS = EFOURMI_1WIRE_OP_RESULT_SUCCESS >> 4,
    OWR_SUCCESSL = EFOURMI_1WIRE_OP_RESULT_SUCCESS_LAST >> 4,
    OWR_NOANSWER = EFOURMI_1WIRE_OP_RESULT_NOANSWER >> 4,
    OWR_ALAST = EFOURMI_1WIRE_OP_RESULT_ALAST >> 4,
    OWR_CNDERR = EFOURMI_1WIRE_OP_RESULT_CMDERR >> 4,
    OWR_UNDEF = 0x10,
    OWR_CRCFAULT = 0x11
};

enum OWireSearch {
    SearchContinue = 0x00, // pseudo code, not use directly
    SearchNormal = 0xF0,
    SearchAlarm = 0xEC
};

typedef void (sleep_function)(int ms);
typedef uint8_t Reg1;
typedef uint16_t Reg16;

class Timer{
protected:
    volatile uint32_t& ticks;
    uint32_t ttime;
    Timer(volatile uint32_t& ticks): ticks(ticks){}
    inline bool isTicksGTEQ(uint32_t t){
        return ((int32_t)this->ticks - (int32_t)t) >= 0;
    }
};

class PulseTimer: public Timer{
    int32_t period;
    enum {
        pulsetimer_off,
        pulsetimer_on
    }state;
public:
    PulseTimer(volatile uint32_t& ticks, int32_t period): Timer(ticks), period(period), state(pulsetimer_off){}
    bool Do(bool input);
};
class OnDelayTimer: public Timer{
    int32_t period;
    enum {
        ondelaytimer_off,
        ondelaytimer_ondelay,
        ondelaytimer_on
    }state;
public:
    OnDelayTimer(volatile uint32_t& ticks, int32_t period): Timer(ticks), period(period), state(ondelaytimer_off){}
    bool Do(bool input);
};

class OffDelayTimer: public Timer{
    int32_t period;
    enum {
        offdelaytimer_on,
        offdelaytimer_offdelay,
        offdelaytimer_off
    }state;
public:
    OffDelayTimer(volatile uint32_t& ticks, int32_t period): Timer(ticks), period(period), state(offdelaytimer_off){}
    bool Do(bool input);
};

struct ModbusBuffer{
    uint8_t buffer[254];
};

class ModbusNode {
    uint8_t* buffer;
    sleep_function& sleep;
    int address;
public:
    ModbusNode(int address, ModbusBuffer& mbuffer, sleep_function& sleep);
    uint8_t* getBuffer()const;
    int getAddress()const;
    bool isLocal()const;
    int doRequest(const uint8_t* data, int& datalen);
    void Sleep(int ms)const;
public:
    int getDI(uint16_t from, uint16_t count, Reg1* regs);
    int getDO(uint16_t from, uint16_t count, Reg1* regs);
    int setDO(uint16_t from, uint16_t count, const Reg1* reg);
    int getRO(uint16_t from, uint16_t count, Reg16* regs);
    int getRW(uint16_t from, uint16_t count, Reg16* regs);
    int setRW(uint16_t from, uint16_t count, const Reg16* reg);
    int setRWM(uint16_t index, uint16_t mask, Reg16 reg);
};


class OWireID {
    uint8_t id[8];
public:
    OWireID();
    OWireID(const OWireID& owid);
    OWireID(uint64_t id64);
    OWireID(const uint8_t* id8);
    operator bool()const;
    OWireID& operator=(const uint8_t* id8);
    OWireID& operator=(uint64_t id64);
    const uint8_t* getRaw()const;
    void Clear();
};

struct OWireReqBase;

struct OWireReqFuncs {
    std::size_t (*getSize)(OWireReqBase* base);
    std::size_t (*getRuntime)(OWireReqBase* base);
    void (*Push)(OWireReqBase* base, uint8_t** buffer);
    void (*Pop)(OWireReqBase* base, uint8_t** buffer, int& blen);
    void (*Destroy)(OWireReqBase* base);
};

struct OWireReqBase {
    const OWireReqFuncs* funcs;
    OWireReqBase* next;
    OWireResult* result;
    OWireReqBase(const OWireReqFuncs* funcs, OWireResult* result): funcs(funcs), next(NULL), result(result) {}
};

class OWireSession {
    ModbusNode& node;
    OWireReqBase* first;
    OWireReqBase** plast;
public:
    OWireSession(ModbusNode& node);
    ~OWireSession();
public:
    void Clean();
    int Do();
public:
    OWireSession& addFSearch(OWireID& id, enum OWireSearch scode, OWireResult* result = NULL);
    OWireSession& addSearch(OWireID& id, OWireResult* result = NULL);
    OWireSession& addDelay(int ms, OWireResult* result = NULL);
    OWireSession& addRExch(const uint8_t* dataout, uint8_t* datain, std::size_t bitcount, OWireResult* result = NULL);
    OWireSession& addAllConvertT(OWireResult* result = NULL);
    OWireSession& addDS18B20TempReader(const OWireID& id, int& temp, OWireResult* result = NULL);
    OWireSession& addDS18B20TempReader(const OWireID& id, float& temp, OWireResult* result = NULL);
public:
    static int getCRC(const uint8_t* data, std::size_t datalen);
};

}
#endif
