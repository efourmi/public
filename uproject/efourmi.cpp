#include "efourmi.hpp"
#include <cassert>
#include <cstring>
#include <cstdlib>


using namespace efourmi;

#ifdef EFOURMI_LITE
    struct efourmi_uheader_lite* header = (struct efourmi_uheader_lite*)0x8008000;
#endif
/*
    class PulseTimer
*/
bool PulseTimer::Do(bool input){
    if(!input){
        this->state = pulsetimer_off;
        return false;
    }
    switch(this->state){
        case pulsetimer_off:
            this->state = pulsetimer_on;
            this->ttime = this->ticks + this->period;
        case pulsetimer_on:
            if(this->isTicksGTEQ(this->ttime)){
                this->ttime = this->ticks + this->period;
                return true;
            }            
    }
    return false;
}
/*
    class OnDelayTimer
*/
bool OnDelayTimer::Do(bool input){
    if(!input){
        this->state = ondelaytimer_off;
        return false;
    }
    switch(this->state){
        case ondelaytimer_off:
            this->state = ondelaytimer_ondelay;
            this->ttime = this->ticks + this->period;
        case ondelaytimer_ondelay:
            if(!this->isTicksGTEQ(this->ttime)){
                return false;
            }
            this->state = ondelaytimer_on;
        case ondelaytimer_on:
            break;
    }
    return true;
}
/*
    class OffDelayTimer
*/
bool OffDelayTimer::Do(bool input){
    if(input){
        this->state = offdelaytimer_on;
        return true;
    }
    switch(this->state){
        case offdelaytimer_on:
            this->state = offdelaytimer_offdelay;
            this->ttime = this->ticks + this->period;
        case offdelaytimer_offdelay:
            if(!this->isTicksGTEQ(this->ttime)){
                return true;
            }
            this->state = offdelaytimer_off;
        case offdelaytimer_off:
            break;
    }
    return false;
}
/*
    class ModbusNode
*/

ModbusNode::ModbusNode(int address, ModbusBuffer& mbuffer, sleep_function& sleep)
  : buffer(mbuffer.buffer), sleep(sleep){
    if((address > 0) && (address <= 247)){
        this->address = address;
    }else{
        this->address = 0;
    }
}
    
uint8_t* ModbusNode::getBuffer()const{
    return this->buffer + 1;
}
int ModbusNode::getAddress()const{
    return this->address? this->address: header->config[EFOURMICFG_MODBUS_ADDRESS];
}
bool ModbusNode::isLocal()const{
    return this->address == 0;
}
int ModbusNode::doRequest(const uint8_t* data, int& datalen){
    int alen;
    if((datalen < 1) || (datalen > 253)){
        return EFOURMI_EINVAL;
    }
    if(data){
        std::memcpy(this->buffer + 1, data, datalen);
    }
    this->buffer[0] = this->getAddress();
    alen = cmd_modbus(this->buffer, datalen + 1);
    if(alen < 0){
        return alen;
    }
    datalen = alen - 1;
    return 0;
}
void ModbusNode::Sleep(int ms)const{
    this->sleep(ms);
}
static int get1(ModbusNode& node, uint16_t from, uint16_t count, Reg1* regs, int func){
    uint8_t* buffer = node.getBuffer();
    int dlen = 5;
    int ret;
    buffer[0] = func;
    buffer[1] = from >> 8;
    buffer[2] = from;
    buffer[3] = count >> 8;
    buffer[4] = count;
    ret = node.doRequest(NULL, dlen);
    if(!ret){
        if((dlen == (2 + (count + 7) / 8) && (buffer[1] == (count + 7) / 8))){
            const uint8_t* pbyte = buffer + 2;
            while(count){
                unsigned int bits = count % 8;
                unsigned int byte = *pbyte++;
                switch(bits){
                    case 0:
                        bits = 8;
                        *regs++ = byte & 1;
                        byte >>= 1;
                    case 7:
                        *regs++ = byte & 1;
                        byte >>= 1;
                    case 6:
                        *regs++ = byte & 1;
                        byte >>= 1;
                    case 5:
                        *regs++ = byte & 1;
                        byte >>= 1;
                    case 4:
                        *regs++ = byte & 1;
                        byte >>= 1;
                    case 3:
                        *regs++ = byte & 1;
                        byte >>= 1;
                    case 2:
                        *regs++ = byte & 1;
                        byte >>= 1;
                    case 1:
                        *regs++ = byte & 1;
                }
                count -= bits;
            }
            return 0;
        }else{
            return EFOURMI_EIO;
        }
    }
    return ret;
}
int ModbusNode::getDI(uint16_t from, uint16_t count, Reg1* regs){
    return get1(*this, from, count, regs, 2);
}
int ModbusNode::getDO(uint16_t from, uint16_t count, Reg1* regs){
    return get1(*this, from, count, regs, 1);
}
int ModbusNode::setDO(uint16_t from, uint16_t count, const Reg1* reg){
    uint8_t* buffer = this->getBuffer();
    int dlen = 6 + (count + 7) / 8;
    int ret;
    int i;
    buffer[0] = 15;
    buffer[1] = from >> 8;
    buffer[2] = from;
    buffer[3] = count >> 8;
    buffer[4] = count;
    buffer[5] = (count + 7) / 8;
    for(i = 0; i < count; i++){
        uint8_t* byte = buffer + 6 + (i / 8);
        int bit = i % 8;
        if(!bit){
            *byte = reg[i]? 1: 0;
        }else{
            *byte |= (reg[i]? 1: 0) << bit;
        }
    }
    ret = this->doRequest(NULL, dlen);
    if(ret){
        return ret;
    }else if(dlen != 5){
        return EFOURMI_EIO;
    }
    return 0;
}
static int get16(ModbusNode& node, uint16_t from, uint16_t count, Reg16* regs, int func){
    uint8_t* buffer = node.getBuffer();
    int dlen = 5;
    int ret;
    buffer[0] = func;
    buffer[1] = from >> 8;
    buffer[2] = from;
    buffer[3] = count >> 8;
    buffer[4] = count;
    ret = node.doRequest(NULL, dlen);
    if(!ret){
        if((dlen == (2 + count * 2) && (buffer[1] == count * 2))){
            const uint8_t* pbyte = buffer + 2;
            while(count--){
                *regs++ = (pbyte[0] << 8) | pbyte[1];
                pbyte += 2;
            }
            return 0;
        }else{
            return EFOURMI_EIO;
        }
    }
    return ret;
}
int ModbusNode::getRO(uint16_t from, uint16_t count, Reg16* regs){
    return get16(*this, from, count, regs, 4);
}
int ModbusNode::getRW(uint16_t from, uint16_t count, Reg16* regs){
    return get16(*this, from, count, regs, 3);
}
int ModbusNode::setRW(uint16_t from, uint16_t count, const Reg16* reg){
    uint8_t* buffer = this->getBuffer();
    uint8_t* out = buffer + 6;
    int dlen = 6 + count * 2;
    int ret;
    buffer[0] = 16;
    buffer[1] = from >> 8;
    buffer[2] = from;
    buffer[3] = count >> 8;
    buffer[4] = count;
    buffer[5] = count * 2;
    while(count--){
        out[0] = reg[0] >> 8;
        out[1] = reg[0];
        out += 2;
        reg++;
    }    
    ret = this->doRequest(NULL, dlen);
    if(ret){
        return ret;
    }else if(dlen != 5){
        return EFOURMI_EIO;
    }
    return 0;
}
int ModbusNode::setRWM(uint16_t index, uint16_t mask, Reg16 reg){
    uint8_t* buffer = this->getBuffer();
    int dlen = 7;
    int ret;
    buffer[0] = 22;
    buffer[1] = index >> 8;
    buffer[2] = index;
    buffer[3] = mask >> 24;
    buffer[4] = mask >> 16;
    buffer[5] = reg >> 8;
    buffer[6] = reg;
    ret = this->doRequest(NULL, dlen);
    if(!ret){
        if(dlen == 7){
            return 0;
        }else{
            return EFOURMI_EIO;
        }
    }
    return ret;
}
/*
    class OWireID
*/
static void memcpy8i(uint8_t* dest, const uint8_t* src){
    for(int i = 0; i < 8; i++){
        dest[i] = src[7 - i];
    }
}
OWireID::OWireID(){
    std::memset(this->id, 0, 8);
}
OWireID::OWireID(const OWireID& owid){
    std::memcpy(this->id, owid.id, 8);
}
OWireID::OWireID(uint64_t id64){
    memcpy8i(this->id, (const uint8_t*)&id64);
}
OWireID::OWireID(const uint8_t* id8){
    std::memcpy(this->id, id8, 8);
}
OWireID::operator bool()const{
    return OWireSession::getCRC(this->id, 8) == 0;
}
OWireID& OWireID::operator=(const uint8_t* id8){
    std::memcpy(this->id, id8, 8);
    return *this;
}
OWireID& OWireID::operator=(uint64_t id64){
    memcpy8i(this->id, (const uint8_t*)&id64);
    return *this;
}
const uint8_t* OWireID::getRaw()const{
    return this->id;
}
void OWireID::Clear(){
    std::memset(this->id, 0, 8);
}

/*
    class OWireSession
*/

#define REQUEST_BASE(TYPE, PVAR) ((TYPE*)(((uint8_t*)PVAR) - offsetof(TYPE, base)))
static void owreq_setresult(OWireReqBase* base, OWireResult owr){
    if(base->result){
        base->result[0] = static_cast<OWireResult>(owr);
    }
}
OWireSession::OWireSession(ModbusNode& node)
  :node(node){
    this->first = NULL;
    this->plast = &this->first;
}
OWireSession::~OWireSession(){
    this->Clean();
}
void OWireSession::Clean(){
    OWireReqBase* req = this->first;
    while(req){
        OWireReqBase* reqn = req->next;
        req->funcs->Destroy(req);
        req = reqn;
    }
    this->first = NULL;
    this->plast = &this->first;
}
static void do_cleanresults(OWireReqBase* freq){
    while(freq){
        owreq_setresult(freq, OWR_EMPTY);
        freq = freq->next;
    }
}
static OWireReqBase* do_pushlist(OWireReqBase* req, uint8_t** data, std::size_t& pruntime){
    int bufferleft = EFOURMI_1WIRE_BUFFERLEN;
    std::size_t runtime = 0;
    while(req){
        bufferleft -= req->funcs->getSize(req);
        if(bufferleft < 0){
            break;
        }
        runtime += req->funcs->getRuntime(req);
        req->funcs->Push(req, data);
        req = req->next;
    }
    pruntime = runtime;
    return req;
}
static int do_getres(ModbusNode& node, int session_id, std::size_t runtime, int& datalen){
    int rtd128 = (runtime + 127) / 128;
    int ms = (runtime + rtd128 + 1023) / 1024;
    int acount = 4;
    int ret;
    rtd128 = (rtd128 + 1023) / 1024;
    node.Sleep(ms);
    while(1){        
        uint8_t* buffer = node.getBuffer();
        buffer[0] = 70;
        buffer[1] = EFOURMI_1WIRE_CMD_GETRES;
        buffer[2] = session_id;
        int reqlen = 3;
        ret = node.doRequest(NULL, reqlen);
        if(ret != EFOURMI_ENAK){
            datalen = reqlen;
            break;
        }
        if(--acount == 0){
            ret = EFOURMI_EFAULT;
            break;
        }
        node.Sleep(rtd128);
    }
    return ret;
}
int OWireSession::Do(){
    OWireReqBase* freq = this->first;
    if(!freq){ // nothing to do
        return 0;
    }
    do_cleanresults(freq);
    int ret;
    int session_id;
    uint8_t* buffer = this->node.getBuffer();
    uint8_t* owbuffer = buffer + 3;
    std::size_t runtime;
    buffer[0] = 70;
    buffer[1] = EFOURMI_1WIRE_CMD_NEWTRANS;
    // buffer[2] = later
    OWireReqBase* nreq = do_pushlist(freq, &owbuffer, runtime);
    int reqlen = owbuffer - buffer;
    buffer[2] = reqlen - 3;
    ret = this->node.doRequest(NULL, reqlen);
    if((ret == 0) && (reqlen == 3)){
        session_id = buffer[2];
        while(1){
            ret = do_getres(this->node, session_id, runtime, reqlen);
            if((ret == 0) && (reqlen >= 3) && (reqlen == (buffer[3] + 4))){
                int alen = buffer[3];
                owbuffer = buffer + 4;
                OWireReqBase* owr = freq;
                while(owr != nreq){
                    owr->funcs->Pop(owr, &owbuffer, alen);
                    owr = owr->next;
                }
            }else if(ret == 0){
                ret = EFOURMI_EIO;
                break;
            }else{
                break;
            }
            if(nreq == NULL){
                buffer[0] = 70;
                buffer[1] = EFOURMI_1WIRE_CMD_FIN;
                buffer[2] = session_id;
                reqlen = 3;
                ret = node.doRequest(NULL, reqlen);
                break;
            }
            freq = nreq;
            buffer[0] = 70;
            buffer[1] = EFOURMI_1WIRE_CMD_CONT;
            buffer[2] = session_id;
            owbuffer = buffer + 4;
            nreq = do_pushlist(freq, &owbuffer, runtime);
            int reqlen = owbuffer - buffer;
            buffer[3] = reqlen - 4;
            ret = node.doRequest(NULL, reqlen);
            if((ret == 0) && (reqlen == 3)){
                
            }else if(ret == 0){
                ret = EFOURMI_EIO;
                break;
            }else{
                break;
            }
        }
    }else if(ret == 0){
        ret = EFOURMI_EIO;
    }
    return ret;
}
struct owreq_search{
    OWireReqBase base;
    OWireID& id;
    OWireSearch scode;
    owreq_search(const OWireReqFuncs* funcs, OWireResult* result, OWireID& id, OWireSearch scode = SearchContinue)
      : base(funcs, result), id(id), scode(scode){
          
    }
};
static std::size_t owreq_search_getsize(OWireReqBase* base){
    (void)base;
    return 9;
}
static std::size_t owreq_search_getruntime(OWireReqBase* base){
    (void)base;
    return ((1 + 1) * EFOURMI_1WIRE_TIMING_RESET) + ((1 + 8 + 64 * 3) * EFOURMI_1WIRE_TIMING_BIT);    
}
static void owreq_search_push(OWireReqBase* base, uint8_t** buffer){
    owreq_search* owreq = REQUEST_BASE(owreq_search, base);
    uint8_t* out = *buffer;
    if(owreq->scode != SearchContinue){
        out[0] = EFOURMI_1WIRE_OP_CMD_FSEARCH;
        out[1] = owreq->scode;
        *buffer = out + 2;
    }else{
        out[0] = EFOURMI_1WIRE_OP_CMD_SEARCH;
        *buffer = out + 1;
    }    
}
static void owreq_search_pop(OWireReqBase* base, uint8_t** buffer, int& blen){
    owreq_search* owreq = REQUEST_BASE(owreq_search, base);
    uint8_t* out = *buffer;
    do{
        if(out == NULL){
            break;
        }
        if(blen < 1){
            break;
        }
        if(    ((out[0] & EFOURMI_1WIRE_OP_CMD_MASK) != EFOURMI_1WIRE_OP_CMD_FSEARCH)
            && ((out[0] & EFOURMI_1WIRE_OP_CMD_MASK) != EFOURMI_1WIRE_OP_CMD_SEARCH)){
            break;
        }
        owreq_setresult(&owreq->base, static_cast<OWireResult>((out[0] & EFOURMI_1WIRE_OP_RESULT_MASK) >> 4));
        switch(out[0] & EFOURMI_1WIRE_OP_RESULT_MASK){
            case EFOURMI_1WIRE_OP_RESULT_SUCCESS:
            case EFOURMI_1WIRE_OP_RESULT_SUCCESS_LAST:
                if(blen < 9){
                    break;
                }
                owreq->id = out + 1;
                *buffer = out + 9;
                blen -= 9;
                return;
            default:
                *buffer = out + 1;
                blen -= 1;
                return;
        }
    }while(0);
    owreq_setresult(&owreq->base, OWR_UNDEF);
    *buffer = NULL;
    return;
}
static void owreq_search_destroy(OWireReqBase* base){
    owreq_search* owreq = REQUEST_BASE(owreq_search, base);
    delete owreq;
}
OWireSession& OWireSession::addFSearch(OWireID& id, enum OWireSearch scode, OWireResult* result){
    static const OWireReqFuncs owreq_search_funcs = {owreq_search_getsize, owreq_search_getruntime, owreq_search_push, owreq_search_pop, owreq_search_destroy};
    owreq_search* ows = new owreq_search(&owreq_search_funcs, result, id, scode);
    assert(ows);
    *this->plast = &ows->base;
    this->plast = &ows->base.next;
    return *this;
}
OWireSession& OWireSession::addSearch(OWireID& id, OWireResult* result){
    return this->addFSearch(id, SearchContinue, result);
}
struct owreq_delay{
    OWireReqBase base;
    uint8_t delay_code;
    owreq_delay(const OWireReqFuncs* funcs, OWireResult* result, int ms)
        : base(funcs, result){
        assert((ms > 0) && (ms <= 2560));
        this->delay_code = (ms + 9) / 10;
    }
};
static std::size_t owreq_delay_getsize(OWireReqBase* base){
    (void)base;
    return 2;
}
static std::size_t owreq_delay_getruntime(OWireReqBase* base){
    owreq_delay* owreq = REQUEST_BASE(owreq_delay, base);
    return (owreq->delay_code + 1) * 10000;
}

static void owreq_delay_push(OWireReqBase* base, uint8_t** buffer){
    owreq_delay* owreq = REQUEST_BASE(owreq_delay, base);
    uint8_t* out = *buffer;
    out[0] = EFOURMI_1WIRE_OP_CMD_DELAY;
    out[1] = owreq->delay_code;
    *buffer = out + 2;
}
static void owreq_delay_pop(OWireReqBase* base, uint8_t** buffer, int& blen){
    owreq_delay* owreq = REQUEST_BASE(owreq_delay, base);
    uint8_t* out = *buffer;
    do{
        if(out == NULL){
            break;
        }
        if(blen < 1){
            break;
        }
        if((out[0] & EFOURMI_1WIRE_OP_CMD_MASK) != EFOURMI_1WIRE_OP_CMD_DELAY){
            break;
        }
        owreq_setresult(&owreq->base, static_cast<OWireResult>((out[0] & EFOURMI_1WIRE_OP_RESULT_MASK) >> 4));
        *buffer = out + 1;
        blen -= 1;
        return;
    }while(0);
    owreq_setresult(&owreq->base, OWR_UNDEF);
    *buffer = NULL;
    return;
}
static void owreq_delay_destroy(OWireReqBase* base){
    owreq_delay* owreq = REQUEST_BASE(owreq_delay, base);
    delete owreq;
}
OWireSession& OWireSession::addDelay(int ms, OWireResult* result){
    static const OWireReqFuncs owreq_delay_funcs = {owreq_delay_getsize, owreq_delay_getruntime, owreq_delay_push, owreq_delay_pop, owreq_delay_destroy};
    owreq_delay* owd = new owreq_delay(&owreq_delay_funcs, result, ms);
    assert(owd);
    *this->plast = &owd->base;
    this->plast = &owd->base.next;
    return *this;
}
struct owreq_rexch{
    OWireReqBase base;
    const uint8_t* dataout;
    uint8_t* datain;
    std::size_t bitcount;
    owreq_rexch(const OWireReqFuncs* funcs, OWireResult* result, const uint8_t* dataout, uint8_t* datain, std::size_t bitcount)
        : base(funcs, result), dataout(dataout), datain(datain), bitcount(bitcount){
        assert(!((bitcount % 8) && (bitcount > 127)));
        assert(!(bitcount > (127 * 8)));
    }
};
static std::size_t owreq_rexch_getsize(OWireReqBase* base){
    owreq_rexch* owreq = REQUEST_BASE(owreq_rexch, base);
    return 1 + 1 + ((owreq->bitcount + 7) / 8);
}
static std::size_t owreq_rexch_getruntime(OWireReqBase* base){
    owreq_rexch* owreq = REQUEST_BASE(owreq_rexch, base);
    return ((1 + 1) * EFOURMI_1WIRE_TIMING_RESET) + ((1 + owreq->bitcount) * EFOURMI_1WIRE_TIMING_BIT);
}
static void owreq_rexch_push(OWireReqBase* base, uint8_t** buffer){
    owreq_rexch* owreq = REQUEST_BASE(owreq_rexch, base);
    uint8_t* out = *buffer;
    std::size_t bytes = (owreq->bitcount + 7) / 8;
    out[0] = EFOURMI_1WIRE_OP_CMD_REXCH;
    out[1] = owreq->bitcount % 8? owreq->bitcount: EFOURMI_1WIRE_SZ_INBYTES | bytes;
    std::memcpy(out + 2, owreq->dataout, bytes);
    *buffer = out + 2 + bytes;
}
static void owreq_rexch_pop(OWireReqBase* base, uint8_t** buffer, int& blen){
    owreq_rexch* owreq = REQUEST_BASE(owreq_rexch, base);
    uint8_t* out = *buffer;
    do{
        if(out == NULL){
            break;
        }
        if(blen < 1){
            break;
        }
        if((out[0] & EFOURMI_1WIRE_OP_CMD_MASK) != EFOURMI_1WIRE_OP_CMD_REXCH){
            break;
        }
        owreq_setresult(&owreq->base, static_cast<OWireResult>((out[0] & EFOURMI_1WIRE_OP_RESULT_MASK) >> 4));
        switch(out[0] & EFOURMI_1WIRE_OP_RESULT_MASK){
            case EFOURMI_1WIRE_OP_RESULT_SUCCESS:
            case EFOURMI_1WIRE_OP_RESULT_SUCCESS_LAST:{
                if(blen < 2){
                    break;
                }
                std::size_t csize = out[1];
                std::size_t bits = csize & EFOURMI_1WIRE_SZ_INBYTES? (csize & EFOURMI_1WIRE_SZ_VALUE) * 8: csize & EFOURMI_1WIRE_SZ_VALUE;
                std::size_t bytes = (bits + 7) / 8;
                if((blen < (2 + bytes)) || (bits != owreq->bitcount)){
                    break;
                }
                if(owreq->datain){
                    std::memcpy(out + 2, owreq->datain, bytes);
                }
                *buffer = out + 2 + bytes;
                blen -= 2 + bytes;
                return;
            }
            default:
                *buffer = out + 1;
                blen -= 1;
                return;
        }
    }while(0);
    owreq_setresult(&owreq->base, OWR_UNDEF);
    *buffer = NULL;
    return;
}
static void owreq_rexch_destroy(OWireReqBase* base){
    owreq_rexch* owreq = REQUEST_BASE(owreq_rexch, base);
    delete owreq;
}
OWireSession& OWireSession::addRExch(const uint8_t* dataout, uint8_t* datain, std::size_t bitcount, OWireResult* result){
    static const OWireReqFuncs owreq_rexch_funcs = {owreq_rexch_getsize, owreq_rexch_getruntime, owreq_rexch_push, owreq_rexch_pop, owreq_rexch_destroy};
    owreq_rexch* owso = new owreq_rexch(&owreq_rexch_funcs, result, dataout, datain, bitcount);
    assert(owso);
    *this->plast = &owso->base;
    this->plast = &owso->base.next;
    return *this;
}
OWireSession& OWireSession::addAllConvertT(OWireResult* result){
    static const uint8_t convert[] = {0xCC, 0x44};
    return this->addRExch(convert, NULL, 16, result);
}
struct owreq_ds18b20tempreader{
    OWireReqBase base;
    const OWireID& id;
    void* temp;
    owreq_ds18b20tempreader(const OWireReqFuncs* funcs, OWireResult* result, const OWireID& id, void* temp)
        : base(funcs, result), id(id), temp(temp){
    }
};
static std::size_t owreq_ds18b20tempreader_getsize(OWireReqBase* base){
    (void)base;
    return 1 + 1 + 19;
}
static std::size_t owreq_ds18b20tempreader_getruntime(OWireReqBase* base){
    (void)base;
    return ((1 + 1) * EFOURMI_1WIRE_TIMING_RESET) + ((1 + 19 * 8) * EFOURMI_1WIRE_TIMING_BIT);
}

static void owreq_ds18b20tempreader_push(OWireReqBase* base, uint8_t** buffer){
    static const uint8_t prolog[] = {EFOURMI_1WIRE_OP_CMD_REXCH, EFOURMI_1WIRE_SZ_INBYTES | 19, 0x55};
    static const uint8_t epilog[] = {0xBE, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
    owreq_ds18b20tempreader* owreq = REQUEST_BASE(owreq_ds18b20tempreader, base);
    uint8_t* out = *buffer;
    std::memcpy(out, prolog, sizeof(prolog));
    std::memcpy(out + sizeof(prolog), owreq->id.getRaw(), 8);
    std::memcpy(out + sizeof(prolog) + 8, epilog, sizeof(epilog));
    *buffer = out + sizeof(prolog) + 8 + sizeof(epilog);
}
static int owreq_ds18b20tempreader_pop(owreq_ds18b20tempreader* owreq, uint8_t** buffer, int& blen){    
    uint8_t* out = *buffer;
    do{
        if(out == NULL){
            break;
        }
        if(blen < 1){
            break;
        }
        if((out[0] & EFOURMI_1WIRE_OP_CMD_MASK) != EFOURMI_1WIRE_OP_CMD_REXCH){
            break;
        }
        owreq_setresult(&owreq->base, static_cast<OWireResult>((out[0] & EFOURMI_1WIRE_OP_RESULT_MASK) >> 4));
        switch(out[0] & EFOURMI_1WIRE_OP_RESULT_MASK){
            case EFOURMI_1WIRE_OP_RESULT_SUCCESS:
            case EFOURMI_1WIRE_OP_RESULT_SUCCESS_LAST:{
                if(blen < 2){
                    break;
                }
                std::size_t csize = out[1];
                std::size_t bits = csize & EFOURMI_1WIRE_SZ_INBYTES? (csize & EFOURMI_1WIRE_SZ_VALUE) * 8: csize & EFOURMI_1WIRE_SZ_VALUE;
                std::size_t bytes = (bits + 7) / 8;
                if((blen < (2 + bytes)) || (bits != (19 * 8))){
                    break;
                }
                *buffer = out + 2 + bytes;
                blen -= 2 + bytes;
                if(OWireSession::getCRC(out + 2 + 10, 9) == 0){
                    return (out[2 + 11] << 8) | out[2 + 10];
                }
                owreq_setresult(&owreq->base, OWR_CRCFAULT);
                return -1;
            }
            default:
                *buffer = out + 1;
                blen -= 1;
                return -1;
        }
    }while(0);
    owreq_setresult(&owreq->base, OWR_UNDEF);
    *buffer = NULL;
    return -1;
}
static void owreq_ds18b20tempreader_pop_i(OWireReqBase* base, uint8_t** buffer, int& blen){
    owreq_ds18b20tempreader* owreq = REQUEST_BASE(owreq_ds18b20tempreader, base);
    int temp = owreq_ds18b20tempreader_pop(owreq, buffer, blen);
    if(temp >= 0){
        *((int*)owreq->temp) = (temp << 16) >> 16;
    }
}
static void owreq_ds18b20tempreader_pop_f(OWireReqBase* base, uint8_t** buffer, int& blen){
    owreq_ds18b20tempreader* owreq = REQUEST_BASE(owreq_ds18b20tempreader, base);
    int temp = owreq_ds18b20tempreader_pop(owreq, buffer, blen);
    if(temp >= 0){
        *((float*)owreq->temp) = ((temp << 16) >> 16) / 16.0;
    }
}
static void owreq_ds18b20tempreader_destroy(OWireReqBase* base){
    owreq_ds18b20tempreader* owreq = REQUEST_BASE(owreq_ds18b20tempreader, base);
    delete owreq;
}

OWireSession& OWireSession::addDS18B20TempReader(const OWireID& id, int& temp, OWireResult* result){
    static const OWireReqFuncs owreq_ds18b20tempreader_i_funcs = {owreq_ds18b20tempreader_getsize, owreq_ds18b20tempreader_getruntime, owreq_ds18b20tempreader_push, owreq_ds18b20tempreader_pop_i, owreq_ds18b20tempreader_destroy};
    owreq_ds18b20tempreader* owreq = new owreq_ds18b20tempreader(&owreq_ds18b20tempreader_i_funcs, result, id, &temp);
    assert(owreq);
    *this->plast = &owreq->base;
    this->plast = &owreq->base.next;
    return *this;
}
OWireSession& OWireSession::addDS18B20TempReader(const OWireID& id, float& temp, OWireResult* result){
    static const OWireReqFuncs owreq_ds18b20tempreader_f_funcs = {owreq_ds18b20tempreader_getsize, owreq_ds18b20tempreader_getruntime, owreq_ds18b20tempreader_push, owreq_ds18b20tempreader_pop_f, owreq_ds18b20tempreader_destroy};
    owreq_ds18b20tempreader* owreq = new owreq_ds18b20tempreader(&owreq_ds18b20tempreader_f_funcs, result, id, &temp);
    assert(owreq);
    *this->plast = &owreq->base;
    this->plast = &owreq->base.next;
    return *this;
}
int OWireSession::getCRC(const uint8_t* data, std::size_t datalen){
    uint8_t crc8 = 0;
    static const uint8_t crc_table[] = {
        0, 94,188,226, 97, 63,221,131,194,156,126, 32,163,253, 31, 65,
        157,195, 33,127,252,162, 64, 30, 95,  1,227,189, 62, 96,130,220,
        35,125,159,193, 66, 28,254,160,225,191, 93,  3,128,222, 60, 98,
        190,224,  2, 92,223,129, 99, 61,124, 34,192,158, 29, 67,161,255,
        70, 24,250,164, 39,121,155,197,132,218, 56,102,229,187, 89,  7,
        219,133,103, 57,186,228,  6, 88, 25, 71,165,251,120, 38,196,154,
        101, 59,217,135,  4, 90,184,230,167,249, 27, 69,198,152,122, 36,
        248,166, 68, 26,153,199, 37,123, 58,100,134,216, 91,  5,231,185,
        140,210, 48,110,237,179, 81, 15, 78, 16,242,172, 47,113,147,205,
        17, 79,173,243,112, 46,204,146,211,141,111, 49,178,236, 14, 80,
        175,241, 19, 77,206,144,114, 44,109, 51,209,143, 12, 82,176,238,
        50,108,142,208, 83, 13,239,177,240,174, 76, 18,145,207, 45,115,
        202,148,118, 40,171,245, 23, 73,  8, 86,180,234,105, 55,213,139,
        87,  9,235,181, 54,104,138,212,149,203, 41,119,244,170, 72, 22,
        233,183, 85, 11,136,214, 52,106, 43,117,151,201, 74, 20,246,168,
        116, 42,200,150, 21, 75,169,247,182,232, 10, 84,215,137,107, 53
    };
    while(datalen--){
        crc8 = crc_table[crc8 ^ *data++];
    }
    return crc8;
}
