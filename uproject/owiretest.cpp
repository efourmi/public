#include "efourmi.hpp"

using namespace efourmi;

volatile uint32_t ticks1ms;

static void sleep(int ms){
    uint32_t to = ticks1ms + ms;
    while(((int32_t)to - (int32_t)ticks1ms) > 0){
        __WFI();
    }
}

IRQ_HANDLER(SysTick_Handler){
    ticks1ms++;
}

static ModbusBuffer mbuffer;

static ModbusNode rnode(100, mbuffer, sleep);

static OWireID ds18b20[16];
static int ds18b20_count;
static OWireResult res[16];
static int value[16];
float fmin, fmax;

static void scan(void){
    OWireSession searchsession(rnode);
    searchsession.addFSearch(ds18b20[0], SearchNormal, res);
    for(int i = 1; i < 16; i++){
        searchsession.addSearch(ds18b20[i], res + i);
    }
    searchsession.Do();
    ds18b20_count = 0;
    for(int i = 0; i < 16; i++){
        if(((res[i] == OWR_SUCCESS) || (res[i] == OWR_SUCCESSL)) && (ds18b20[i] == true)){
            if(i > ds18b20_count){
                ds18b20[ds18b20_count] = ds18b20[i];
            }
            ds18b20_count++;
        }
    }
}

int main(void){
    cmd_regirq(SysTick_IRQn, SysTick_Handler);
    SysTick_Config(cmd_envget(EFOURMI_ENV_SYSFREQ) / 1000);
    scan();
    OWireSession readsession(rnode);
    readsession
        .addDelay(2000)
        .addAllConvertT()
        .addDelay(860);
    for(int i = 0; i < ds18b20_count; i++){
        readsession.addDS18B20TempReader(ds18b20[i], value[i], res + i);
    }
    while(1){
        int readed = 0;
        int min = 1000;
        int max = -1000;
        readsession.Do();
        for(int i = 0; i < ds18b20_count; i++){
            if(res[i] == OWR_SUCCESS){
                readed++;
                if(value[i] < min){
                    min = value[i];
                }
                if(value[i] > max){
                    max = value[i];
                }
            }
        }
        ROData[0] = readed;
        ROData[1] = (int16_t)min;
        ROData[2] = (int16_t)max;
        fmin = min / 16.0;
        fmax = max / 16.0;
    }
}
