#include "efourmi.hpp"

using namespace efourmi;

volatile uint32_t ticks1ms;

static void sleep(int ms){
    uint32_t to = ticks1ms + ms;
    while(((int32_t)to - (int32_t)ticks1ms) > 0){
        __WFI();
    }
}

IRQ_HANDLER(SysTick_Handler){
    ticks1ms++;
}

static ModbusBuffer mbuffer;

static ModbusNode node100(100, mbuffer, sleep); // io configuration IIIIIIII

static ModbusNode self(0, mbuffer, sleep); // io configuration OOOOOOOO

static Reg16 node100in0;
static Reg16 node100ro0;

static Reg16 node100rw0_2[3];
static Reg16& node100rw0 = node100rw0_2[0];
static Reg16& node100rw1 = node100rw0_2[1];
static Reg16& node100rw2 = node100rw0_2[2];

static Reg16 selfout0;

int main(void){
    cmd_regirq(SysTick_IRQn, SysTick_Handler);
    SysTick_Config(cmd_envget(EFOURMI_ENV_SYSFREQ) / 1000);
    int counter = 0;
    while(1){
        if(!node100.getRO(EFOURMI_ROREG_DI + 0, 1, &node100in0)){
            node100in0 = 0;
        }
        if(!node100.getRO(EFOURMI_ROREG_USER + 0, 1, &node100ro0)){
            node100ro0 = 0;
        }
        if(!node100.getRW(EFOURMI_RWREG_USER + 0, 3, node100rw0_2)){
             node100rw0 = 0;
             node100rw1 = 0;
             node100rw2 = 0;
        }
        node100rw0 = counter;
        node100rw1 = counter << 13;
        node100rw2 = node100rw0;
        selfout0 = node100in0;
        cmd_regset(EFOURMI_RWREG_DO + 0, selfout0);
        node100.setRW(EFOURMI_RWREG_USER + 0, 3, node100rw0_2);
        sleep(50);
        counter++;
    }
}
