#ifndef MODBUSD_H
#define MODBUSD_H

#include <stdint.h>

#ifdef __cplusplus
extern "C"{
#endif

#define MODBUSD_VERSION                          (1)
#define MODBUSD_FRONTEND_PREFIX                  "mbfe_"
#define MODBUSD_BACKEND_PREFIX                   "mbbe_"
#define MODBUSD_HELP_POSTFIX                     "_help"

#define MODBUSD_LOGLEVEL_EMERG                   (0)
#define MODBUSD_LOGLEVEL_ALERT                   (1)
#define MODBUSD_LOGLEVEL_CRIT                    (2)
#define MODBUSD_LOGLEVEL_ERR                     (3)
#define MODBUSD_LOGLEVEL_WARNING                 (4)
#define MODBUSD_LOGLEVEL_NOTICE                  (5)
#define MODBUSD_LOGLEVEL_INFO                    (6)
#define MODBUSD_LOGLEVEL_DEBUG                   (7)

#define MODBUSD_EINVAL                           (-1)
#define MODBUSD_ENOMEM                           (-2)
#define MODBUSD_EISCONN                          (-3)
#define MODBUSD_ENOTCONN                         (-4)
#define MODBUSD_EINTR                            (-5)
#define MODBUSD_ENODEV                           (-6)
#define MODBUSD_EERROR                           (-100)

#define MODBUSD_EXC_ILLFUNCTION                  (0x1)
#define MODBUSD_EXC_ILLDATAADDR                  (0x2)
#define MODBUSD_EXC_ILLDATAVALUE                 (0x3)
#define MODBUSD_EXC_SERVERFAILURE                (0x4)
#define MODBUSD_EXC_ACKNOLEDGE                   (0x5)
#define MODBUSD_EXC_SERVERBUSY                   (0x6)
#define MODBUSD_EXC_GATEWAYPATH                  (0xA)
#define MODBUSD_EXC_GATEWAYNORESP                (0xB)

#define MODBUSD_ACTION_ONOPEN                    (0)
/*
 * int frontend_action(MODBUSD_ACTION_ONOPEN,...){
 *   const struct modbusd_frontend* = vararg[0];
 *   return 0;
 * }
 *
 * int frontend_action(MODBUSD_ACTION_ONOPEN,...){
 *   const struct modbusd_backend* = vararg[0];
 *   return 0;
 * }
 *
 */
#define MODBUSD_ACTION_ONCLOSE                   (1)
/*
 * int frontend_action(MODBUSD_ACTION_ONCLOSE,...){
 *   const struct modbusd_frontend* = vararg[0];
 *   return 0;
 * }
 *
 * int frontend_action(MODBUSD_ACTION_ONCLOSE,...){
 *   const struct modbusd_backend* = vararg[0];
 *   return 0;
 * }
 *
 */
// return (acception permit) arg = const struct modbusd_action_clientdef*
#define MODBUSD_ACTION_ONACCEPT                  (2)
/*
 * int frontend_action(MODBUSD_ACTION_ONACCEPT, ...){
 *   const struct modbusd_frontend* = vararg[0];
 *   uint32_t ip4 = vararg[1];
 *   return is_connection_allowed? 1: 0;
 * }
 *
 */
#define MODBUSD_ACTION_ONDISCONNECT              (3)
/*
 * int frontend_action(MODBUSD_ACTION_ONDISCONNECT, ...){
 *   const struct modbusd_frontend* = vararg[0];
 *   uint32_t ip4 = vararg[1];
 *   return 0;
 * }
 *
 */
// return (answer len) arg = struct modbusd_action_request*
#define MODBUSD_ACTION_ONREQUEST                 (4)
/*
 * int frontend_action(MODBUSD_ACTION_ONACCEPT, ...){
 *   const struct modbusd_frontend* = vararg[0];
 *   uint32_t ip4 = vararg[1];
 *   uint8_t* data = vararg[2];
 *   int datalen = vararg[3];
 *   return (int)answer_length;
 * }
 *
 */

typedef int (*modbusd_action)(int action, ...);

typedef void (*modbusd_logout)(int ll, const char* format, ...);

typedef const char* (*modbusd_help)(void);

struct modbusd_frontend{
    int  (*open)(const struct modbusd_frontend* mbfe);
    int  (*close)(const struct modbusd_frontend* mbfe);
    int  (*status)(const struct modbusd_frontend* mbfe);
    int  (*serve)(const struct modbusd_frontend* mbfe, int ms);
    void (*free)(const struct modbusd_frontend* mbfe);
    const char* arg;
    void* tag;
    const char* driver;
    const char* id;
};

struct modbusd_frontend_def{
    int modbusd_version;
    modbusd_action action;
    modbusd_logout logout;
    const char* arg;
    void* tag;
};

#define mbfe_open(MBFE)                          (MBFE)->open(MBFE)
#define mbfe_close(MBFE)                         (MBFE)->close(MBFE)
#define mbfe_status(MBFE)                        (MBFE)->status(MBFE)
#define mbfe_serve(MBFE, TIMEOUT)                (MBFE)->serve((MBFE), (TIMEOUT))
#define mbfe_free(MBFE)                          (MBFE)->free(MBFE)
#define mbfe_arg(MBFE)                           (MBFE)->arg
#define mbfe_tag(MBFE)                           (MBFE)->tag
#define mbfe_driver(MBFE)                        (MBFE)->driver
#define mbfe_id(MBFE)                            (MBFE)->id

typedef const struct modbusd_frontend* (*modbusd_frontend_new)(struct modbusd_frontend_def* def);

struct modbusd_backend{
    int  (*open)(const struct modbusd_backend* mbbe);
    int  (*close)(const struct modbusd_backend* mbbe);
    int  (*status)(const struct modbusd_backend* mbbe);
    int  (*dorequest)(const struct modbusd_backend* mbbe, uint8_t* data, int datalen);
    void (*free)(const struct modbusd_backend* mbbe);
    const char* arg;
    void* tag;
    const char* driver;
    const  char* id;
};

struct modbusd_backend_def{
    int modbusd_version;
    modbusd_action action;
    modbusd_logout logout;
    const char* arg;
    void* tag;
};

typedef const struct modbusd_backend* (*modbusd_backend_new)(struct modbusd_backend_def* def);

#define mbbe_open(MBBE)                          (MBBE)->open(MBBE)
#define mbbe_close(MBBE)                         (MBBE)->close(MBBE)
#define mbbe_status(MBBE)                        (MBBE)->status(MBBE)
#define mbbe_dorequest(MBBE, DATA, DATALEN)      (MBBE)->serve((MBBE), (DATA), (DATALEN))
#define mbbe_free(MBBE)                          (MBBE)->free(MBBE)
#define mbbe_arg(MBBE)                           (MBBE)->arg
#define mbbe_tag(MBBE)                           (MBBE)->tag
#define mbbe_driver(MBBE)                        (MBBE)->driver
#define mbbe_id(MBBE)                            (MBBE)->id

typedef struct {const char* name; const modbusd_help helpfunc; const modbusd_frontend_new newfunc;} fedriver_def;
typedef struct {const char* name; const modbusd_help helpfunc; const modbusd_backend_new newfunc;} bedriver_def;

#ifdef __cplusplus
}
#endif

#endif // MODBUSD_H
