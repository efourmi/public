#include "modbusd.h"
#include <stdlib.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/queue.h>
#include <net/if.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <netdb.h>

#define NEG_ERRNO
#define MB_ERRNO

struct modbusd_frontend_tcp_client{
    LIST_ENTRY(modbusd_frontend_tcp_client) entry;
    int sock;
    uint32_t ip4;
    enum{
        rcv_head,
        rcv_msg,
        send_head,
        send_msg
    } state;
    uint8_t header[6];
    uint8_t request[254];
    uint8_t answer[254];
    int left;
    uint8_t* dpointer;
};

struct modbusd_frontend_tcp{
    struct modbusd_frontend mbfe;
    modbusd_action action;
    modbusd_logout logout;
    LIST_HEAD(, modbusd_frontend_tcp_client) clientlist;
    int sock;
    struct sockaddr_in saddr;
    fd_set readfd;
    fd_set writefd;
    char id[15+1+5+1];/* [IPv4:PORT] */
    char arg[];
};
#define MBFETOMBFETCP(MBFE) ((struct modbusd_frontend_tcp*)((size_t)(MBFE) - offsetof(struct modbusd_frontend_tcp, mbfe)))

MB_ERRNO  static int negerrno_to_mberrno(int e){
    switch(e){
    case -EINVAL: return MODBUSD_EINVAL;
    case -ENOMEM: return MODBUSD_ENOMEM;
    case -EISCONN: return MODBUSD_EISCONN;
    case -ENOTCONN: return MODBUSD_ENOTCONN;
    case -EINTR: return MODBUSD_EINTR;
    default: return MODBUSD_EERROR;
    }
}

NEG_ERRNO static int socket_unblock(int sock){
    int opt = 1;
    if(ioctl(sock, FIONBIO, &opt) != 0){
        return -errno;
    }
    return 0;
}

NEG_ERRNO static int socket_modbusconf(int sock) {
    int opt;
    opt = 900;
    if(setsockopt(sock, SOL_SOCKET, SO_RCVBUF, &opt, sizeof(opt)) != 0){
        return -errno;
    }
    opt = 900;
    if(setsockopt(sock, SOL_SOCKET, SO_SNDBUF, &opt, sizeof(opt)) != 0) {
        return -errno;
    }
    opt = 1;
    if(setsockopt(sock, IPPROTO_TCP, TCP_NODELAY, &opt, sizeof(opt)) != 0) {
        return -errno;
    }
    return 0;
}

NEG_ERRNO static int socket_recvnblock(int sock, void* data, size_t datalen, int flags) {
    int r, _errno;
    size_t dataleft = datalen;
    if(datalen == 0){
        return 0;
    }
    while(dataleft){
        r = recv(sock, data, dataleft, flags);
        _errno = errno;
        if(r == -1){
            if(_errno == EAGAIN){
                return datalen - dataleft;
            }else{
                return -_errno;
            }
        }else if(r == 0){
            return -ECONNABORTED;
        }
        dataleft -= r;
        data += r;
    }
    return datalen;
}

NEG_ERRNO static int socket_sendnblock(int s, const void *data, size_t datalen, int flags) {
    int r, _errno;
    size_t dataleft = datalen;
    if(datalen == 0){
        return 0;
    }
    while(dataleft){
        r = send(s, data, dataleft, flags);
        _errno = errno;
        if(r == -1){
            if(_errno == EAGAIN){
                return datalen - dataleft;
            }else{
                return -_errno;
            }
        }else if(r == 0){
            return -ECONNABORTED;
        }
        dataleft -= r;
        data += r;
    }
    return datalen;
}

static void timespec_addms(struct timespec* ts, int ms){
    long sec = ms / 1000;
    long nsec = (ms % 1000) * 1000000;
    ts->tv_nsec += nsec;
    ts->tv_sec += sec;
    if(ts->tv_nsec > 1000000000){
        ts->tv_sec += ts->tv_nsec / 1000000000;
        ts->tv_nsec = ts->tv_nsec % 1000000000;
    }
}

static int timespec_cmp(struct timespec* ts1, struct timespec* ts2){
    long dsec = ts1->tv_sec - ts2->tv_sec;
    long dnsec = ts1->tv_nsec - ts2->tv_nsec;
    if(!dsec && !dnsec){
        return 0;
    }else if((dsec > 0) || (!dsec && (dnsec > 0))){
        return 1;
    }
    return -1;
}

static void timespec_sub(struct timespec* ts1, struct timespec* ts2, struct timeval* tv){
    tv->tv_sec = ts1->tv_sec - ts2->tv_sec;
    tv->tv_usec = (ts1->tv_nsec - ts2->tv_nsec) / 1000;
    if(tv->tv_usec < 0){
        tv->tv_usec = 1000000 + tv->tv_usec;
        tv->tv_sec -= 1;
    }
    if((tv->tv_sec < 0) || ((tv->tv_sec == 0) && (tv->tv_usec < 0))){
        tv->tv_sec = 0;
        tv->tv_usec = 0;
    }
}

static void client_destroy(struct modbusd_frontend_tcp* mbfetcp, struct modbusd_frontend_tcp_client* client){
    if(client->sock >= 0){
        close(client->sock);
        client->sock = -1;
    }
    mbfetcp->action(MODBUSD_ACTION_ONDISCONNECT, &mbfetcp->mbfe, client->ip4);
    LIST_REMOVE(client, entry);
    free(client);
}

NEG_ERRNO static int mbfetcp_accept(struct modbusd_frontend_tcp* mbfetcp){
    struct sockaddr_in saddr;
    socklen_t saddrlen = sizeof(struct sockaddr_in);
    int s = accept(mbfetcp->sock, (struct sockaddr*)&saddr, &saddrlen);
    int ret;
    if(s >= 0){
        if(!mbfetcp->action(MODBUSD_ACTION_ONACCEPT, &mbfetcp->mbfe, (uint32_t)ntohl(saddr.sin_addr.s_addr))){
            close(s);
            return 0;
        }
        ret = socket_unblock(s);
        if(ret < 0){
            close(s);
            return ret;
        }
        ret = socket_modbusconf(s);
        if(ret < 0){
            close(s);
            return ret;
        }
        struct modbusd_frontend_tcp_client* client = (struct modbusd_frontend_tcp_client*)malloc(sizeof(struct modbusd_frontend_tcp_client));
        if(client){
            client->sock = s;
            client->ip4 = htonl(saddr.sin_addr.s_addr);
            client->state = rcv_head;
            client->left = 6;
            client->dpointer = client->header;
            LIST_INSERT_HEAD(&mbfetcp->clientlist, client, entry);
            return 0;
        }else{
            close(s);
            return -ENOMEM;
        }
    }else{
        return -errno;
    }
}

NEG_ERRNO static int mbfetcp_argtosaddr(struct modbusd_frontend_tcp* mbfetcp){
    char* addressbuf_s;
    char* address_s;
    char* addrtype_s;
    char* port_s;
    int ret;
    addressbuf_s = strdup(mbfetcp->arg);
    if(!addressbuf_s){
        return -ENOMEM;
    }
    address_s = addressbuf_s;
    addrtype_s = address_s;
    while(*address_s) {
        if(*address_s == ':') {
            *address_s++ = 0;
            break;
        }
        address_s++;
    }
    if(*address_s == 0) {
        ret = -EINVAL;
        goto __onerror;
    }
    port_s = address_s;
    while(*port_s) {
        if(*port_s == ':') {
            *port_s++ = 0;
            break;
        }
        port_s++;
    }
    mbfetcp->saddr.sin_family = AF_INET;
    if(*port_s == 0) {
        ret = -EINVAL;
        goto __onerror;
    } else {
        char* end;
        unsigned long port = strtoul(port_s, &end, 10);
        if((end == port_s) || (port > 65535) || (port == 0) || (*end != 0)) {
            return -1;
        }
        mbfetcp->saddr.sin_port = htons(port);
    }
    if(!strcmp(addrtype_s, "ip")){
        struct hostent* hent = gethostbyname(address_s);
        if(!hent){
            ret = -EINVAL;
            goto __onerror;
        }
        mbfetcp->saddr.sin_addr = *(struct in_addr *)hent->h_addr_list[0];
    }else if(!strcmp(addrtype_s, "if")){
        struct ifreq ifr;
        int s = socket(PF_INET, SOCK_STREAM, 0);
        int r;
        if(s < 0) {
            ret = -errno;
            goto __onerror;
        }
        strncpy(ifr.ifr_name, address_s , IFNAMSIZ - 1);
        r = ioctl(s, SIOCGIFADDR, &ifr);
        close(s);
        if(r >= 0) {
            mbfetcp->saddr.sin_addr.s_addr = ((struct sockaddr_in*)&ifr.ifr_addr)->sin_addr.s_addr;
        } else {
            ret = -errno;
            goto __onerror;
        }
    }else{
        ret = -EINVAL;
        goto __onerror;
    }
    free(addressbuf_s);
    return 0;
__onerror:
    free(addressbuf_s);
    return ret;
}

MB_ERRNO  static int mbfetcp_open(const struct modbusd_frontend* mbfe){
    int ret;
    struct modbusd_frontend_tcp* mbfetcp = MBFETOMBFETCP(mbfe);
    if(mbfetcp->sock >= 0){
        return MODBUSD_EISCONN;
    }
    ret = mbfetcp_argtosaddr(mbfetcp);
    if(ret < 0){
        mbfetcp->logout(MODBUSD_LOGLEVEL_DEBUG, "mbfe_tcp.c/mbfetcp_argtosaddr(): return %i", ret);
        return negerrno_to_mberrno(ret);
    }
    mbfetcp->sock = socket(PF_INET, SOCK_STREAM, 0);
    if(mbfetcp->sock < 0){
        ret = -errno;
        mbfetcp->logout(MODBUSD_LOGLEVEL_DEBUG, "mbfe_tcp.c/mbfetcp_open()/socket(): return %i", ret);
        return negerrno_to_mberrno(ret);
    }
    ret = bind(mbfetcp->sock, (struct sockaddr*)&mbfetcp->saddr, sizeof(struct sockaddr));
    if(ret < 0){
        ret = -errno;
        mbfetcp->logout(MODBUSD_LOGLEVEL_DEBUG, "mbfe_tcp.c/mbfetcp_open()/bind(): return %i", ret);
        goto __onerror;
    }
    ret = socket_unblock(mbfetcp->sock);
    if(ret < 0){
        mbfetcp->logout(MODBUSD_LOGLEVEL_DEBUG, "mbfe_tcp.c/mbfetcp_open()/socket_unblock(): return %i", ret);
        goto __onerror;
    }
    int opt = 1;
    ret = setsockopt(mbfetcp->sock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
    if(ret < 0){
        ret = -errno;
        mbfetcp->logout(MODBUSD_LOGLEVEL_DEBUG, "mbfe_tcp.c/mbfetcp_open()/setsockopt(): return %i", ret);
        goto __onerror;
    }
    ret = listen(mbfetcp->sock, 4);
    if(ret < 0){
        ret = -errno;
        mbfetcp->logout(MODBUSD_LOGLEVEL_DEBUG, "mbfe_tcp.c/mbfetcp_open()/listen(): return %i", ret);
        goto __onerror;
    }
    sprintf(mbfetcp->id, "%s:%i", inet_ntoa(mbfetcp->saddr.sin_addr), ntohs(mbfetcp->saddr.sin_port));
    mbfetcp->mbfe.id = mbfetcp->id;
    mbfetcp->action(MODBUSD_ACTION_ONOPEN, &mbfetcp->mbfe);
    return 0;
__onerror:
    close(mbfetcp->sock);
    mbfetcp->sock = -1;
    return negerrno_to_mberrno(ret);
}

MB_ERRNO  static int mbfetcp_close(const struct modbusd_frontend* mbfe){
    struct modbusd_frontend_tcp* mbfetcp = MBFETOMBFETCP(mbfe);
    struct modbusd_frontend_tcp_client* client;
    if(mbfetcp->sock < 0){
        return MODBUSD_ENOTCONN;
    }
    while(client = LIST_FIRST(&mbfetcp->clientlist), client){
        client_destroy(mbfetcp, client);
    }
    close(mbfetcp->sock);
    mbfetcp->sock = -1;
    mbfetcp->action(MODBUSD_ACTION_ONCLOSE, &mbfetcp->mbfe);
    mbfetcp->mbfe.id = NULL;
    return 0;
}

MB_ERRNO  static int mbfetcp_status(const struct modbusd_frontend* mbfe){
    struct modbusd_frontend_tcp* mbfetcp = MBFETOMBFETCP(mbfe);
    if(mbfetcp->sock < 0){
        return 0;
    }
    return 1;
}

MB_ERRNO  static int mbfetcp_serve(const struct modbusd_frontend* mbfe, int ms){
    struct modbusd_frontend_tcp* mbfetcp = MBFETOMBFETCP(mbfe);
    struct timespec ts, cts;
    struct timeval tv;
    struct modbusd_frontend_tcp_client* client,* nclient;
    int ret;
    if(mbfetcp->sock < 0){
        return MODBUSD_ENOTCONN;
    }
    clock_gettime(CLOCK_MONOTONIC, &ts);
    timespec_addms(&ts, ms);
    do{
        FD_ZERO(&mbfetcp->readfd);
        FD_ZERO(&mbfetcp->writefd);
        int nfds = mbfetcp->sock;
        FD_SET(mbfetcp->sock, &mbfetcp->readfd);
        LIST_FOREACH(client, &mbfetcp->clientlist, entry){
            switch(client->state){
            case rcv_head:
            case rcv_msg:
                FD_SET(client->sock, &mbfetcp->readfd);
                break;
            case send_head:
            case send_msg:
                FD_SET(client->sock, &mbfetcp->writefd);
                break;
            }
            if(client->sock > nfds){
                nfds = client->sock;
            }
        }
        clock_gettime(CLOCK_MONOTONIC, &cts);
        timespec_sub(&ts, &cts, &tv);
        ret = select(nfds + 1, &mbfetcp->readfd, &mbfetcp->writefd, NULL, &tv);
        if(ret < 0){
            ret = -errno;
            if(ret != -EINTR){
                mbfetcp->logout(MODBUSD_LOGLEVEL_DEBUG, "mbfe_tcp.c/mbfetcp_serve()/select(): return %i", ret);
            }
            return negerrno_to_mberrno(ret);
        }
        nclient = LIST_FIRST(&mbfetcp->clientlist);
        while(nclient){
            client = nclient;
            nclient = LIST_NEXT(nclient, entry);
            switch(client->state){
            case rcv_head:
                if(FD_ISSET(client->sock, &mbfetcp->readfd)){
                    ret = socket_recvnblock(client->sock, client->dpointer, client->left, 0);
                    if(ret > 0){
                        client->left -= ret;
                        client->dpointer += ret;
                        if(!client->left){ // header compleate
                            if((client->header[2] == 0) && (client->header[3] == 0) && (client->header[4] == 0) && (client->header[5] <= 254)){
                                int msglen = client->header[5];
                                if(!msglen){
                                    client->state = send_head;
                                    client->dpointer = client->header;
                                    client->left = 6;
                                    FD_SET(client->sock, &mbfetcp->writefd);
                                    goto state_send_head;
                                }
                                client->state = rcv_msg;
                                client->dpointer = client->request;
                                client->left = msglen;
                                goto state_rcv_msg;
                            }else{// header corrupted
                                mbfetcp->logout(MODBUSD_LOGLEVEL_NOTICE, "mbfe_tcp.c/tcp_serve(): client \"%i.%i.%i.%i\" send corrupted header, disconnect client", (client->ip4 >> 24) & 0xFF, (client->ip4 >> 16) & 0xFF, (client->ip4 >> 8) & 0xFF, client->ip4& 0xFF);
                                client_destroy(mbfetcp, client);
                            }
                        }
                    }else if(ret == -EAGAIN){
                    }else if(ret == 0){
                    }else if(ret == -EINTR){
                        return MODBUSD_EINTR;
                    }else{
                        mbfetcp->logout(MODBUSD_LOGLEVEL_DEBUG, "mbfe_tcp.c/mbfetcp_open()/socket_recvnblock(): return %i", ret);
                        client_destroy(mbfetcp, client);
                    }
                }
                break;
            state_rcv_msg:
            case rcv_msg:
                if(FD_ISSET(client->sock, &mbfetcp->readfd)){
                    ret = socket_recvnblock(client->sock, client->dpointer, client->left, 0);
                    if(ret > 0){
                        client->left -= ret;
                        client->dpointer += ret;
                        if(!client->left){ // request compleate
                            memcpy(client->answer, client->request, client->header[5]);
                            int answer = mbfetcp->action(MODBUSD_ACTION_ONREQUEST, &mbfetcp->mbfe, client->ip4, client->answer, (int)client->header[5]);
                            if(answer == 0){
                                client->answer[0] = client->request[0];
                                client->answer[1] = client->request[1] | 0x80;
                                client->answer[2] = MODBUSD_EXC_GATEWAYNORESP;
                                client->header[5] = 3;
                            }else if(answer < 0){
                                client->answer[0] = client->request[0];
                                client->answer[1] = client->request[1] | 0x80;
                                client->answer[2] = MODBUSD_EXC_GATEWAYPATH;
                                client->header[5] = 3;
                            }else{
                                client->header[5] = answer;
                            }
                            client->state = send_head;
                            client->dpointer = client->header;
                            client->left = 6;
                            FD_SET(client->sock, &mbfetcp->writefd);
                            goto state_send_head;
                        }
                    }else if(ret == -EAGAIN){
                    }else if(ret == 0){
                    }else if(ret == -EINTR){
                        return MODBUSD_EINTR;
                    }else{
                        mbfetcp->logout(MODBUSD_LOGLEVEL_DEBUG, "mbfe_tcp.c/mbfetcp_open()/socket_recvnblock(): return %i", ret);
                        client_destroy(mbfetcp, client);
                    }
                }
                break;
            state_send_head:
            case send_head:
                if(FD_ISSET(client->sock, &mbfetcp->writefd)){
                    ret = socket_sendnblock(client->sock, client->dpointer, client->left, 0);
                    if(ret > 0){
                        client->left -= ret;
                        client->dpointer += ret;
                        if(!client->left){// send header compleate
                            if(client->header[5] == 0){
                                client->state = rcv_head;
                                client->dpointer = client->header;
                                client->left = 6;
                            }else{
                                client->state = send_msg;
                                client->dpointer = client->answer;
                                client->left = client->header[5];
                                goto state_send_msg;
                            }
                        }
                    }else if(ret == -EAGAIN){
                    }else if(ret == 0){
                    }else if(ret == -EINTR){
                        return MODBUSD_EINTR;
                    }else{
                        mbfetcp->logout(MODBUSD_LOGLEVEL_DEBUG, "mbfe_tcp.c/mbfetcp_open()/socket_recvnblock(): return %i", ret);
                        client_destroy(mbfetcp, client);
                    }
                }
                break;
            state_send_msg:
            case send_msg:
                if(FD_ISSET(client->sock, &mbfetcp->writefd)){
                    ret = socket_sendnblock(client->sock, client->dpointer, client->left, 0);
                    if(ret > 0){
                        client->left -= ret;
                        client->dpointer += ret;
                        if(!client->left){// send request compleate
                            client->state = rcv_head;
                            client->dpointer = client->header;
                            client->left = 6;
                        }
                    }else if(ret == -EAGAIN){
                    }else if(ret == 0){
                    }else if(ret == -EINTR){
                        return MODBUSD_EINTR;
                    }else{
                        mbfetcp->logout(MODBUSD_LOGLEVEL_DEBUG, "mbfe_tcp.c/mbfetcp_open()/socket_recvnblock(): return %i", ret);
                        client_destroy(mbfetcp, client);
                    }
                }
                break;
            }
        }
        if(FD_ISSET(mbfetcp->sock, &mbfetcp->readfd)){
            do{
                ret = mbfetcp_accept(mbfetcp);
                if(ret == -EINTR){
                    return MODBUSD_EINTR;
                }else if(ret == -EAGAIN){
                    break;
                }else if(ret == 0){
                }else if(ret == -EBADF){
                    mbfetcp->logout(MODBUSD_LOGLEVEL_DEBUG, "mbfe_tcp.c/mbfetcp_serve()/mbfetcp_accept(): return %i", -EBADF);
                    mbfetcp_close(mbfe);
                    return MODBUSD_EERROR;
                }else{
                    mbfetcp->logout(MODBUSD_LOGLEVEL_DEBUG, "mbfe_tcp.c/mbfetcp_serve()/mbfetcp_accept(): return %i", ret);
                }
            }while(1);
        }
        clock_gettime(CLOCK_MONOTONIC, &cts);
    }while(timespec_cmp(&ts, &cts) > 0);
    return 0;
}

static void mbfetcp_free(const struct modbusd_frontend* mbfe){
    struct modbusd_frontend_tcp* mbfetcp = MBFETOMBFETCP(mbfe);
    mbfetcp_close(mbfe);
    free(mbfetcp);
}

static int dummy_action(int action, ...){
    (void)action;
    return 0;
}

static void dummy_logout(int ll, const char* format, ...){
    (void)ll;
    (void)format;
}

const struct modbusd_frontend* mbfe_tcp(struct modbusd_frontend_def* def){
    if(def->modbusd_version != MODBUSD_VERSION){
        if(def->logout){
            def->logout(MODBUSD_LOGLEVEL_DEBUG, "modbusd version (%i) mismatch frontend driver version (%i)", def->modbusd_version, MODBUSD_VERSION);
        }
        return NULL;
    }
    struct modbusd_frontend_tcp* mbfetcp = (struct modbusd_frontend_tcp*)malloc(sizeof(struct modbusd_frontend_tcp) + strlen(def->arg) + 1);
    if(mbfetcp){
        mbfetcp->mbfe.open = mbfetcp_open;
        mbfetcp->mbfe.close = mbfetcp_close;
        mbfetcp->mbfe.status = mbfetcp_status;
        mbfetcp->mbfe.serve = mbfetcp_serve;
        mbfetcp->mbfe.free = mbfetcp_free;
        mbfetcp->mbfe.arg = mbfetcp->arg;
        mbfetcp->mbfe.tag = def->tag;
        mbfetcp->mbfe.driver = "tcp";
        mbfetcp->mbfe.id = NULL;

        mbfetcp->action = def->action? def->action: dummy_action;
        mbfetcp->logout = def->logout? def->logout: dummy_logout;

        strcpy(mbfetcp->arg, def->arg);
        LIST_INIT(&mbfetcp->clientlist);
        mbfetcp->sock = -1;
    }else if(def->logout){
        def->logout(MODBUSD_LOGLEVEL_DEBUG, "mbfe_tcp.c/mbfe_tcp(): not enought memory");
    }
    return &mbfetcp->mbfe;
}

const char* mbfe_tcp_help(void){
  return "usage: ... -ftcp \"ip|if:value:port\r\n" \
         "    ip|if            type of value, ip for hostname or IPv4 addr xxx.xxx.xxx.xxx, if for interface name\r\n" \
         "    value            value\r\n" \
         "    port             listen port\r\n" \
         "examples: ... -ftcp \"ip:127.0.0.1:502\" ...\r\n" \
         "          ... -ftcp \"if:eth0:502\" ...\r\n";
}
