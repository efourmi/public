#include "modbusd.h"
#include <stddef.h>

extern const struct modbusd_frontend* mbfe_tcp(struct modbusd_frontend_def* def);
extern const char* mbfe_tcp_help(void);

const fedriver_def fedrivers[]={
    {"tcp", mbfe_tcp_help, mbfe_tcp},
    {NULL, NULL, NULL}
};

extern const struct modbusd_backend* mbbe_serial(struct modbusd_backend_def* def);
extern const char* mbbe_serial_help(void);

extern const struct modbusd_backend* mbbe_efourmi(struct modbusd_backend_def* def);
extern const char* mbbe_efourmi_help(void);

const bedriver_def bedrivers[] = {
    {"serial", mbbe_serial_help, mbbe_serial},
    {"efourmi", mbbe_efourmi_help, mbbe_efourmi},
    {NULL, NULL, NULL}
};
