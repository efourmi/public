#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <dlfcn.h>
#include <unistd.h>
#include <fcntl.h>
#include <syslog.h>
#include <errno.h>
#include <signal.h>
#include "modbusd.h"

#define LOGLEVEL_MAX    (MODBUSD_LOGLEVEL_DEBUG)
#define LOGLEVEL_DEF    (MODBUSD_LOGLEVEL_ERR)
#define SHARED_EXT ".so"

static const char* fedriver = NULL;
static const char* fedriver_arg = NULL;
static const char* bedriver = NULL;
static const char* bedriver_arg = NULL;
static int daemonmode = 0;
static int loglevel = 100;
static volatile int run = 1;

const char* libsearchpath[] = {
  "./",
  "",
  NULL
};

extern const fedriver_def fedrivers[];

extern const bedriver_def bedrivers[];

static void vlogout(int ll, const char* format, va_list list){
    if(ll <= loglevel){
        vprintf(format, list);
        puts("");
    }
}

static void logout(int ll, const char* format, ...){
    va_list va;
    va_start(va, format);
    if(!daemonmode){
        vlogout(ll, format, va);
    }else{
        vsyslog(ll, format, va);
    }
    va_end(va);
}

static void print_help(void){
    printf("usage: \"modbusd help <f|b> [<drivername>]\" for <drivername> help\r\n");
    printf("    f|b                frontend/backend selection\r\n");
    printf("    drivername         driver name, if not specified, print available build-in frontend/backend drivers\r\n");
    printf("    \nor\r\n\r\n");
    printf("usage: \"modbusd [-D] [-l<n>] -b<driver> <backend options> -f<driver> <frontend options>\" for run\r\n");
    printf("    -D                 \"daemon\" mode\r\n");
    printf("    -l<n>              log level in range 0..%i, default is %i\r\n", LOGLEVEL_MAX, LOGLEVEL_DEF);
    printf("    -b<driver>         backend driver\r\n");
    printf("    <backend options>  backend driver options, use \"modbusd help b <driver>\" for driver help\r\n");
    printf("    -f<driver>         frontend driver\r\n");
    printf("    <frontend options> frontend driver options, use \"modbusd help f <driver>\" for driver help\r\n");
    printf("example:\r\n");
    printf("    modbusd -l7 -D -bserial \"-d/dev/ttyUSB0 -m115200N -t1\" -ftcp \"ip:localhost:10001\"\r\n");
}

static void print_helpnotice(void){
    printf("syntax error, use \"modbusd --help\" for more information\r\n");
}

static int frontend_action(int action, ...){
    switch(action){
    case MODBUSD_ACTION_ONOPEN:{
        va_list va;
        va_start(va, action);
        const struct modbusd_frontend* mbfe = va_arg(va, const struct modbusd_frontend*);
        logout(MODBUSD_LOGLEVEL_INFO, "frontend \"%s[%s(%s)]\" successfully opened", mbfe_id(mbfe), mbfe_driver(mbfe), mbfe_arg(mbfe));
        va_end(va);
        return 0;
    }
    case MODBUSD_ACTION_ONCLOSE:{
        va_list va;
        va_start(va, action);
        const struct modbusd_frontend* mbfe = va_arg(va, const struct modbusd_frontend*);
        logout(MODBUSD_LOGLEVEL_INFO, "frontend \"[%s(%s)]\" closed", mbfe_driver(mbfe), mbfe_arg(mbfe));
        va_end(va);
        return 0;
    }
    case MODBUSD_ACTION_ONACCEPT:{
        va_list va;
        va_start(va, action);
        const struct modbusd_frontend* mbfe = va_arg(va, const struct modbusd_frontend*);
        (void)mbfe;
        uint32_t ip4 = va_arg(va, uint32_t);
        logout(MODBUSD_LOGLEVEL_INFO, "new client \"%i.%i.%i.%i\"", (ip4 >> 24) & 0xFF, (ip4 >> 16) & 0xFF, (ip4 >> 8) & 0xFF, ip4 & 0xFF);
        va_end(va);
        return 1;
    }
    case MODBUSD_ACTION_ONDISCONNECT:{
        va_list va;
        va_start(va, action);
        const struct modbusd_frontend* mbfe = va_arg(va, const struct modbusd_frontend*);
        (void)mbfe;
        uint32_t ip4 = va_arg(va, uint32_t);
        logout(MODBUSD_LOGLEVEL_INFO, "disconnect client \"%i.%i.%i.%i\"", (ip4 >> 24) & 0xFF, (ip4 >> 16) & 0xFF, (ip4 >> 8) & 0xFF, ip4 & 0xFF);
        va_end(va);
        return 1;
    }
    case MODBUSD_ACTION_ONREQUEST:{
        va_list va;
        va_start(va, action);
        const struct modbusd_frontend* mbfe = va_arg(va, const struct modbusd_frontend*);
        uint32_t ip4 = va_arg(va, uint32_t);
        uint8_t* data = va_arg(va, uint8_t*);
        int datalen = va_arg(va, int);
        logout(MODBUSD_LOGLEVEL_INFO, "new request (len=%i) from client \"%i.%i.%i.%i\"",datalen, (ip4 >> 24) & 0xFF, (ip4 >> 16) & 0xFF, (ip4 >> 8) & 0xFF, ip4 & 0xFF);
        const struct modbusd_backend* be = (const struct modbusd_backend*)mbfe_tag(mbfe);
        int ret =  be->dorequest(be, data, datalen);
        va_end(va);
        if(ret < 0){
            mbbe_close(be);
        }
        return ret;
    }
    }
    return -1;
}

static int backend_action(int action, ...){
    switch(action){
    case MODBUSD_ACTION_ONOPEN:{
        va_list va;
        va_start(va, action);
        const struct modbusd_backend* mbbe = va_arg(va, const struct modbusd_backend*);
        logout(MODBUSD_LOGLEVEL_INFO, "backend \"%s[%s(%s)]\" successfully opened",mbbe_id(mbbe), mbbe_driver(mbbe), mbbe_arg(mbbe));
        va_end(va);
        return 0;
    }
    case MODBUSD_ACTION_ONCLOSE:{
        va_list va;
        va_start(va, action);
        const struct modbusd_backend* mbbe = va_arg(va, const struct modbusd_backend*);
        logout(MODBUSD_LOGLEVEL_INFO, "backend \"%s[%s(%s)]\" closed", mbbe_id(mbbe), mbbe_driver(mbbe), mbbe_arg(mbbe));
        va_end(va);
        return 0;
    }
    }
    return -1;
}

static void* loadlib(const char* libname, const char* fsuffix){
    size_t libname_sz = strlen(libname);
    size_t fsuffix_sz = strlen(fsuffix);
    size_t sp = 0;
    void* ret = NULL;
    char* flibname = NULL;
    char* ffuncname = (char*)malloc(libname_sz + fsuffix_sz + 1);
    if(ffuncname){
        sprintf(ffuncname,"%s%s", libname, fsuffix);
        while(libsearchpath[sp] != NULL){
            flibname = (char*)malloc(strlen(libsearchpath[sp]) + libname_sz + strlen(SHARED_EXT) + 1);
            if(!flibname){
                break;
            }
            sprintf(flibname, "%s%s%s", libsearchpath[sp], libname, SHARED_EXT);
            void* libhandle = dlopen(libname, RTLD_GLOBAL | RTLD_NOW);
            if(libhandle){
                ret = dlsym(libhandle, ffuncname);
                if(ret){
                    break;
                }
                dlclose(libhandle);
            }else{
                logout(MODBUSD_LOGLEVEL_DEBUG, "main.c/loadlib(\"%s\") return \"%s\"", flibname, dlerror());
            }
            free(flibname);
            flibname = NULL;
            sp++;
        }
        free(flibname);
        free(ffuncname);
    }
    return ret;
}

static const struct modbusd_frontend* frontend_new(const char* drivername, const char* arg, void* tag){
    char* libname;
    struct modbusd_frontend_def mfd;
    mfd.action = frontend_action;
    mfd.logout = logout;
    mfd.arg = arg;
    mfd.modbusd_version = MODBUSD_VERSION;
    mfd.tag = tag;
    int i = 0;
    while(fedrivers[i].name){
        if(!strcmp(drivername, fedrivers[i].name)){
            return fedrivers[i].newfunc(&mfd);
        }
        i++;
    }
    libname = (char*)malloc(strlen(MODBUSD_FRONTEND_PREFIX) + strlen(drivername) + 1);
    if(libname){
        sprintf(libname, "%s%s", MODBUSD_FRONTEND_PREFIX, drivername);
        modbusd_frontend_new newfunc = loadlib(libname, "");
        free(libname);
        if(newfunc){
            return newfunc(&mfd);
        }
    }
    return NULL;
}

static const struct modbusd_backend* backend_new(const char* drivername, const char* arg, void* tag){
    char* libname;
    struct modbusd_backend_def mfd;
    mfd.action = backend_action;
    mfd.logout = logout;
    mfd.arg = arg;
    mfd.modbusd_version = MODBUSD_VERSION;
    mfd.tag = tag;
    int i = 0;
    while(bedrivers[i].name){
        if(!strcmp(drivername, bedrivers[i].name)){
            return bedrivers[i].newfunc(&mfd);
        }
        i++;
    }
    libname = (char*)malloc(strlen(MODBUSD_BACKEND_PREFIX) + strlen(drivername) + 1);
    if(libname){
        sprintf(libname, "%s%s", MODBUSD_BACKEND_PREFIX, drivername);
        modbusd_backend_new newfunc = loadlib(libname, "");
        free(libname);
        if(newfunc){
            return newfunc(&mfd);
        }
    }
    return NULL;
}

static const char* frontend_help(const char* drivername){
    char* libname;
    int i = 0;
    while(fedrivers[i].name){
        if(!strcmp(drivername, fedrivers[i].name)){
            return fedrivers[i].helpfunc();
        }
        i++;
    }
    libname = (char*)malloc(strlen(MODBUSD_FRONTEND_PREFIX) + strlen(drivername) + 1);
    if(libname){
        sprintf(libname, "%s%s", MODBUSD_FRONTEND_PREFIX, drivername);
        modbusd_help helpfunc = loadlib(libname, MODBUSD_HELP_POSTFIX);
        free(libname);
        if(helpfunc){
            return helpfunc();
        }
    }
    return NULL;
}

static const char* backend_help(const char* drivername){
    char* libname;
    int i = 0;
    while(bedrivers[i].name){
        if(!strcmp(drivername, bedrivers[i].name)){
            return bedrivers[i].helpfunc();
        }
        i++;
    }
    libname = (char*)malloc(strlen(MODBUSD_BACKEND_PREFIX) + strlen(drivername) + 1);
    if(libname){
        sprintf(libname, "%s%s", MODBUSD_BACKEND_PREFIX, drivername);
        modbusd_help helpfunc = loadlib(libname, MODBUSD_HELP_POSTFIX);
        free(libname);
        if(helpfunc){
            return helpfunc();
        }
    }
    return NULL;
}

static void sig_fatalsighandler(int sig){
    logout(LOG_CRIT, "main.c/sig_fatalsighandler(): catch '%s' signal, shutdown", strsignal(sig));
    run = 0;
}

static void sig_usr1signalhandler(int sig){
    sig_fatalsighandler(sig);
}

static void sig_hupsignalhandler(int sig){
    sig_fatalsighandler(sig);
}

static void sig_ignore(int sig){
    struct sigaction sa;
    int r;
    sa.sa_handler = SIG_IGN;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    r = sigaction(sig, &sa, NULL);
    if(r < 0){
        r = -errno;
        logout(LOG_DEBUG, "main.c/sig_ignore()/sigaction(): return %i", r);
    }
}

static void sig_sethandler(int sig, void(*handler)(int)){
    struct sigaction sa;
    int r;
    sa.sa_handler = handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    r = sigaction(sig, &sa, NULL);
    if(r < 0){
        r = -errno;
        logout(LOG_DEBUG, "main.c/sig_sethandler()/sigaction(): return %i", r);
    }
}

static void sig_init(void){
    sig_ignore(SIGUSR2);
    sig_ignore(SIGPIPE);
    sig_ignore(SIGALRM);
    sig_ignore(SIGTSTP);
    sig_ignore(SIGPROF);
    sig_ignore(SIGCHLD);
    sig_sethandler(SIGQUIT, sig_fatalsighandler);
    sig_sethandler(SIGILL, sig_fatalsighandler);
    sig_sethandler(SIGTRAP, sig_fatalsighandler);
    sig_sethandler(SIGABRT, sig_fatalsighandler);
    sig_sethandler(SIGIOT, sig_fatalsighandler);
    sig_sethandler(SIGBUS, sig_fatalsighandler);
    sig_sethandler(SIGFPE, sig_fatalsighandler);
    sig_sethandler(SIGSEGV, sig_fatalsighandler);
    sig_sethandler(SIGCONT, sig_fatalsighandler);
    sig_sethandler(SIGPWR, sig_fatalsighandler);
    sig_sethandler(SIGSYS, sig_fatalsighandler);
    sig_sethandler(SIGTERM,sig_fatalsighandler);
    sig_sethandler(SIGINT, sig_fatalsighandler);
    sig_sethandler(SIGUSR1, sig_usr1signalhandler);
    sig_sethandler(SIGHUP, sig_hupsignalhandler);
}

static int process_daemonize(void){
    int i, r;
    r = chdir("/");
    if(r < 0){
        r = -errno;
        logout(LOG_DEBUG, "main.c/process_daemonize()/chdir(): return %i", r);
        return r;
    }
    r = fork();
    if(r < 0){
        r = -errno;
        logout(LOG_DEBUG, "main.c/process_daemonize()/fork(): return %i", r);
    }else if(r > 0){
        return 1;
    }
    r = setsid();
    if(r < 0){
        r = -errno;
        logout(LOG_DEBUG, "main.c/process_daemonize()/setsid(): return %i", r);
        if(r != -EPERM){
            return r;
        }
    }
    i = sysconf(_SC_OPEN_MAX);
    while(--i >= 0){
        close(i);
    }
    i = open("/dev/null", O_RDWR);
    if(i < 0){
        logout(LOG_DEBUG, "main.c/process_daemonize()/open(): return %i", -errno);
    }
    i = open("/dev/null", O_RDWR);
    if(i < 0){
        logout(LOG_DEBUG, "main.c/process_daemonize()/open(): return %i", -errno);
    }
    i = open("/dev/null", O_RDWR);
    if(i < 0){
        logout(LOG_DEBUG, "main.c/process_daemonize()/open(): return %i", -errno);
    }
    return 0;
}

int main(int argc, char *argv[]){
    const struct modbusd_backend* be = NULL;
    const struct modbusd_frontend* fe = NULL;
    static const char* empty_str = "";
    if((argc == 2) && (!strcmp(argv[1], "--help"))){
        print_help();
        return EXIT_SUCCESS;
    }else if((argc >= 2) && (!strcmp(argv[1], "help"))){
        if(argc >= 3){
            if(argv[2][0] == 'f'){
                if(argc == 4){
                    const char* h = frontend_help(argv[3]);
                    if(h){
                        printf("%s", h);
                    }
                    return EXIT_SUCCESS;
                }else{
                    printf("available frontend drivers:\r\n");
                    int i = 0;
                    while(fedrivers[i].name){
                        printf("\t%s\r\n",fedrivers[i].name);
                        i++;
                    }
                    return EXIT_SUCCESS;
                }
            }else if(argv[2][0] == 'b'){
                if(argc == 4){
                    const char* h = backend_help(argv[3]);
                    if(h){
                        printf("%s", h);
                    }
                    return EXIT_SUCCESS;
                }else{
                    printf("available backend drivers:\r\n");
                    int i = 0;
                    while(bedrivers[i].name){
                        printf("\t%s\r\n", bedrivers[i].name);
                        i++;
                    }
                    return EXIT_SUCCESS;
                }
            }
        }
        printf("use \"f\" or \"b\" for selection\r\n");
        return EXIT_FAILURE;
    }
    int argn = 1;
    int _daemonmode = 0;
    while(argn < argc){
        if((argv[argn][0] == '-') && (argv[argn][1] == 'f') && (argv[argn][2] != 0)){
            // -f[driver] frontend driver definition
            if(fedriver){
                logout(MODBUSD_LOGLEVEL_CRIT, "duplicate -f[driver] flag definition, use \"--help\"");
                return EXIT_FAILURE;
            }
            fedriver = argv[argn] + 2;
            argn++;
            if(argn == argc){
                fedriver_arg = empty_str;
            }else{
                fedriver_arg = argv[argn++];
            }
        }else if((argv[argn][0] == '-') && (argv[argn][1] == 'b') && (argv[argn][2] != 0)){
            // -b[driver] backend driver definition
            if(bedriver){
                logout(MODBUSD_LOGLEVEL_CRIT, "duplicate -b[driver] flag definition, use \"--help\"");
                return EXIT_FAILURE;
            }
            bedriver = argv[argn] + 2;
            argn++;
            if(argn == argc){
                bedriver_arg = empty_str;
            }else{
                bedriver_arg = argv[argn++];
            }
        }else if((argv[argn][0] == '-') && (argv[argn][1] == 'l') && (argv[argn][2] != 0)){
            if(loglevel < 100){
                logout(MODBUSD_LOGLEVEL_CRIT, "duplicate -l[num] flag definition, use \"--help\"");
                return EXIT_FAILURE;
            }
            char* endp;
            unsigned long int ll = strtoul(argv[argn] + 2, &endp, 10);
            if((*endp != 0) || (ll > LOGLEVEL_MAX)){
                logout(MODBUSD_LOGLEVEL_CRIT, "log level must be integer in range 0..%i", LOGLEVEL_MAX);
            }
            loglevel = ll;
            argn++;
        }else if(!strcmp(argv[argn], "-D")){
            // daemon mode flag
            if(_daemonmode){
                logout(MODBUSD_LOGLEVEL_CRIT, "duplicate -D flag definition, use \"--help\"");
                return EXIT_FAILURE;
            }
            _daemonmode = 1;
            argn++;
        }else{
            logout(MODBUSD_LOGLEVEL_CRIT, "undefined \"%s\" flag definition, use \"modbusd --help\" for help", argv[argn]);
            return EXIT_FAILURE;
        }
    }
    if(loglevel == 100){
        loglevel = LOGLEVEL_DEF;
    }
    if(!fedriver || !fedriver_arg || !bedriver || !bedriver_arg){
        print_helpnotice();
        return EXIT_FAILURE;
    }
    if(_daemonmode) {
        openlog("modbusd", LOG_PID, LOG_DAEMON);
        setlogmask(LOG_MASK(loglevel + 1) - 1);
        daemonmode = 1;
    }
    if(daemonmode){
        int r = process_daemonize();
        if(r == 1){
            return EXIT_SUCCESS;
        }else if(r < 0){
            return EXIT_FAILURE;
        }
    }
    sig_init();
    be = backend_new(bedriver, bedriver_arg, NULL);
    if(!be){
        logout(MODBUSD_LOGLEVEL_ERR, "cannot init backend \"%s\" driver", bedriver);
        goto __exit;
    }
    fe = frontend_new(fedriver, fedriver_arg, (void*)be);
    if(!fe){
        logout(MODBUSD_LOGLEVEL_ERR, "cannot init frontend \"%s\" driver", fedriver);
        goto __exit;
    }
    if(mbfe_open(fe) < 0){
        logout(MODBUSD_LOGLEVEL_ERR, "cannot open frontend \"%s\"", mbfe_arg(fe));
    }
    if(mbbe_open(be) < 0){
        logout(MODBUSD_LOGLEVEL_ERR, "cannot open backend \"%s\"", mbbe_arg(be));
    }
    while(run){
        if(!mbbe_status(be)){
            mbbe_open(be);
        }
        if(mbfe_status(fe)){
            mbfe_serve(fe, 200);
        }else{
            struct timeval tv;
            tv.tv_sec = 0;
            tv.tv_usec = 200000;
            select(0, NULL, NULL, NULL, &tv);
            mbfe_open(fe);
        }
    }
__exit:
    if(fe){
        mbfe_free(fe);
    }
    if(be){
        mbbe_free(be);
    }
    if(daemonmode){
        closelog();
    }
    return 0;
}
