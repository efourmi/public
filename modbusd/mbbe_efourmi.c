#include "modbusd.h"
#include "efourmi.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libusb-1.0/libusb.h>

#define MB_ERRNO

struct modbusd_backend_efourmi{
    struct modbusd_backend mbbe;
    modbusd_action action;
    modbusd_logout logout;
    int vid;
    int pid;
    struct libusb_context* context;
    struct libusb_device_handle* usb_handle;
    int efourmi_interface;
    int proto;
    int out_ep;
    int out_ep_int;
    int out_ep_size;
    int in_ep;
    int in_ep_int;
    int in_ep_size;
    char serial[EFOURMI_SERIAL_SIZE + 1];
    char arg[];
};
#define MBBETOMBBEEFOURMI(MBBE) ((struct modbusd_backend_efourmi*)((size_t)(MBBE) - offsetof(struct modbusd_backend_efourmi, mbbe)))


static char* getarg(char** parg){
    char* p = *parg;
    char* begin;
    while(*p == ' '){
        p++;
    }
    if(*p == 0){
        return NULL;
    }
    begin = p;
    while((*p != ' ') && (*p != 0)){
        p++;
    }
    if(*p == 0){
        *parg = p;
    }else{
        *p = 0;
        *parg = p + 1;
    }
    return begin;
}

static int parse_arg(struct modbusd_backend_efourmi* be){
    char* _arg = strdup(be->arg);
    if(!_arg){
        be->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_efourmi.c/parse_arg(): not enought memory");
        return -1;
    }
    char* carg,* parg;
    parg = _arg;
    while(carg = getarg(&parg), carg){
        if((carg[0] == '-') && (carg[1] == 'v') && (carg[2] != 0)){
            if(be->vid > 0){
                be->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_efourmi.c/parse_arg() VID redefined");
                goto __error;
            }
            char* end;
            unsigned long vid = strtoul(carg + 2, &end, 10);
            if((vid == 0) || (*end != 0)){
                vid = strtoul(carg + 2, &end, 16);
            }
            if((vid != 0) && (*end == 0)){
                if(vid > 0xFFFF){
                    be->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_efourmi.c/parse_arg() VID is out of range 0..65535");
                    goto __error;
                }else{
                    be->vid = vid;
                }
            }else{
                be->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_efourmi.c/parse_arg() VID definition corrupted");
                goto __error;
            }
        }else if((carg[0] == '-') && (carg[1] == 'p') && (carg[2] != 0)){
            if(be->pid > 0){
                be->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_efourmi.c/parse_arg() PID redefined");
                goto __error;
            }
            char* end;
            unsigned long pid = strtoul(carg + 2, &end, 10);
            if((pid == 0) || (*end != 0)){
                pid = strtoul(carg + 2, &end, 16);
            }
            if((pid != 0) && (*end == 0)){
                if(pid > 0xFFFF){
                    be->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_efourmi.c/parse_arg() PID is out of range 0..65535");
                    goto __error;
                }else{
                    be->pid = pid;
                }
            }else{
                be->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_efourmi.c/parse_arg() PID definition corrupted");
                goto __error;
            }
        }else if((carg[0] == '-') && (carg[1] == 's') && (carg[2] != 0)){
            if(be->serial[0] != 0){
                be->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_efourmi.c/parse_arg() SERIAL redefined");
                goto __error;
            }
            if(strlen(carg + 2) != EFOURMI_SERIAL_SIZE){
                goto __error_serial;
            }
            strcpy(be->serial, carg + 2);
            for(int i = 0; i < EFOURMI_SERIAL_SIZE; i++){
                if(((be->serial[i] >= '0') && (be->serial[i] <= '9'))
                 ||((be->serial[i] >= 'a') && (be->serial[i] <= 'f'))
                 ||((be->serial[i] >= 'A') && (be->serial[i] <= 'F'))){
                }else{
                    goto __error_serial;
                }
            }
        }else{
            be->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_efourmi.c/parse_arg() unexpected argument \"%s\"", carg);
            goto __error;
        }
    }
    free(_arg);
    return 0;
__error_serial:
    be->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_efourmi.c/parse_arg() SERIAL corrupted");
__error:
    free(_arg);
    return -1;
}

struct mbbe_efourmi_open_state{
    struct libusb_device** devicelist;
    struct libusb_device_handle* device;
    struct libusb_config_descriptor* configdescr;
    int device_interface;
    struct libusb_device_descriptor devicedescr;
    char str[256];
};

static int mbbe_efourmi_open_finish(int ret, struct mbbe_efourmi_open_state* state){
    if(state->device){
        if(state->device_interface){
            libusb_release_interface(state->device, state->device_interface);
            libusb_attach_kernel_driver(state->device, state->device_interface);
        }
        libusb_close(state->device);
    }
    if(state->configdescr){
        libusb_free_config_descriptor(state->configdescr);
    }
    if(state->devicelist){
        libusb_free_device_list(state->devicelist, 1);
    }
    free(state);
    return ret;
}

MB_ERRNO static int mbbe_efourmi_open(const struct modbusd_backend* mbbe){
    struct modbusd_backend_efourmi* mbbeefourmi = MBBETOMBBEEFOURMI(mbbe);
    int ret;
    struct mbbe_efourmi_open_state* state;
    if(mbbeefourmi->usb_handle != NULL){
        return MODBUSD_EISCONN;
    }
    state = (struct mbbe_efourmi_open_state*)malloc(sizeof(struct mbbe_efourmi_open_state));
    if(!state){
        return MODBUSD_ENOMEM;
    }
    state->devicelist = NULL;
    state->device = NULL;
    state->configdescr = NULL;
    state->device_interface = -1;
    ssize_t device_count = libusb_get_device_list(mbbeefourmi->context, &state->devicelist);
    if(device_count < 0){
        mbbeefourmi->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_efourmi.c/mbbe_efourmi_open()/libusb_get_device_list(): return %i", (int)device_count);
        return MODBUSD_EERROR;
    }
    ssize_t device_i;
    for(device_i = 0; device_i < device_count; device_i++){
        ret = libusb_get_device_descriptor(state->devicelist[device_i], &state->devicedescr);
        if(ret != LIBUSB_SUCCESS){
            continue;
        }
        if((state->devicedescr.idVendor != mbbeefourmi->vid) || (state->devicedescr.idProduct != mbbeefourmi->pid)){
            continue;
        }
        ret = libusb_open(state->devicelist[device_i], &state->device);
        if(ret != LIBUSB_SUCCESS){
            continue;
        }
        ret = libusb_get_string_descriptor_ascii(state->device, state->devicedescr.iSerialNumber, (unsigned char*)state->str, sizeof(state->str));
        if((ret < 0) || (strcmp(state->str, mbbeefourmi->serial))){
            goto __close_dev;
        }
        ret = libusb_get_active_config_descriptor(state->devicelist[device_i], &state->configdescr);
        if(ret < 0){
            goto __close_dev;
        }
        int interface_i;
        for(interface_i = 0; interface_i < state->configdescr->bNumInterfaces; interface_i++){
            const struct libusb_interface_descriptor* interface = &state->configdescr->interface[interface_i].altsetting[0];
            if((interface->bInterfaceClass == EFOURMI_USB_INTERFACE_CLASS)
                 && (interface->bInterfaceSubClass == EFOURMI_USB_INTERFACE_SUBCLASS)){
                int proto = interface->bInterfaceProtocol;
                ret = libusb_kernel_driver_active(state->device, interface_i);
                if(ret < 0){
                    mbbeefourmi->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_efourmi.c/mbbe_efourmi_open()/libusb_kernel_driver_active(): return %i", ret);
                    break;
                }
                if(ret > 0){
                    ret = libusb_detach_kernel_driver(state->device, interface_i);
                    if(ret < 0){
                        mbbeefourmi->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_efourmi.c/mbbe_efourmi_open()/libusb_kernel_driver_active(): return %i", ret);
                        break;
                    }
                }
                ret = libusb_claim_interface(state->device, interface_i);
                if(ret < 0){
                    mbbeefourmi->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_efourmi.c/mbbe_efourmi_open()/libusb_claim_interface(): return %i", ret);
                    break;
                }
                ret = libusb_control_transfer(state->device, LIBUSB_ENDPOINT_OUT | LIBUSB_REQUEST_TYPE_VENDOR | LIBUSB_RECIPIENT_INTERFACE, EFOURMI_USB_REQ_SYNC,0, interface_i, NULL, 0, 1000);
                if(ret < 0){
                    mbbeefourmi->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_efourmi.c/mbbe_efourmi_open()/libusb_control_transfer(EFOURMI_USB_REQ_SYNC): return %i", ret);
                    break;
                }
                mbbeefourmi->usb_handle = state->device;
                mbbeefourmi->efourmi_interface = interface_i;
                mbbeefourmi->proto = proto;
                if(interface->endpoint[0].bEndpointAddress & LIBUSB_ENDPOINT_DIR_MASK){
                    mbbeefourmi->in_ep = interface->endpoint[0].bEndpointAddress;
                    mbbeefourmi->in_ep_int = (interface->endpoint[0].bmAttributes & LIBUSB_TRANSFER_TYPE_MASK) == LIBUSB_TRANSFER_TYPE_INTERRUPT;
                    mbbeefourmi->in_ep_size = interface->endpoint[0].wMaxPacketSize;
                    mbbeefourmi->out_ep = interface->endpoint[1].bEndpointAddress;
                    mbbeefourmi->out_ep_int = (interface->endpoint[1].bmAttributes & LIBUSB_TRANSFER_TYPE_MASK) == LIBUSB_TRANSFER_TYPE_INTERRUPT;
                    mbbeefourmi->out_ep_size = interface->endpoint[1].wMaxPacketSize;
                }else{
                    mbbeefourmi->in_ep = interface->endpoint[1].bEndpointAddress;
                    mbbeefourmi->in_ep_int = (interface->endpoint[1].bmAttributes & LIBUSB_TRANSFER_TYPE_MASK) == LIBUSB_TRANSFER_TYPE_INTERRUPT;
                    mbbeefourmi->in_ep_size = interface->endpoint[1].wMaxPacketSize;
                    mbbeefourmi->out_ep = interface->endpoint[0].bEndpointAddress;
                    mbbeefourmi->out_ep_int = (interface->endpoint[0].bmAttributes & LIBUSB_TRANSFER_TYPE_MASK) == LIBUSB_TRANSFER_TYPE_INTERRUPT;
                    mbbeefourmi->out_ep_size = interface->endpoint[0].wMaxPacketSize;
                }
                state->device = NULL;
                mbbeefourmi->action(MODBUSD_ACTION_ONOPEN, mbbeefourmi);
                return mbbe_efourmi_open_finish(0, state);
            }
        }
        libusb_free_config_descriptor(state->configdescr);
        state->configdescr = NULL;
        __close_dev:
        libusb_close(state->device);
        state->device = NULL;
    }
    return mbbe_efourmi_open_finish(MODBUSD_ENODEV, state);
}

MB_ERRNO static int mbbe_efourmi_close(const struct modbusd_backend* mbbe){
    struct modbusd_backend_efourmi* mbbeefourmi = MBBETOMBBEEFOURMI(mbbe);
    if(mbbeefourmi->usb_handle == NULL){
        return MODBUSD_ENOTCONN;
    }
    libusb_release_interface(mbbeefourmi->usb_handle, mbbeefourmi->efourmi_interface);
    libusb_attach_kernel_driver(mbbeefourmi->usb_handle, mbbeefourmi->efourmi_interface);
    libusb_close(mbbeefourmi->usb_handle);
    mbbeefourmi->usb_handle = NULL;
    mbbeefourmi->action(MODBUSD_ACTION_ONCLOSE, &mbbeefourmi->mbbe);
    return 0;
}

MB_ERRNO static int mbbe_efourmi_status(const struct modbusd_backend* mbbe){
    struct modbusd_backend_efourmi* mbbeefourmi = MBBETOMBBEEFOURMI(mbbe);
    if(mbbeefourmi->usb_handle == NULL){
        return 0;
    }
    int c;
    int ret = libusb_get_configuration(mbbeefourmi->usb_handle, &c);
    if(ret < 0){
        mbbe_efourmi_close(&mbbeefourmi->mbbe);
        return 0;
    }
    return 1;
}

MB_ERRNO static int mbbe_efourmi_dorequest(const struct modbusd_backend* mbbe, uint8_t* data, int datalen){
    struct modbusd_backend_efourmi* mbbeefourmi = MBBETOMBBEEFOURMI(mbbe);
    int ret;
    int alen;
    if(mbbeefourmi->usb_handle == NULL){
        return MODBUSD_ENOTCONN;
    }
    if(datalen == 0){
        return 0;
    }
    if(datalen > 254){
        return 0;
    }
    if(mbbeefourmi->out_ep_int){
        ret = libusb_interrupt_transfer(mbbeefourmi->usb_handle, mbbeefourmi->out_ep, data, datalen, &alen, 1000);
        if(ret < 0){
            mbbeefourmi->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_efourmi.c/mbbe_efourmi_dorequest()/libusb_interrupt_transfer(): return %i", ret);
            return MODBUSD_EERROR;
        }
        if(alen != datalen){
            mbbeefourmi->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_efourmi.c/mbbe_efourmi_dorequest()/libusb_interrupt_transfer(): io error");
            return MODBUSD_EERROR;
        }
        if((datalen > 0) && ((datalen % mbbeefourmi->out_ep_size) == 0)){
            ret = libusb_interrupt_transfer(mbbeefourmi->usb_handle, mbbeefourmi->out_ep, NULL, 0, &alen, 1000);
            if(ret < 0){
                mbbeefourmi->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_efourmi.c/mbbe_efourmi_dorequest()/libusb_interrupt_transfer(0): return %i", ret);
                return MODBUSD_EERROR;
            }
            if(alen != 0){
                mbbeefourmi->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_efourmi.c/mbbe_efourmi_dorequest()/libusb_interrupt_transfer(0): io error");
                return MODBUSD_EERROR;
            }
        }
    }else{
        ret = libusb_bulk_transfer(mbbeefourmi->usb_handle, mbbeefourmi->out_ep, data, datalen, &alen, 1000);
        if(ret < 0){
            mbbeefourmi->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_efourmi.c/mbbe_efourmi_dorequest()/libusb_bulk_transfer(): return %i", ret);
            return MODBUSD_EERROR;
        }
        if(alen != datalen){
            mbbeefourmi->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_efourmi.c/mbbe_efourmi_dorequest()/libusb_bulk_transfer(): io error");
            return MODBUSD_EERROR;
        }
        if((datalen > 0) && ((datalen % mbbeefourmi->out_ep_size) == 0)){
            ret = libusb_bulk_transfer(mbbeefourmi->usb_handle, mbbeefourmi->out_ep, NULL, 0, &alen, 1000);
            if(ret < 0){
                mbbeefourmi->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_efourmi.c/mbbe_efourmi_dorequest()/libusb_bulk_transfer(0): return %i", ret);
                return MODBUSD_EERROR;
            }
            if(alen != 0){
                mbbeefourmi->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_efourmi.c/mbbe_efourmi_dorequest()/libusb_bulk_transfer(0): io error");
                return MODBUSD_EERROR;
            }
        }
    }
    if(mbbeefourmi->in_ep_int){
        ret = libusb_interrupt_transfer(mbbeefourmi->usb_handle, mbbeefourmi->in_ep, data, 256, &alen, 1000);
        if(ret < 0){
            mbbeefourmi->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_efourmi.c/mbbe_efourmi_dorequest()/libusb_interrupt_transfer(): return %i", ret);
            return MODBUSD_EERROR;
        }
    }else{
        ret = libusb_bulk_transfer(mbbeefourmi->usb_handle, mbbeefourmi->in_ep, data, 256, &alen, 1000);
        if(ret < 0){
            mbbeefourmi->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_efourmi.c/mbbe_efourmi_dorequest()/libusb_bulk_transfer(): return %i", ret);
            return MODBUSD_EERROR;
        }
    }
    return alen;
}

MB_ERRNO static void mbbe_efourmi_free(const struct modbusd_backend* mbbe){
    struct modbusd_backend_efourmi* mbbeefourmi = MBBETOMBBEEFOURMI(mbbe);
    mbbe_efourmi_close(mbbe);
    libusb_exit(mbbeefourmi->context);
    free(mbbeefourmi);
}

static int dummy_action(int action, ...){
    (void)action;
    return 0;
}

static void dummy_logout(int ll, const char* format, ...){
    (void)ll;
    (void)format;
}

const struct modbusd_backend* mbbe_efourmi(struct modbusd_backend_def* def){
    if(def->modbusd_version != MODBUSD_VERSION){
        if(def->logout){
            def->logout(MODBUSD_LOGLEVEL_DEBUG, "modbusd version (%i) mismatch frontend driver version (%i)", def->modbusd_version, MODBUSD_VERSION);
        }
        return NULL;
    }
    struct modbusd_backend_efourmi* be = (struct modbusd_backend_efourmi*)malloc(sizeof(struct modbusd_backend_efourmi) + strlen(def->arg) + 1);
    int ret;
    if(be){
        be->mbbe.open = mbbe_efourmi_open;
        be->mbbe.close = mbbe_efourmi_close;
        be->mbbe.status = mbbe_efourmi_status;
        be->mbbe.dorequest = mbbe_efourmi_dorequest;
        be->mbbe.free = mbbe_efourmi_free;
        be->mbbe.arg = be->arg;
        be->mbbe.tag = def->tag;
        be->mbbe.driver = "efourmi";
        be->mbbe.id = be->mbbe.arg;

        be->action = def->action? def->action: dummy_action;
        be->logout = def->logout? def->logout: dummy_logout;

        be->vid = -1;
        be->pid = -1;

        ret = libusb_init(&be->context);
        if(ret != LIBUSB_SUCCESS){
            be->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_efourmi.c/mbbe_efourmi()/libusb_init(): return %i", ret);
            goto __error_free;
        }
        be->usb_handle = NULL;
        be->efourmi_interface = -1;
        be->serial[0] = 0;

        strcpy(be->arg, def->arg);

        ret = parse_arg(be);
        if(ret){
            be->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_efourmi.c/mbbe_efourmi(): check argument, use \"modbusd help b efourmi\" for help");
            goto __error_free;
        }

        if(be->vid < 0){
            be->vid = EFOURMI_USB_VID_DEF;
        }

        if(be->pid < 0){
            be->pid = EFOURMI_USB_PID_DEF;
        }

        return &be->mbbe;
    __error_free:
        free(be);
    }else{
        def->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_efourmi.c/mbbe_efourmi(): not enought memory");
    }
    return NULL;
}

const char* mbbe_efourmi_help(void){
    return "usage: ... -befourmi \"[-vVID] [-pPID] -sSERIAL\" ...\r\n" \
           "    -v[...]            custom target VID, optional\r\n" \
           "    -p[...]            custom target PID, optional\r\n" \
           "    -s[SERIAL]         serial number of efourmi device\r\n";
}

