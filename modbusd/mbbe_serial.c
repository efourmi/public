#include "modbusd.h"
#include <stdlib.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>

#define NEG_ERRNO
#define MB_ERRNO

struct mbbe_serial_mode{
    int kbps;
    int parity;
    int timeout;
    char device[];
};

struct modbusd_backend_serial{
    struct modbusd_backend mbbe;
    modbusd_action action;
    modbusd_logout logout;
    struct mbbe_serial_mode* mode;
    int fd;
    char arg[];
};
#define MBBETOMBBESERIAL(MBBE) ((struct modbusd_backend_serial*)((size_t)(MBBE) - offsetof(struct modbusd_backend_serial, mbbe)))

MB_ERRNO  static int negerrno_to_mberrno(int e){
    switch(e){
    case -EINVAL: return MODBUSD_EINVAL;
    case -ENOMEM: return MODBUSD_ENOMEM;
    case -EISCONN: return MODBUSD_EISCONN;
    case -ENOTCONN: return MODBUSD_ENOTCONN;
    case -EINTR: return MODBUSD_EINTR;
    case -ENODEV: case -ENOENT: return MODBUSD_ENODEV;
    default: return MODBUSD_EERROR;
    }
}

static const unsigned short crc16tab[]={
    0x0000, 0xC0C1, 0xC181, 0x0140, 0xC301, 0x03C0, 0x0280, 0xC241,
    0xC601, 0x06C0, 0x0780, 0xC741, 0x0500, 0xC5C1, 0xC481, 0x0440,
    0xCC01, 0x0CC0, 0x0D80, 0xCD41, 0x0F00, 0xCFC1, 0xCE81, 0x0E40,
    0x0A00, 0xCAC1, 0xCB81, 0x0B40, 0xC901, 0x09C0, 0x0880, 0xC841,
    0xD801, 0x18C0, 0x1980, 0xD941, 0x1B00, 0xDBC1, 0xDA81, 0x1A40,
    0x1E00, 0xDEC1, 0xDF81, 0x1F40, 0xDD01, 0x1DC0, 0x1C80, 0xDC41,
    0x1400, 0xD4C1, 0xD581, 0x1540, 0xD701, 0x17C0, 0x1680, 0xD641,
    0xD201, 0x12C0, 0x1380, 0xD341, 0x1100, 0xD1C1, 0xD081, 0x1040,
    0xF001, 0x30C0, 0x3180, 0xF141, 0x3300, 0xF3C1, 0xF281, 0x3240,
    0x3600, 0xF6C1, 0xF781, 0x3740, 0xF501, 0x35C0, 0x3480, 0xF441,
    0x3C00, 0xFCC1, 0xFD81, 0x3D40, 0xFF01, 0x3FC0, 0x3E80, 0xFE41,
    0xFA01, 0x3AC0, 0x3B80, 0xFB41, 0x3900, 0xF9C1, 0xF881, 0x3840,
    0x2800, 0xE8C1, 0xE981, 0x2940, 0xEB01, 0x2BC0, 0x2A80, 0xEA41,
    0xEE01, 0x2EC0, 0x2F80, 0xEF41, 0x2D00, 0xEDC1, 0xEC81, 0x2C40,
    0xE401, 0x24C0, 0x2580, 0xE541, 0x2700, 0xE7C1, 0xE681, 0x2640,
    0x2200, 0xE2C1, 0xE381, 0x2340, 0xE101, 0x21C0, 0x2080, 0xE041,
    0xA001, 0x60C0, 0x6180, 0xA141, 0x6300, 0xA3C1, 0xA281, 0x6240,
    0x6600, 0xA6C1, 0xA781, 0x6740, 0xA501, 0x65C0, 0x6480, 0xA441,
    0x6C00, 0xACC1, 0xAD81, 0x6D40, 0xAF01, 0x6FC0, 0x6E80, 0xAE41,
    0xAA01, 0x6AC0, 0x6B80, 0xAB41, 0x6900, 0xA9C1, 0xA881, 0x6840,
    0x7800, 0xB8C1, 0xB981, 0x7940, 0xBB01, 0x7BC0, 0x7A80, 0xBA41,
    0xBE01, 0x7EC0, 0x7F80, 0xBF41, 0x7D00, 0xBDC1, 0xBC81, 0x7C40,
    0xB401, 0x74C0, 0x7580, 0xB541, 0x7700, 0xB7C1, 0xB681, 0x7640,
    0x7200, 0xB2C1, 0xB381, 0x7340, 0xB101, 0x71C0, 0x7080, 0xB041,
    0x5000, 0x90C1, 0x9181, 0x5140, 0x9301, 0x53C0, 0x5280, 0x9241,
    0x9601, 0x56C0, 0x5780, 0x9741, 0x5500, 0x95C1, 0x9481, 0x5440,
    0x9C01, 0x5CC0, 0x5D80, 0x9D41, 0x5F00, 0x9FC1, 0x9E81, 0x5E40,
    0x5A00, 0x9AC1, 0x9B81, 0x5B40, 0x9901, 0x59C0, 0x5880, 0x9841,
    0x8801, 0x48C0, 0x4980, 0x8941, 0x4B00, 0x8BC1, 0x8A81, 0x4A40,
    0x4E00, 0x8EC1, 0x8F81, 0x4F40, 0x8D01, 0x4DC0, 0x4C80, 0x8C41,
    0x4400, 0x84C1, 0x8581, 0x4540, 0x8701, 0x47C0, 0x4680, 0x8641,
    0x8201, 0x42C0, 0x4380, 0x8341, 0x4100, 0x81C1, 0x8081, 0x4040
};
#define CRC16(ch, crc) (crc16tab[((crc) ^ (ch)) & 0xff] ^ ((crc) >> 8))
static uint16_t crc16(const uint8_t* data, unsigned long len){
    uint16_t crc = 0xFFFF;
    unsigned long i;
    for(i = 0; i < len; i++){
        crc = CRC16(data[i], crc);
    }
    return crc;
}

static int speedtokbps(unsigned long speed){
    switch(speed){
    case 50:      return B50;
    case 75:      return B75;
    case 110:     return B110;
    case 134:     return B134;
    case 150:     return B150;
    case 200:     return B200;
    case 300:     return B300;
    case 600:     return B600;
    case 1200:    return B1200;
    case 1800:    return B1800;
    case 2400:    return B2400;
    case 4800:    return B4800;
    case 9600:    return B9600;
    case 19200:   return B19200;
    case 38400:   return B38400;
    case 57600:   return B57600;
    case 115200:  return B115200;
    case 230400:  return B230400;
    case 460800:  return B460800;
    case 500000:  return B500000;
    case 576000:  return B576000;
    case 921600:  return B921600;
    case 1000000: return B1000000;
    case 1152000: return B1152000;
    case 1500000: return B1500000;
    case 2000000: return B2000000;
    case 2500000: return B2500000;
    case 3000000: return B3000000;
    default:return 0;
    }
}

static char* getarg(char** parg){
    char* p = *parg;
    char* begin;
    while(*p == ' '){
        p++;
    }
    if(*p == 0){
        return NULL;
    }
    begin = p;
    while((*p != ' ') && (*p != 0)){
        p++;
    }
    if(*p == 0){
        *parg = p;
    }else{
        *p = 0;
        *parg = p + 1;
    }
    return begin;
}

static struct mbbe_serial_mode* mbbe_parse(struct modbusd_backend_def* def){
    char* arg_s = strdup(def->arg);
    char* carg,* parg;
    char* device_s = NULL;
    char* mode_s = NULL;
    char* timeout_s = NULL;
    unsigned long timeout = 0;
    unsigned long speed = 0;
    int parity = -1;
    if(!arg_s){
        if(def->logout){
            def->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_serial.c/mbbe_parse(): not enought memory");
        }
        return NULL;
    }
    parg = arg_s;
    while(carg = getarg(&parg), carg){
        if((carg[0] == '-') && (carg[1] == 'd') && (carg[2] != 0)){
            if(device_s){
                goto __error;
            }
            device_s = carg + 2;
        }else if((carg[0] == '-') && (carg[1] == 'm') && (carg[2] != 0)){
            if(mode_s){
                goto __error;
            }
            mode_s = carg + 2;
        }else if((carg[0] == '-') && (carg[1] == 't') && (carg[2] != 0)){
            if(timeout_s){
                goto __error;
            }
            timeout_s = carg + 2;
        }else{
            goto __error;
        }
    }
    if(!device_s || !mode_s || !timeout_s){
        goto __error;
    }
    speed = speedtokbps(strtoul(mode_s, &parg, 10));
    if((speed == 0) || (parg == mode_s) || (*parg == 0) || (*(parg + 1) != 0)){
        goto __error;
    }
    switch(*parg){
    case 'N':
        parity = 0;
        break;
    case 'O':
        parity = 1;
        break;
    case 'E':
        parity = 2;
        break;
    }
    if(parity == -1){
        goto __error;
    }
    timeout = strtoul(timeout_s, &parg, 10);
    if((timeout == 0) || (parg == timeout_s) || (*parg != 0)){
        goto __error;
    }
    struct mbbe_serial_mode* mode = (struct mbbe_serial_mode*)malloc(sizeof(struct mbbe_serial_mode) + strlen(device_s) + 1);
    if(mode){
        mode->kbps = speed;
        mode->parity = parity;
        mode->timeout = timeout;
        strcpy(mode->device, device_s);
    }else{
        if(def->logout){
            def->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_serial.c/mbbe_parse(): not enought memory");
        }
    }
    free(arg_s);
    return mode;
__error:
    free(arg_s);
    if(def->logout){
        def->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_serial.c/mbbe_parse(): check argument, use \"modbusd help b serial\" for help");
    }
    return NULL;
}

MB_ERRNO static int mbbe_serial_open(const struct modbusd_backend* mbbe){
    struct modbusd_backend_serial* mbbeserial = MBBETOMBBESERIAL(mbbe);
    int ret;
    struct termios options;
    if(mbbeserial->fd >= 0){
        return MODBUSD_EISCONN;
    }
    mbbeserial->fd = open(mbbeserial->mode->device,  O_RDWR | O_NOCTTY);
    if(mbbeserial->fd < 0){
        ret = -errno;
        mbbeserial->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_serial.c/mbbe_open()/open(): return %i", ret);
        return negerrno_to_mberrno(ret);
    }
    ret = tcgetattr(mbbeserial->fd, &options);
    if(ret < 0){
        ret = -errno;
        mbbeserial->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_serial.c/mbbe_open()/tcgetattr(): return %i", ret);
        goto __error;
    }
    ret = cfsetispeed(&options, mbbeserial->mode->kbps);
    if(ret < 0){
        ret = -errno;
        mbbeserial->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_serial.c/mbbe_open()/cfsetispeed(): return %i", ret);
        goto __error;
    }
    ret = cfsetospeed(&options, mbbeserial->mode->kbps);
    if(ret < 0){
        ret = -errno;
        mbbeserial->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_serial.c/mbbe_open()/cfsetospeed(): return %i", ret);
        goto __error;
    }
    cfmakeraw(&options);
    switch(mbbeserial->mode->parity){
    case 0:
        options.c_cflag |= CSTOPB;
        break;
    case 1:
        options.c_cflag |= PARENB | PARODD;
        break;
    case 2:
        options.c_cflag |= PARENB;
        break;
    }
    options.c_cc[VTIME] = mbbeserial->mode->timeout;
    options.c_cc[VMIN] = 0;
    ret = tcsetattr(mbbeserial->fd, TCSANOW , &options);
    if(ret < 0){
        ret = -errno;
        mbbeserial->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_serial.c/mbbe_open()/tcsetattr(): return %i", ret);
        goto __error;
    }
    mbbeserial->action(MODBUSD_ACTION_ONOPEN, &mbbeserial->mbbe);
    return 0;
__error:
    close(mbbeserial->fd);
    mbbeserial->fd = -1;
    return negerrno_to_mberrno(ret);
}

MB_ERRNO static int  mbbe_serial_close(const struct modbusd_backend* mbbe){
    struct modbusd_backend_serial* mbbeserial = MBBETOMBBESERIAL(mbbe);
    if(mbbeserial->fd < 0){
        return MODBUSD_ENOTCONN;
    }
    close(mbbeserial->fd);
    mbbeserial->fd = -1;
    mbbeserial->action(MODBUSD_ACTION_ONCLOSE, &mbbeserial->mbbe);
    return 0;
}

MB_ERRNO static int  mbbe_serial_status(const struct modbusd_backend* mbbe){
    struct modbusd_backend_serial* mbbeserial = MBBETOMBBESERIAL(mbbe);
    if(mbbeserial->fd < 0) {
        return 0;
    }
    int ret = write(mbbeserial->fd, NULL, 0);
    if(ret < 0){
        ret = -errno;
        if(ret == -EINTR){
            return 1;
        }
        mbbeserial->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_serial.c/mbbe_status()/write(): return %i", ret);
        mbbe_serial_close(&mbbeserial->mbbe);
        return 0;
    }else{
        return 1;
    }
}

MB_ERRNO static int mbbe_serial_dorequest(const struct modbusd_backend* mbbe, uint8_t* data, int datalen){
    struct modbusd_backend_serial* mbbeserial = MBBETOMBBESERIAL(mbbe);
    if(mbbeserial->fd < 0){
        return MODBUSD_ENOTCONN;
    }
    int ret = tcflush(mbbeserial->fd, TCIOFLUSH);
    if(ret < 0){
        ret = -errno;
        mbbeserial->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_serial.c/mbbe_request()/tcflush(): return %i", ret);
        goto __error;
    }
    do{
        ret = write(mbbeserial->fd, data, datalen);
        if(ret < 0){
            ret = -errno;
        }
    }while(ret == -EINTR);
    if(ret < 0){
        mbbeserial->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_serial.c/mbbe_request()/write(): return %i", ret);
        goto __error;
    }
    uint16_t crc = crc16(data, datalen);
    uint8_t tmp[257];
    tmp[0] = crc >> 8;
    tmp[1] = crc & 0xFF;
    do{
        ret = write(mbbeserial->fd, tmp, 2);
        if(ret < 0){
            ret = -errno;
        }
    }while(ret == -EINTR);
    if(ret < 0){
        mbbeserial->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_serial.c/mbbe_request()/write(): return %i", ret);
        goto __error;
    }
    do{
        ret = tcdrain(mbbeserial->fd);
        if(ret < 0){
            ret = -errno;
        }
    }while(ret == -EINTR);
    if(ret < 0){
        mbbeserial->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_serial.c/mbbe_request()/tcdrain(): return %i", ret);
        goto __error;
    }
    do{
        ret = read(mbbeserial->fd, tmp, 257);
        if(ret < 0){
            ret = -errno;
        }
    }while(ret == -EINTR);
    if(ret < 0){
        mbbeserial->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_serial.c/mbbe_request()/read(): return %i", ret);
        goto __error;
    }else if(ret == 257){
        mbbeserial->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_serial.c/mbbe_request()/read(): buffer overflow");
        return 0;
    }else if(ret < 4){
        mbbeserial->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_serial.c/mbbe_request()/read(): too short answer(%i)", ret);
        return 0;
    }
    if(crc16(tmp, ret) == 0){
        memcpy(data, tmp, ret - 2);
        return ret - 2;
    }
    mbbeserial->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_serial.c/mbbe_request(): answer CRC error");
    return 0;
__error:
    mbbe_serial_close(&mbbeserial->mbbe);
    return negerrno_to_mberrno(ret);
}

MB_ERRNO static void mbbe_serial_free(const struct modbusd_backend* mbbe){
    struct modbusd_backend_serial* mbbeserial = MBBETOMBBESERIAL(mbbe);
    mbbe_serial_close(&mbbeserial->mbbe);
    free(mbbeserial->mode);
    free(mbbeserial);
}

static int dummy_action(int action, ...){
    (void)action;
    return 0;
}

static void dummy_logout(int ll, const char* format, ...){
    (void)ll;
    (void)format;
}

const struct modbusd_backend* mbbe_serial(struct modbusd_backend_def* def){
    if(def->modbusd_version != MODBUSD_VERSION){
        if(def->logout){
            def->logout(MODBUSD_LOGLEVEL_DEBUG, "modbusd version (%i) mismatch frontend driver version (%i)", def->modbusd_version, MODBUSD_VERSION);
        }
        return NULL;
    }
    struct mbbe_serial_mode* mode = mbbe_parse(def);
    if(!mode){
        return NULL;
    }
    struct modbusd_backend_serial* mbbe = (struct modbusd_backend_serial*)malloc(sizeof(struct modbusd_backend_serial) + strlen(def->arg) + 1);
    if(mbbe){
        mbbe->mbbe.open = mbbe_serial_open;
        mbbe->mbbe.close = mbbe_serial_close;
        mbbe->mbbe.status = mbbe_serial_status;
        mbbe->mbbe.dorequest = mbbe_serial_dorequest;
        mbbe->mbbe.free = mbbe_serial_free;
        mbbe->mbbe.tag = def->tag;
        mbbe->mbbe.arg = mbbe->arg;
        mbbe->mbbe.driver = "serial";
        mbbe->mbbe.id = mbbe->arg;

        mbbe->action = def->action? def->action: dummy_action;
        mbbe->logout = def->logout? def->logout: dummy_logout;

        strcpy(mbbe->arg, def->arg);
        mbbe->fd = -1;
        mbbe->mode = mode;
    }else if(def->logout){
        def->logout(MODBUSD_LOGLEVEL_DEBUG, "mbbe_serial.c/mbbe_serial(): not enought memory");
    }
    return (struct modbusd_backend*)mbbe;
}

const char* mbbe_serial_help(void){
    return "usage: ... -bserial \"-d/dev/tty* -m115200N -t1\" ...\r\n" \
           "    -d[path]           serial device file\r\n" \
           "    -m[kbps][N|O|E]    kbps is an exchange speed, N|O|E is parity none|odd|even\r\n" \
           "    -t[timeout]        message receive timeout in 1/10 sec\r\n";
}
